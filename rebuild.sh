#!/bin/zsh

ppath="$HOME/Projects/public/ins3cure.com"
hugo --config=${ppath}/config.yaml --cleanDestinationDir --destination=${ppath}/public

echo
git status

echo
echo -n "I can run git add -A. Continue? (y/n) "
read -q 
if [ $? -eq 0 ]; then
	git add -A
	git commit
fi
