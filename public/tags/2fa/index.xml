<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>2FA on ins3cure.com</title>
    <link>https://ins3cure.com/tags/2fa/</link>
    <description>ins3cure.com (2FA)</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <lastBuildDate>Sat, 15 Jan 2022 02:04:14 +0100</lastBuildDate>
    
    <atom:link href="https://ins3cure.com/tags/2fa/index.xml" rel="self" type="application/rss+xml" />
    
    
    <item>
      <title>Configuring RHEL 8 for YubiKey</title>
      <link>https://ins3cure.com/rhel-8-yubikey/</link>
      <pubDate>Sat, 15 Jan 2022 02:04:14 +0100</pubDate>
      
      <guid>https://ins3cure.com/rhel-8-yubikey/</guid>
      <description>&lt;p&gt;An alternative to Google Authenticator.&lt;/p&gt;
&lt;p&gt;According to &lt;a href=&#34;https://en.wikipedia.org/wiki/YubiKey&#34;&gt;Wikipedia&lt;/a&gt;, &lt;em&gt;The YubiKey is a hardware authentication device manufactured by Yubico to protect access to computers, networks, and online services that supports one-time passwords (OTP), public-key cryptography, and authentication, and the Universal 2nd Factor (U2F) and FIDO2 protocols developed by the FIDO Alliance.&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;To make it work we will need to install some packages from the &lt;a href=&#34;https://docs.fedoraproject.org/en-US/epel/#_el8&#34;&gt;EPEL repository&lt;/a&gt;:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;yum -y install pam_yubico ykclient ykpers&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;(Note: man pages are not nearly as complete as the documentation in the &lt;a href=&#34;https://developers.yubico.com/yubikey-personalization/Manuals/ykpersonalize.1.html&#34;&gt;yubico developers page&lt;/a&gt; Take a look at it if you need more information).&lt;/p&gt;
&lt;p&gt;Insert your YubiKey and run:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;ykpersonalize -2 -ochal-resp -ochal-hmac -ohmac-lt64 -oserial-api-visible&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;Optionally add &lt;code&gt;-ochal-btn-trig&lt;/code&gt; and the device will require a button touch; this is hardly a security improvement if you leave your YubiKey plugged in. I guess this is solved with the new Bio Series YubiKeys that will recognize your fingerprint but they are way too expensive.&lt;/p&gt;
&lt;p&gt;You should get a similar output to this:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;&lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;user@host ~&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;$ ykpersonalize -2 -ochal-resp -ochal-hmac -ohmac-lt64 -oserial-api-visible
Firmware version 3.4.3 Touch level &lt;span style=&#34;color:#ae81ff&#34;&gt;1551&lt;/span&gt; Program sequence &lt;span style=&#34;color:#ae81ff&#34;&gt;3&lt;/span&gt;

Configuration data to be written to key configuration 2:

fixed: m:
uid: n/a
key: h:b4...blablahbla...d0
acc_code: h:000000000000
OATH IMF: h:0
ticket_flags: CHAL_RESP
config_flags: CHAL_HMAC|HMAC_LT64
extended_flags: SERIAL_API_VISIBLE

Commit? &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;y/n&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt; &lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;n&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;: y
&lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;user@host ~&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;$&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;The Yubikey has to be associated to a user. So, logged in as the desired user, run:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;&lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;user@host ~&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;$ ykpamcfg -2
Stored initial challenge and expected response in &lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#39;/home/user/.yubico/challenge-&amp;lt;serial&amp;gt;&amp;#39;&lt;/span&gt;.&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;At this point you may have to touch the YubiKey button depending on your configuration.&lt;/p&gt;
&lt;p&gt;We are almost done!&lt;/p&gt;
&lt;h2 id=&#34;testing&#34;&gt;Testing&lt;/h2&gt;
&lt;p&gt;In order to test minimizing the risk of being locked out, make sure you can run &lt;code&gt;sudo&lt;/code&gt;. In my case I have a file &lt;code&gt;/etc/sudoers.d/user&lt;/code&gt; containing &lt;code&gt;user ALL=(ALL) ALL&lt;/code&gt;.&lt;/p&gt;
&lt;p&gt;Modify &lt;code&gt;/etc/pam.d/sudo&lt;/code&gt; and add this line before &lt;code&gt;auth       include      system-auth&lt;/code&gt;:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;auth       required   pam_yubico.so mode&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;challenge-response&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;In my case this is the resulting file:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;&lt;span style=&#34;color:#75715e&#34;&gt;#%PAM-1.0&lt;/span&gt;
auth       required   pam_yubico.so mode&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;challenge-response
auth       include      system-auth
account    include      system-auth
password   include      system-auth
session    include      system-auth&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;Disconnect the YubiKey and try to run this command:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;&lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;user@host ~&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;$ sudo echo test
&lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;sudo&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt; password &lt;span style=&#34;color:#66d9ef&#34;&gt;for&lt;/span&gt; user: 
Sorry, try again.
&lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;sudo&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt; password &lt;span style=&#34;color:#66d9ef&#34;&gt;for&lt;/span&gt; user: 
sudo: &lt;span style=&#34;color:#ae81ff&#34;&gt;1&lt;/span&gt; incorrect password attempt&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;The &lt;code&gt;sudo&lt;/code&gt; command failed, as expected. Now try again after connecting the YubiKey:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;&lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;user@host ~&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;$ sudo echo test
&lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;sudo&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt; password &lt;span style=&#34;color:#66d9ef&#34;&gt;for&lt;/span&gt; user: 
test&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;It works! Now the serious stuff. Want to require the 2FA to log in? Update &lt;code&gt;/etc/pam.d/gdm-password&lt;/code&gt; by adding &lt;code&gt;auth       required   pam_yubico.so mode=challenge-response&lt;/code&gt; before &lt;code&gt;auth substack password-auth&lt;/code&gt;. This should be the result on a standard RHEL 8 system:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;auth     &lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;success&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;&lt;span style=&#34;color:#66d9ef&#34;&gt;done&lt;/span&gt; ignore&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;ignore default&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;bad&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt; pam_selinux_permit.so
auth        required      pam_yubico.so mode&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;challenge-response
auth        substack      password-auth
auth        optional      pam_gnome_keyring.so
auth        include       postlogin
&lt;span style=&#34;color:#f92672&#34;&gt;[&lt;/span&gt;...&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;Save the file. Lock the screen. Make sure your YubiKey is connected. Fingers crossed. Try to unlock.&lt;/p&gt;
&lt;p&gt;If you are still reading this, it has worked. And SELinux is still in enforcing mode! 😂&lt;/p&gt;
&lt;h2 id=&#34;moving-the-challenge-file&#34;&gt;Moving the challenge file&lt;/h2&gt;
&lt;p&gt;In some cases you may want to move the challenge file to a path only readable and writable by root. You can do it this way:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;sudo mkdir /var/yubico
sudo chown root.root /var/yubico
sudo chmod &lt;span style=&#34;color:#ae81ff&#34;&gt;700&lt;/span&gt; /var/yubico
ykpamcfg -2 -v
...
Stored initial challenge and expected response in &lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#39;$HOME/.yubico/challenge-123456&amp;#39;&lt;/span&gt;.
sudo mv ~/.yubico/challenge-123456 /var/yubico/&amp;lt;username&amp;gt;-123456
sudo chown root.root /var/yubico/&amp;lt;username&amp;gt;-123456
sudo chmod &lt;span style=&#34;color:#ae81ff&#34;&gt;600&lt;/span&gt; /var/yubico/&amp;lt;username&amp;gt;-123456&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;Where &lt;code&gt;&amp;lt;username&amp;gt;&lt;/code&gt; is the name of the user that has to be authenticated with the YubiKey. Then you have to add the right path as an option to the PAM module in the PAM file configuration. In our exmaple, the line in &lt;code&gt;/etc/pam.d/gdp-password&lt;/code&gt; will read:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;auth   required        pam_yubico.so mode&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;challenge-response chalresp_path&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;/var/yubico&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;h1 id=&#34;2fa-for-ssh&#34;&gt;2FA for ssh&lt;/h1&gt;
&lt;p&gt;There are several methods:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;OTP via PAM module (&lt;code&gt;pam_yubico&lt;/code&gt;). Requires a validation server.&lt;/li&gt;
&lt;li&gt;U2F via &lt;code&gt;pam-u2f&lt;/code&gt;.&lt;/li&gt;
&lt;li&gt;With OpenSSH 8.2 or higher, FIDO/U2F is natively supported. See &lt;a href=&#34;https://www.openssh.com/txt/release-8.2&#34;&gt;OpenSSH 8.2 release notes&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;maybe addressed in another post.&lt;/p&gt;
&lt;h1 id=&#34;conclusion&#34;&gt;Conclusion&lt;/h1&gt;
&lt;p&gt;Advantages:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Easy to configure&lt;/li&gt;
&lt;li&gt;Usually easier than typing a 6-digit code&lt;/li&gt;
&lt;li&gt;More convenient than Google Authenticator for frequent use&lt;/li&gt;
&lt;li&gt;Works offline (or not, but you can choose)&lt;/li&gt;
&lt;li&gt;No SELinux hassle! It is well supported and you have your booleans if you ever need them:&lt;/li&gt;
&lt;/ul&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;user@host$ getsebool -a | grep -i yubikey
authlogin_yubikey --&amp;gt; off&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;Disadvantages:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;Expensive&lt;/li&gt;
&lt;li&gt;Even more expensive if you need a backup (recommended)&lt;/li&gt;
&lt;li&gt;Forget your YubiKey connected and you are screwed&lt;/li&gt;
&lt;li&gt;Lose your YubiKey and you are (even more) screwed&lt;/li&gt;
&lt;/ul&gt;
&lt;h1 id=&#34;references&#34;&gt;References&lt;/h1&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://support.yubico.com/hc/en-us/articles/360016648999-Red-Hat-Linux-Family-Login-Guide-Challenge-Response&#34;&gt;Red Hat Linux Family Login Guide - Challenge Response&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://developers.yubico.com/yubico-pam/Authentication_Using_Challenge-Response.html&#34;&gt;Local Authentication Using Challenge Response&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://fedoraproject.org/wiki/Using_Yubikeys_with_Fedora&#34;&gt;Using YubiKeys with Fedora&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
    </item>
    
    <item>
      <title>Enabling 2FA on RHEL 8 using Google Authenticator</title>
      <link>https://ins3cure.com/enabling-2fa-on-rhel8/</link>
      <pubDate>Fri, 14 Jan 2022 20:02:11 +0100</pubDate>
      
      <guid>https://ins3cure.com/enabling-2fa-on-rhel8/</guid>
      <description>&lt;p&gt;Enabling 2FA on RHEL 8 using Google Authenticator is easy&amp;hellip; not. Especially if SELinux is enforced.&lt;/p&gt;
&lt;p&gt;First things first. Let&amp;rsquo;s install &lt;code&gt;google-authenticator&lt;/code&gt;. You need to &lt;a href=&#34;https://docs.fedoraproject.org/en-US/epel/#_el8&#34;&gt;enable EPEL repository&lt;/a&gt; and install &lt;code&gt;google-authenticator&lt;/code&gt; and, most likely, also &lt;code&gt;qrencode&lt;/code&gt; in order to be able to create the usual QR. Of course, install the app in your phone too. By the way, it does not have to be the Google one, there are multiple options in the marketplaces.&lt;/p&gt;
&lt;p&gt;I would suggest to try to get a grip on how PAM works. &lt;a href=&#34;https://www.redhat.com/sysadmin/pluggable-authentication-modules-pam&#34;&gt;An introduction to Pluggable Authentication Modules (PAM) in Linux&lt;/a&gt; can be a good start.&lt;/p&gt;
&lt;h1 id=&#34;configuring-2fa-for-ssh&#34;&gt;Configuring 2FA for ssh&lt;/h1&gt;
&lt;p&gt;When enabling 2FA on RHEL (and RHEL variants like CentOS, Rocky Linux, etc.) most people thinks of &lt;code&gt;ssh&lt;/code&gt;. Ok, it makes sense, RHEL derivatives are commonly users as servers.&lt;/p&gt;
&lt;p&gt;So let&amp;rsquo;s go:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;yum -y install google-authenticator qrencode&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;user@host$ google-authenticator 

Do you want authentication tokens to be time-based &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;y/n&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt; y
Warning: pasting the following URL into your browser exposes the OTP secret to Google:
  https://www.google.com/chart?chs&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;200x200&amp;amp;chld&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;M|0&amp;amp;cht&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;qr&amp;amp;chl&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;otpauth://totp/...secret...

&lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;Big QR here &lt;span style=&#34;color:#66d9ef&#34;&gt;if&lt;/span&gt; qrencode was installed&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt;

Your new secret key is: KTMWDPAUYTMQPNWHFUIAXBXXMA
Enter code from app &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;-1 to skip&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt;: &lt;span style=&#34;color:#ae81ff&#34;&gt;358739&lt;/span&gt;
Code incorrect &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;correct code 820697&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt;. Try again.
Enter code from app &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;-1 to skip&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt;: &lt;span style=&#34;color:#ae81ff&#34;&gt;820697&lt;/span&gt;
Code confirmed
Your emergency scratch codes are:
  &lt;span style=&#34;color:#ae81ff&#34;&gt;23949500&lt;/span&gt;
  &lt;span style=&#34;color:#ae81ff&#34;&gt;33803586&lt;/span&gt;
  &lt;span style=&#34;color:#ae81ff&#34;&gt;12458013&lt;/span&gt;
  &lt;span style=&#34;color:#ae81ff&#34;&gt;68333993&lt;/span&gt;
  &lt;span style=&#34;color:#ae81ff&#34;&gt;78368786&lt;/span&gt;

Do you want me to update your &lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#34;/home/user/.google_authenticator&amp;#34;&lt;/span&gt; file? &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;y/n&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt; y

Do you want to disallow multiple uses of the same authentication
token? This restricts you to one login about every 30s, but it increases
your chances to notice or even prevent man-in-the-middle attacks &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;y/n&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt; n

By default, a new token is generated every &lt;span style=&#34;color:#ae81ff&#34;&gt;30&lt;/span&gt; seconds by the mobile app.
In order to compensate &lt;span style=&#34;color:#66d9ef&#34;&gt;for&lt;/span&gt; possible time-skew between the client and the server,
we allow an extra token before and after the current time. This allows &lt;span style=&#34;color:#66d9ef&#34;&gt;for&lt;/span&gt; a
time skew of up to &lt;span style=&#34;color:#ae81ff&#34;&gt;30&lt;/span&gt; seconds between authentication server and client. If you
experience problems with poor time synchronization, you can increase the window
from its default size of &lt;span style=&#34;color:#ae81ff&#34;&gt;3&lt;/span&gt; permitted codes &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;one previous code, the current
code, the next code&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt; to &lt;span style=&#34;color:#ae81ff&#34;&gt;17&lt;/span&gt; permitted codes &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;the &lt;span style=&#34;color:#ae81ff&#34;&gt;8&lt;/span&gt; previous codes, the current
code, and the &lt;span style=&#34;color:#ae81ff&#34;&gt;8&lt;/span&gt; next codes&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt;. This will permit &lt;span style=&#34;color:#66d9ef&#34;&gt;for&lt;/span&gt; a time skew of up to &lt;span style=&#34;color:#ae81ff&#34;&gt;4&lt;/span&gt; minutes
between client and server.
Do you want to &lt;span style=&#34;color:#66d9ef&#34;&gt;do&lt;/span&gt; so? &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;y/n&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt; y

If the computer that you are logging into isn&lt;span style=&#34;color:#960050;background-color:#1e0010&#34;&gt;&amp;#39;&lt;/span&gt;t hardened against brute-force
login attempts, you can enable rate-limiting &lt;span style=&#34;color:#66d9ef&#34;&gt;for&lt;/span&gt; the authentication module.
By default, this limits attackers to no more than &lt;span style=&#34;color:#ae81ff&#34;&gt;3&lt;/span&gt; login attempts every 30s.
Do you want to enable rate-limiting? &lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;y/n&lt;span style=&#34;color:#f92672&#34;&gt;)&lt;/span&gt; y&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;Cool, now we have a &lt;code&gt;.google_authenticator&lt;/code&gt; file with our secret, configuration and some one-time emergency tokens we could use in emergency cases.&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;user@host$ cat $HOME/.google_authenticator 
KTMWDPAUYTMQPNWHFUIAXBXXMA
&lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#34; RATE_LIMIT 3 30
&lt;/span&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#34;&lt;/span&gt; WINDOW_SIZE &lt;span style=&#34;color:#ae81ff&#34;&gt;17&lt;/span&gt;
&lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#34; TOTP_AUTH
&lt;/span&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;23949500
&lt;/span&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;33803586
&lt;/span&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;12458013
&lt;/span&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;68333993
&lt;/span&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;78368786&lt;/span&gt;&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;&lt;em&gt;(Note: no, I don&amp;rsquo;t care about sharing this secret publicly, it does not exist anymore)&lt;/em&gt;&lt;/p&gt;
&lt;p&gt;So far so good. Now we have to configure PAM (&lt;a href=&#34;https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html-single/configuring_authentication_and_authorization_in_rhel/index&#34;&gt;Pluggable Authentication Modules&lt;/a&gt;) and ssh configuration.&lt;/p&gt;
&lt;p&gt;For PAM we just have to add this line:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;auth required pam_google_authenticator.so&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;at the end of the PAM SSH configuration file: &lt;code&gt;/etc/pam.d/sshd&lt;/code&gt;.&lt;/p&gt;
&lt;p&gt;And make sure challenge &lt;code&gt;ChallengeResponseAuthentication yes&lt;/code&gt; is present so reponse authentication is enabled.&lt;/p&gt;
&lt;p&gt;That&amp;rsquo;s all, let&amp;rsquo;s check everything works:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;user@host$ ssh user@localhost
Password: 
Verification code: 
Password: 
Verification code: 
Password: 
Verification code: 
Received disconnect from ::1 port 22:2: Too many authentication failures
Disconnected from ::1 port &lt;span style=&#34;color:#ae81ff&#34;&gt;22&lt;/span&gt;
user@host$ &lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;What&amp;rsquo;s happening here? It should work!&lt;/p&gt;
&lt;p&gt;Ok, let&amp;rsquo;s do the obvious first troubleshooting step:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;user@host$ sudo setenforce &lt;span style=&#34;color:#ae81ff&#34;&gt;0&lt;/span&gt;
user@host$ ssh user@localhost
Password: 
Verification code: 

Last failed login: Fri Jan &lt;span style=&#34;color:#ae81ff&#34;&gt;14&lt;/span&gt; 21:28:25 CET &lt;span style=&#34;color:#ae81ff&#34;&gt;2022&lt;/span&gt; from ::1 on ssh:notty
There were &lt;span style=&#34;color:#ae81ff&#34;&gt;3&lt;/span&gt; failed login attempts since the last successful login.
Last login: Fri Jan &lt;span style=&#34;color:#ae81ff&#34;&gt;14&lt;/span&gt; 21:27:30 &lt;span style=&#34;color:#ae81ff&#34;&gt;2022&lt;/span&gt; from ::1
user@host$ &lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;We&amp;rsquo;re in! F*ck SELinux! Well, no. The problem is SELinux does not allow ssh to read the secret file:&lt;/p&gt;
&lt;p&gt;Jan 14 21:34:44 host sshd(pam_google_authenticator)[19454]: Failed to read &amp;ldquo;/home/user/.google_authenticator&amp;rdquo; for &amp;ldquo;user&amp;rdquo;. In some cases, ssh/PAM may need to write a file with a random name before updating the secret file. Unfortunately there is not an easy solution for this except to move the secret file to a directory with the right SELinux context: &lt;code&gt;$HOME/.ssh&lt;/code&gt;. But we will have to do a slight modification to the PAM configuration file:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;auth required pam_google_authenticator.so secret&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;/home/&lt;span style=&#34;color:#e6db74&#34;&gt;${&lt;/span&gt;USER&lt;span style=&#34;color:#e6db74&#34;&gt;}&lt;/span&gt;/.ssh/.google_authenticator&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;Move the secret file to &lt;code&gt;.ssh&lt;/code&gt; and &lt;strong&gt;make sure the context is right&lt;/strong&gt;:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;user@host$ ls -laZ .ssh/
total &lt;span style=&#34;color:#ae81ff&#34;&gt;40&lt;/span&gt;
drwx------.  &lt;span style=&#34;color:#ae81ff&#34;&gt;2&lt;/span&gt; user user unconfined_u:object_r:ssh_home_t:s0       &lt;span style=&#34;color:#ae81ff&#34;&gt;180&lt;/span&gt; Jan &lt;span style=&#34;color:#ae81ff&#34;&gt;14&lt;/span&gt; 21:47 .
drwx------. &lt;span style=&#34;color:#ae81ff&#34;&gt;20&lt;/span&gt; user user unconfined_u:object_r:user_home_dir_t:s0 &lt;span style=&#34;color:#ae81ff&#34;&gt;4096&lt;/span&gt; Jan &lt;span style=&#34;color:#ae81ff&#34;&gt;14&lt;/span&gt; 21:45 ..
-rw-r-----.  &lt;span style=&#34;color:#ae81ff&#34;&gt;1&lt;/span&gt; user user unconfined_u:object_r:ssh_home_t:s0       &lt;span style=&#34;color:#ae81ff&#34;&gt;138&lt;/span&gt; Jan  &lt;span style=&#34;color:#ae81ff&#34;&gt;8&lt;/span&gt; 19:16 config
-r--------.  &lt;span style=&#34;color:#ae81ff&#34;&gt;1&lt;/span&gt; user user system_u:object_r:ssh_home_t:s0           &lt;span style=&#34;color:#ae81ff&#34;&gt;153&lt;/span&gt; Jan &lt;span style=&#34;color:#ae81ff&#34;&gt;14&lt;/span&gt; 21:47 .google_authenticator
-rw-------.  &lt;span style=&#34;color:#ae81ff&#34;&gt;1&lt;/span&gt; user user unconfined_u:object_r:ssh_home_t:s0      &lt;span style=&#34;color:#ae81ff&#34;&gt;2602&lt;/span&gt; Jan  &lt;span style=&#34;color:#ae81ff&#34;&gt;3&lt;/span&gt; 18:15 id_rsa
-rw-r--r--.  &lt;span style=&#34;color:#ae81ff&#34;&gt;1&lt;/span&gt; user user unconfined_u:object_r:ssh_home_t:s0       &lt;span style=&#34;color:#ae81ff&#34;&gt;572&lt;/span&gt; Jan  &lt;span style=&#34;color:#ae81ff&#34;&gt;3&lt;/span&gt; 18:15 id_rsa.pub
-rw-r--r--.  &lt;span style=&#34;color:#ae81ff&#34;&gt;1&lt;/span&gt; user user unconfined_u:object_r:ssh_home_t:s0      &lt;span style=&#34;color:#ae81ff&#34;&gt;2078&lt;/span&gt; Jan  &lt;span style=&#34;color:#ae81ff&#34;&gt;9&lt;/span&gt; 20:33 known_hosts&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;(Note: I&amp;rsquo;m quite sure the initial context was &lt;code&gt;unconfined_u:object_r:ssh_home_t&lt;/code&gt; ,but it is apparently changed after the first login).&lt;/p&gt;
&lt;div class=&#34;alert warning&#34;&gt;
    Just to clarify, this will work for user/password authentication. The verification code will not be required for public key authentication. Can it be done? Yes. Does it make sense? Probably not.
&lt;/div&gt;
&lt;h2 id=&#34;bonus-track-grace-period&#34;&gt;Bonus track: grace period&lt;/h2&gt;
&lt;p&gt;You may want to skip the 2FA for some time if you frequently login to the box. For example, you may want to request the verfication code once every hour. If that&amp;rsquo;s your case, just update &lt;code&gt;/etc/pam.d/sshd&lt;/code&gt; like this:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;auth required pam_google_authenticator.so secret&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;/home/&lt;span style=&#34;color:#e6db74&#34;&gt;${&lt;/span&gt;USER&lt;span style=&#34;color:#e6db74&#34;&gt;}&lt;/span&gt;/.ssh/.google_authenticator grace_period&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;&lt;span style=&#34;color:#ae81ff&#34;&gt;3600&lt;/span&gt;&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;where &lt;code&gt;grace_period&lt;/code&gt; is the number of seconds the 2FA will be ignored.&lt;/p&gt;
&lt;h1 id=&#34;beyond-ssh-2fa-for-your-graphical-login&#34;&gt;Beyond ssh: 2FA for your graphical login&lt;/h1&gt;
&lt;p&gt;Chances are that you are happy with the above configuration, but what if you use RHEL/CentOS/Rocky or even Fedora as a daily driver? Can you use Google Authenticator for you graphical login? Of course you can but I&amp;rsquo;m not sure it is the most convenient.&lt;/p&gt;
&lt;p&gt;Take into account that different services use different PAM modules. Documentation is not very clear so there is some trial and error here. In general, &lt;code&gt;system-auth&lt;/code&gt; is mostly used by local services and &lt;code&gt;password-auth&lt;/code&gt; by network services. But I don&amp;rsquo;t know for sure&amp;hellip;&lt;/p&gt;
&lt;p&gt;Let&amp;rsquo;s start by &lt;em&gt;temporarily&lt;/em&gt; disabling SELiniux and modifying &lt;code&gt;/etc/pam.d/system-auth&lt;/code&gt;:&lt;/p&gt;
&lt;p&gt;Remove:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;auth        sufficient        pam_unix.so&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;and add:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;auth        requisite         pam_unix.so
auth        sufficient        pam_google_authenticator.so secret&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;/home/&lt;span style=&#34;color:#e6db74&#34;&gt;${&lt;/span&gt;USER&lt;span style=&#34;color:#e6db74&#34;&gt;}&lt;/span&gt;/.ssh/.google_authenticator&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;in its place. So far so good:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;user@host$ su - user
Password: 
Verification code: 
user@host$&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;Unfortunately the very convenient &lt;code&gt;grace_period&lt;/code&gt; does not work 😢&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;Jan &lt;span style=&#34;color:#ae81ff&#34;&gt;14&lt;/span&gt; 22:39:49 host su-l&lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;pam_google_authenticator&lt;span style=&#34;color:#f92672&#34;&gt;)[&lt;/span&gt;23132&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;: Accepted google_authenticator &lt;span style=&#34;color:#66d9ef&#34;&gt;for&lt;/span&gt; user
Jan &lt;span style=&#34;color:#ae81ff&#34;&gt;14&lt;/span&gt; 22:39:49 host su-l&lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;pam_google_authenticator&lt;span style=&#34;color:#f92672&#34;&gt;)[&lt;/span&gt;23132&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;: debug: google_authenticator &lt;span style=&#34;color:#66d9ef&#34;&gt;for&lt;/span&gt; host &lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#34;(null)&amp;#34;&lt;/span&gt;
Jan &lt;span style=&#34;color:#ae81ff&#34;&gt;14&lt;/span&gt; 22:39:49 host su-l&lt;span style=&#34;color:#f92672&#34;&gt;(&lt;/span&gt;pam_google_authenticator&lt;span style=&#34;color:#f92672&#34;&gt;)[&lt;/span&gt;23132&lt;span style=&#34;color:#f92672&#34;&gt;]&lt;/span&gt;: Failed to store grace_period timestamp in config&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;The grace period works storing the IP address and the timestamp of the last successful login in the secret file. Like this:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;&lt;span style=&#34;color:#e6db74&#34;&gt;&amp;#34; LAST0 ::1 1642197329&lt;/span&gt;&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;(yes, &lt;code&gt;::1&lt;/code&gt; is the IP address)&lt;/p&gt;
&lt;p&gt;This time it is not a permissions or SELinux problem. My guess is grace period does not work with local authentication because the service does not pass the IP address. This may be a showstopper because no one wants to pick up the phone, start the app and enter the 6-digit 2FA every time your screen is locked after inactivity timeout.&lt;/p&gt;
&lt;p&gt;Of course there would be also SELinux denials but grace period won&amp;rsquo;t work anyway.&lt;/p&gt;
&lt;p&gt;By the way, graphical login could be configured adding:&lt;/p&gt;
&lt;div class=&#34;highlight&#34;&gt;&lt;pre tabindex=&#34;0&#34; style=&#34;color:#f8f8f2;background-color:#272822;-moz-tab-size:4;-o-tab-size:4;tab-size:4&#34;&gt;&lt;code class=&#34;language-shell&#34; data-lang=&#34;shell&#34;&gt;auth    sufficient    pam_google_authenticator.so secret&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;/home/&lt;span style=&#34;color:#e6db74&#34;&gt;${&lt;/span&gt;USER&lt;span style=&#34;color:#e6db74&#34;&gt;}&lt;/span&gt;/.ssh/.google_authenticator grace_period&lt;span style=&#34;color:#f92672&#34;&gt;=&lt;/span&gt;&lt;span style=&#34;color:#ae81ff&#34;&gt;3600&lt;/span&gt;&lt;/code&gt;&lt;/pre&gt;&lt;/div&gt;
&lt;p&gt;to &lt;code&gt;/etc/pam.d/gdm-password&lt;/code&gt;. But again the secret file can&amp;rsquo;t be updated with the IP and timestamp so the grace period does not work.&lt;/p&gt;
&lt;p&gt;There will be a SELinux denial because SELinux will not allow gdm to write to the &lt;code&gt;$HOME/.ssh&lt;/code&gt; directory. The verification code will work but the secret file update won&amp;rsquo;t (it would not work anyway because the host is &amp;lsquo;null&amp;rsquo;).&lt;/p&gt;
&lt;p&gt;If you want to use it anyway I would suggest to copy the same secret file to a directory outside .ssh. &lt;code&gt;$HOME&lt;/code&gt; (the default ocation for &lt;code&gt;.google_autenticator&lt;/code&gt;) would work but maybe a new standard location (i.e. &lt;code&gt;$HOME/.config/google-authentiator/secret&lt;/code&gt;) would be better once all involved parties agree. Doing this everything should work except grace period no matter if SELinux is enforcing.&lt;/p&gt;
&lt;p&gt;So to summarize, at least &lt;code&gt;google-authenticator-libpam&lt;/code&gt; needs to be patched. If you are not happy you can try a &lt;a href=&#34;https://ins3cure.com/rhel-8-yubikey&#34;&gt;YubiKey&lt;/a&gt; as an alternative.&lt;/p&gt;
&lt;h1 id=&#34;caveats&#34;&gt;Caveats&lt;/h1&gt;
&lt;p&gt;Even if you have configured 2FA it is trivial for an attacker with physical access to your machine to bypass by booting in &lt;code&gt;rd.break&lt;/code&gt; mode. Encrypt your filesystem, protect your GRUB and BIOS with passwords, etc. according to your risk.&lt;/p&gt;
&lt;p&gt;While testing you can get locket out. In that case you can &lt;a href=&#34;https://ins3cure.com/rhel8-root-password-recovery/&#34;&gt;follow this guide&lt;/a&gt; and revert the changes in the screwed up files (instead of updateing the password).&lt;/p&gt;
&lt;h1 id=&#34;references&#34;&gt;References&lt;/h1&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href=&#34;https://www.redhat.com/sysadmin/pluggable-authentication-modules-pam&#34;&gt;An introduction to Pluggable Authentication Modules (PAM) in Linux&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/4/html/reference_guide/s1-pam-sample-simple&#34;&gt;Sample PAM Configuration Files&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://bugzilla.redhat.com/show_bug.cgi?id=1840113&#34;&gt;[BUG] SELinux preventing google-authenticator to work on CentOS 8&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://github.com/google/google-authenticator-libpam/issues/101&#34;&gt;[BUG] google-authenticator-libpam&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&#34;https://github.com/fedora-selinux/selinux-policy/pull/469&#34;&gt;[BUG] SELinux policy&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</description>
    </item>
    
  </channel>
</rss>