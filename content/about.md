---
title: About us
url: about
author: /me
draft: false
---

I'm an old school sysadmin interested in computing, software engineering, linux, open source and that kind of things. Currently working in the cybersecurity field.

This is just my note taking application — sometimes these notes may come in handy. However do not expect to find very interesting stuff here. 

I may write in English, Spanish or something in between.
