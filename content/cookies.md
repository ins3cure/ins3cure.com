---
title: Cookie policy
url: cookies
author: /me
draft: false
---

### What are cookies?

HTTP cookies (also called web cookies, Internet cookies, browser cookies, or simply cookies) are small pieces of data stored on the user's computer by the web browser while browsing a website.

In compliance with EU cookie legislation, this page serves to inform visitors of cookies used on this website.

You can learn more about cookies [in the Wikipedia](https://en.wikipedia.org/wiki/HTTP_cookie).

### What kind cookies are set and what do you use it for?

We use cookies to analyze how the sites are used and how they are performing. For example, these Cookies track what pages are most frequently visited, and from what locations our visitors come from. This apply Google Analytics cookies.

We use Disqus for comments. To be able to post comments and have your Disqus login information remembered across pages, cookies are set.

We do not track personal information.

### How to reject cookies, and how to subsequently change the status regarding the cookies.

Do not use this site. There are more sites in the internet. A lot. Some of them may use cookies and some not.

### How do I get rid of them?

[It depends](https://bfy.tw/PrXM). But in general you just have to open your browser, go to settings/preferencies, find the privacy/cookie section and hit a delete/remove/forget all cookies button. Easy!

### Why do you need this s***?

Because some lawyer decided it is a great idea to bother you with a colourful banner with a button every time you visit a web site. Go figure.