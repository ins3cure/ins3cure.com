---
title: "RHEL8 root password recovery"
date: 2020-12-07T00:12:54+01:00
url: rhel8-root-password-recovery
author: /me
categories: ['blog', 'cheatsheet']
tags: ['linux', 'rhel8']
draft: false
---

When studying to apply for a Redhat certification I tend to go over root password recovery again. Just in case :smile: 

<!--more-->

A _normal_ installation of RHEL8 will display the grub menu when booting. When the menu appears on screen press <kbd>e</kbd>:

![GRUB](/img/rhel-grub-1.png)

Go to the end of the `linux` line (which is longer than one row) and add <kbd>rd.break</kbd> at the end of the line:

![GRUB](/img/rhel-grub-2.png)

Press <kbd>Ctrl+x</kbd> to boot and wait for the prompt.

![GRUB](/img/rhel-pwd-recovery-1.png)

Now you have to mount (rw mode) the <kbd>sysroot</kbd> partition, <kbd>chroot</kbd> to it and change the root password:

![GRUB](/img/rhel-pwd-recovery-2.png)

{{< alert danger >}}
If SELinux is enforcing, this is mandatory
{{< /alert >}}

Before rebooting, a file <kbd>/.autorelabel</kbd> must be created in the root directory to instruct SELinux to relabel filesystem upon next boot. Then, type <kbd>exit</kbd> twice:

![GRUB](/img/rhel-pwd-recovery-3.png)

The boot process will take longer than usual because of relabeling, but you will be able to login with your new root password.

{{< youtube q9dJj1yHcd8 >}}