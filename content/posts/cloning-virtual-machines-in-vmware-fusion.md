---
title: "Cloning virtual machines in VMware Fusion"
date: 2020-12-07T19:18:11+01:00
url: cloning-virtual-machines-in-vmware-fusion
author: /me
categories: ['blog']
tags: ['macos', 'virtualization', 'vmware']
draft: false
---

How to clone a virtual machine in VMware Fusion for Mac Personal Edition.

<!--more-->

As I already mentioned in a previous post, after upgrading to macOS Big Sur I decided to give [VMware Fusion](https://www.vmware.com/products/fusion.html) a chance.

There were two main reasons:

- VMware Fusion supports macOS Big Sur while Oracle VirtualBox still has some issues
- VMware Fusion is now free for personal use.

Free version of VMware fusion does not have a <kbd>Clone</kbd> button. However, you can still clone virtual machines.

## Cloning a virtual machine in VMware Fusion

1. Go to <kbd>File</kbd> <kbd>New</kbd> as you would do to create a new virtual machine
2. Installation method: Select "create a custom virtual machine"
3. Select the operating system (same as the original)
4. Choose firmware: same as original
5. Choose a Virtual Disk: select "Use an existing virtual disk"
6. Select the `vmdk` file from the original vm
7. Customize Settings: optionally select the name of the new vm

There may some post-cloning tasks like:

- Reconfiguring the virtual machine. Depending on the configiration of the original vm, the new one may require some tuning (IP/MAC addresses, hostname, etc.)
- Disable Side Channel Mitigations. It affects performance and, depending on your threat model, you may not need them.

You can watch a video of the whole process:

{{< youtube Jvy8g08afSY >}}