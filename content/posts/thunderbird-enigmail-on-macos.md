---
title: "Thunderbird Enigmail on Macos"
date: 2019-12-26
url: thunderbird-enigmail-on-macos
categories: ['blog']
tags: ['macos','thunderbird', 'pgp']
summary: Installing Enigmail on Thunderbird for macOS
---

{{< alert warning >}}
**Deprecation warning**\
This is not needed anymore since [Thunderbird includes OpenPGP tools from version 78](https://blog.thunderbird.net/2020/09/openpgp-in-thunderbird-78/)
{{< /alert >}}

![enigmail logo](/img/enigmail_logo.png)

Since some time ago Enigmail plugin for Thunderbird requires a working installation of GPG tools, including a graphical pinentry to be able to ask for the key passphrase.

It seems not to be available by default. To solve it, install a graphical pinentry:

```
brew install pinentry-mac
echo "use-standard-socket
pinentry-program /usr/local/bin/pinentry-mac" >> ~/.gnupg/gpg-agent.conf
killall gpg-agent
```

And you're good to go.

## References

- [Enigmail, gnupg & pinentry on Mac OS X using Homebrew](http://www.harpojaeger.com/2017/09/20/enigmail-gnupg-pinentry-on-mac-os-x-using-homebrew)

