---
title: "macOS Security (3)"
date: 2020-11-30
url: macos-security-3
categories: ['blog']
tags: ['macos','security']
summary: This third post about macOS security is focused on the System Integrity Protection
draft: true
---

From [Apple Documentation](https://developer.apple.com/library/archive/documentation/Security/Conceptual/System_Integrity_Protection_Guide/Introduction/Introduction.html#//apple_ref/doc/uid/TP40016462-CH1-DontLinkElementID_15)

> System Integrity Protection is a security feature in macOS that protects the system shipped by Apple. By protecting access to system locations and restricting runtime attachment to system processes, this security policy guards against compromise — whether accidental or by malicious code. 

But I don't care, I am root! Nope:

> System Integrity Protection restricts the root user account and limits the actions that the root user can perform on protected parts of the Mac operating system.

Starting in OS X 10.11 El Capitan (2015), System Integrity Protection (SIP) comprises a number of security mechanisms enforced by the kernel. In particular, system-owned files and folders can be only modified by authorized Apple-signed binaries.

And it has evolved to the point where the system volume is read-only.

## APFS

APFS (Apple File System) is the default filesystem for:

- macOS 10.13 High Sierra and later
- iOS 10.3 and later
- tvOS 10.2 and later
- watchOS 3.2 and later
- All iPadOS

APFS, successor to HFS+, is optimized for flash and SSD disks and strongly focused on encryption allowing features like multi-key encryption where each file (or event segments of files) are encrypted with a different key.

What's the role of the APFS filesystem in protecting macOS system software.

One key advantage of APFS over HFS+ is APFS volumes share free space. This allows implementation of features that were not possible in HFS+l, one of them being protecting system software from malicious changes or updates.

Starting with OS X El Capitan, some directories were write protected. From macOS Catalina, the whole root filesystem is read-only.

In fact it is read-only and writable at the same time. In the past it was done by mounting filesystems on top of directories in the root filesystem. Starting with Catalina is done via _firmlinks_.

![Catalina layout](/img/catalinadatar.png)

## Key concepts of SIP

### Protected system locations

The following directories can only be written to by the system:

- `/bin`
- `/sbin`
- `/usr`
- `/System`

The following directories are available to any process:

- `/usr/local`
- `/Applications`
- `[~]/Library`

All directories in `/usr` except for `/usr/local` are restricted to the system. Apple app directories in `/Applications` are restricted to the system.

### Runtime protections

When a process is started, the kernel checks to see whether the main executable is protected on disk or is signed with an special system entitlement. If either is true, then a flag is set to denote that it is protected against modification. Any attempt to attach to a protected process is denied by the kernel.

DTrace cannot be used to inspect system processes, whether from the Instruments application or the dtrace(1) command-line tool.

Attempting to attach to a system process, such as with LLDB, fails—even when running as root with sudo:

```
$> sudo lldb -n Finder
(lldb) process attach —name "Finder"
error: attach failed: attach failed: lost connection
```

{{< alert info >}}
System Integrity Protection does not prevent you from inspecting your own apps when you’re developing them. You can continue to use Instruments and LLDB to profile and debug apps built with Xcode.
{{< /alert >}}

### Kernel extensions

Kernel Extensions (KEXTs) allow developers load code directly into the macOS kernel. While this is really powerful, it poses obvious stability and security risks.

{{< highlight shell >}}
% ls /System/Library/Extensions     
ALF.kext                                  AppleSmartBatteryManager.kext
AMD10000Controller.kext                   AppleStorageDrivers.kext
AMD7000Controller.kext                    AppleSystemPolicy.kext
[...]
AppleSDXC.kext                            ntfs.kext
AppleSEPManager.kext                      pmtelemetry.kext
AppleSMBIOS.kext                          pthread.kext
AppleSMBusController.kext                 smbfs.kext
AppleSMBusPCI.kext                        triggers.kext
AppleSMC.kext                             udf.kext
AppleSMCLMU.kext                          vecLib.kext
AppleSMCRTC.kext                          watchdog.kext
AppleSRP.kext                             webcontentfilter.kext
AppleSSE.kext                             webdav_fs.kext
{{< /highlight >}}

With macOS Catalina Apple released:

- DriverKit framework
- SystemExtension framework

While KEXTs run in kernel space and must be coded in C or C++, SystemExtension framework runs in userspace and any macOS SDK framework or language is allowed. A bug in a SystemExtensions will not probably crash the whole system or make it vulnerable or unstable.

So KEXTs are being gradually deprecated and [some of them will not load in Big Sur](https://developer.apple.com/support/kernel-extensions/).

Examples of currently loaded kernel extensions:

{{< highlight shell >}}
% kextstat | grep -v com.apple
Index Refs Address            Size       Wired      Name (Version) UUID <Linked Against>
  172    3 0xffffff7f846bc000 0xf2000    0xf2000    org.virtualbox.kext.VBoxDrv (6.1.16) DAF044F0-5043-3B8E-8758-5A462567BDAA <8 6 5 3 1>
  177    0 0xffffff7f847c8000 0x8000     0x8000     org.virtualbox.kext.VBoxUSB (6.1.16) CA33FA52-5933-3F0E-9B40-8B1ECF4D6A36 <176 172 62 8 6 5 3 1>
  178    0 0xffffff7f847d0000 0x5000     0x5000     org.virtualbox.kext.VBoxNetFlt (6.1.16) C93EAF6A-676F-3370-BAE2-BDFD2BF16582 <172 8 6 5 3 1>
  179    0 0xffffff7f847d5000 0x6000     0x6000     org.virtualbox.kext.VBoxNetAdp (6.1.16) 76200407-D2F0-3D84-BCCF-7EEA18F16654 <172 6 5 1>
  181    0 0xffffff7f847db000 0x17000    0x17000    com.pcloud.pcloudfs.filesystems.pcloudfs (3.11.2) EC5A8692-4B37-35D9-B015-DC3E74971502 <8 6 5 3 1>
  193    0 0xffffff7f84862000 0x15000    0x15000    com.vmware.kext.vmnet (1719.52.30) 5322FF7C-6058-325E-B015-B85DBDE51D39 <6 5 3 1>
  194    0 0xffffff7f84877000 0x15000    0x15000    com.vmware.kext.vmx86 (1719.52.30) DDE9BAD3-3EB9-3209-A04F-33770FFC0E3A <8 6 5 3 1>
{{< /highlight >}}

## Configuring System Integrity Protection

SIP configuration is stored in NVRAM so it persists not only across reboots but even installations.

SIP can be configured using `csrutil`:

```
% csrutil status
System Integrity Protection status: enabled.
```

To enable/disable SIP you must boot to Recovery OS and run `csrutil` from the Terminal and reboot.

# References

- [What's new in Apple filesystems](https://developer.apple.com/videos/play/wwdc2019/710/)
- [macOS Catalina Boot Volume Layout](https://eclecticlight.co/2019/10/11/macos-catalina-boot-volume-layout-revised-for-10-15-release/)
- [Guide for Aple IT: macOS kernel extensions](https://blog.kandji.io/guide-for-apple-it-macos-kernel-extensions)
- [Deprecated Kernel Extensions and System Extension Alternatives](https://developer.apple.com/support/kernel-extensions/)
