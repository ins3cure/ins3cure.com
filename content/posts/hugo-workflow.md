---
title: "Hugo workflow"
date: 2020-11-15T14:05:15+01:00
url: hugo-workflow
categories: ['blog']
tags: ['hugo', 'web']
summary: Workflow with Hugo
draft: false
---

![Hugo](/img/hugo_logo.png)

This is the way I'm working with Hugo:

## Repository

I have a public repository in Bitbucket

## Local work

Clone the repo:

```
git clone https://ins3cure@bitbucket.org/ins3cure/ins3cure.com.git
```

It is not relevant, but I'm using [Visual Studio Code](https://code.visualstudio.com) to work with the code.

## Update the site

I'm using two directories under `posts`:

```
.
└── content
    └── posts
        └── blog
        └── htb
```

this way I keep separated normal "blog" posts and htb writeups (or any other category I come up with).

So to create new posts:

```
hugo new posts/blog/hugo-workflow.md
```

To test:

```
hugo server -D
```

Use the `-D` switch to include drafts, but remember to remove the draft tag from the front matter of the post.

Rebuild:

```
rm -rf ./public
hugo
```

{{< alert warning >}}
Remember to cleanup the public directory before committing. Hugo will not remove stale files or directories
{{< /alert >}}

This can be done with these command line options:

```
ppath=path/to/wherever
hugo --config=${ppath}/config.toml --cleanDestinationDir --destination=${ppath}/public
```

## Push

Once updates are ok, update remote git repo:

```
git add -A
git commit -m "added a new interesting post"
git push origin master
```

## Website updates

This happens automatically. There is a cronjob running in the server that pulls from bitbucket and syncs the `/public` Hugo directory with the root directory of the web site:

```
cd /home/user/git/ins3cure.com/
git pull
rsync -av --delete /home/user/git/ins3cure.com/public/ /web_directory/html/
```
