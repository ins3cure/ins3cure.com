---
title: "Scanning hosts with nmap (and other tools)"
date: 2021-01-03T19:53:51+01:00
url: scanning-hosts-with-nmap
author: /me
categories: ['blog']
tags: ['nmap', 'scan']
draft: false
---

No matter whether you a running a CTF challenge or performing a real life pentest exercise, chances are you will need to use nmap over and over again. There are a ton of options, which ones to use?

<!--more-->

Let's run a "real" example: the [lame](https://www.hackthebox.eu/home/machines/profile/1) machine from [HackTheBox](https://www.hackthebox.eu/). Let's start running `nmap` with no argumentes (except target IP, of course):

```sh
user@kali:lame $ nmap 10.10.10.3
Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-03 20:46 CET
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 3.06 seconds
```

Why didn't it work? It is because if `nmap` runs as user, it uses <kbd>-sT</kbd> (TCP Connect) option while a privileged scan (run as root) uses <kbd>-sT</kbd> (TCP SYN method). So we can either:

add <kbd>-Pn</kbd> to the scan:

```sh
user@kali:lame $ nmap -Pn 10.10.10.3                                                  130 ⨯
Host discovery disabled (-Pn). All addresses will be marked 'up' and scan times will be slower.
Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-03 20:49 CET
Nmap scan report for 10.10.10.3
Host is up (0.13s latency).
Not shown: 996 filtered ports
PORT    STATE SERVICE
21/tcp  open  ftp
22/tcp  open  ssh
139/tcp open  netbios-ssn
445/tcp open  microsoft-ds

Nmap done: 1 IP address (1 host up) scanned in 16.41 seconds
```

or run with sudo:

```sh
user@kali:lame $ sudo nmap 10.10.10.3                                                      130 ⨯
Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-03 20:50 CET
Nmap scan report for 10.10.10.3
Host is up (0.15s latency).
Not shown: 996 filtered ports
PORT    STATE SERVICE
21/tcp  open  ftp
22/tcp  open  ssh
139/tcp open  netbios-ssn
445/tcp open  microsoft-ds

Nmap done: 1 IP address (1 host up) scanned in 12.17 seconds
```

Results are the same but the second option seems to be slightly faster and is of course more _stealth_.

A very important note: _only first 1000 TCP ports are scanned with any of the above options_. But we may want to run a full scan, right? In that case we can use these options:

```sh
user@kali:lame $ sudo nmap -p- 10.10.10.3
Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-03 20:54 CET
Nmap scan report for 10.10.10.3
Host is up (0.13s latency).
Not shown: 65530 filtered ports
PORT     STATE SERVICE
21/tcp   open  ftp
22/tcp   open  ssh
139/tcp  open  netbios-ssn
445/tcp  open  microsoft-ds
3632/tcp open  distccd

Nmap done: 1 IP address (1 host up) scanned in 505.31 seconds
```

Of course the scan took significantly longer. Notice we found a new port that stayed under the radar with the default options, but this may not always happen. And there are times where it is necessary to run a full scan just in case...

But the scan above is TCP only, what if we need to check por UDP as well? We will have to add the <kbd>-sU</kbd> option and wait even longer time. Since there are no connections, UDP scans may take notably longer since there is to wait for timeouts and retries in order to ensure a port is responding or not. But we have some control over time with argument <kbd>-Tn</kbd> where `n`ranges from 0 (paranoid) to 5 (insane). Paranoid (0) and sneaky (1) are terribly slow because they are intended for IDS evasion. Polite (2) is still quite slow but workable. Normal (3) is the default value while aggresive (4) is usually fine with fast and reliable networks. Finally insane (5) is the fastest but prone to flase positives.

This scan runs on default ports for both TCP and UDP and is a bit faster than the default:

```sh
Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-03 21:07 CET
Nmap scan report for 10.10.10.3
Host is up (0.13s latency).
Not shown: 997 open|filtered ports, 996 filtered ports
PORT    STATE  SERVICE
21/tcp  open   ftp
22/tcp  open   ssh
139/tcp open   netbios-ssn
445/tcp open   microsoft-ds
22/udp  closed ssh
139/udp closed netbios-ssn
445/udp closed microsoft-ds

Nmap done: 1 IP address (1 host up) scanned in 19.31 seconds
```

## Beyond open ports

Finding open ports is very valuable information but `nmap` is much more than that. It can run more thorough scan and scripts to find out a lot more information.

For instance, <kbd>-sC</kbd> will run a default set of scripts. <kbd>-sV</kbd>, in turn, will perform a version detection. Let's see some examples with a reduced number of ports to save some time:

Run version detections

```sh
user@kali:lame $ sudo nmap -sV -p 139,445 10.10.10.3
Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-03 21:57 CET
Nmap scan report for 10.10.10.3
Host is up (0.13s latency).

PORT    STATE SERVICE     VERSION
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 12.17 seconds
```

Run default scripts:

```sh
user@kali:lame $ sudo nmap -sC -p 139,445 10.10.10.3
Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-03 21:57 CET
Nmap scan report for 10.10.10.3
Host is up (0.14s latency).

PORT    STATE SERVICE
139/tcp open  netbios-ssn
445/tcp open  microsoft-ds

Host script results:
|_clock-skew: mean: 2h34m11s, deviation: 3h32m08s, median: 4m10s
| smb-os-discovery: 
|   OS: Unix (Samba 3.0.20-Debian)
|   Computer name: lame
|   NetBIOS computer name: 
|   Domain name: hackthebox.gr
|   FQDN: lame.hackthebox.gr
|_  System time: 2021-01-03T16:01:47-05:00
| smb-security-mode: 
|   account_used: <blank>
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb2-time: Protocol negotiation failed (SMB2)

Nmap done: 1 IP address (1 host up) scanned in 44.46 seconds
```

<kbd>-A</kbd> will run a compmlete set of options: enables OS detection (-O), version scanning (-sV), script scanning (-sC) and traceroute (--traceroute):

```sh
user@kali:lame $ sudo nmap -A -p 139,445 10.10.10.3
Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-03 21:58 CET
Nmap scan report for 10.10.10.3
Host is up (0.13s latency).

PORT    STATE SERVICE     VERSION
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 3.0.20-Debian (workgroup: WORKGROUP)
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Device type: WAP|broadband router|remote management|printer|general purpose|specialized
Running (JUST GUESSING): Linux 2.4.X|2.6.X (92%), Arris embedded (92%), Control4 embedded (92%), Dell embedded (92%), Linksys embedded (92%), Tranzeo embedded (92%), Xerox embedded (92%), Citrix XenServer 5.X (92%)
OS CPE: cpe:/o:linux:linux_kernel:2.4.36 cpe:/h:dell:remote_access_card:6 cpe:/h:linksys:wet54gs5 cpe:/h:tranzeo:tr-cpq-19f cpe:/h:xerox:workcentre_pro_265 cpe:/o:linux:linux_kernel:2.4 cpe:/o:linux:linux_kernel:2.6.18 cpe:/o:citrix:xenserver:5.5
Aggressive OS guesses: DD-WRT v24-sp1 (Linux 2.4.36) (92%), OpenWrt White Russian 0.9 (Linux 2.4.30) (92%), Arris TG862G/CT cable modem (92%), Control4 HC-300 home controller (92%), Dell Integrated Remote Access Controller (iDRAC6) (92%), Linksys WET54GS5 WAP, Tranzeo TR-CPQ-19f WAP, or Xerox WorkCentre Pro 265 printer (92%), Linux 2.4.21 - 2.4.31 (likely embedded) (92%), Citrix XenServer 5.5 (Linux 2.6.18) (92%), Linux 2.6.27 - 2.6.28 (92%), Linux 2.6.8 - 2.6.30 (92%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops

Host script results:
|_clock-skew: mean: 2h34m12s, deviation: 3h32m10s, median: 4m10s
| smb-os-discovery: 
|   OS: Unix (Samba 3.0.20-Debian)
|   Computer name: lame
|   NetBIOS computer name: 
|   Domain name: hackthebox.gr
|   FQDN: lame.hackthebox.gr
|_  System time: 2021-01-03T16:03:10-05:00
| smb-security-mode: 
|   account_used: <blank>
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb2-time: Protocol negotiation failed (SMB2)

TRACEROUTE (using port 139/tcp)
HOP RTT       ADDRESS
1   132.52 ms 10.10.14.1
2   134.49 ms 10.10.10.3

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 56.95 seconds
```

## Log output

`nmap` can write its output in different formats:

* <kbd>-oN</kbd>. Normal output.
* <kbd>-oX</kbd>. XML.
* <kbd>-oG</kbd>. Grepable format.
* <kbd>-oA</kbd>. All formats.

There is also a <kbd>-oS</kbd>. s|<rIpt kIddi3 mode for... fun?:

```sh
$taRting Nmap 7.91 ( hTtpz://nmAp.org ) at 2021-01-03 22:23 C3T
Nmap $cAn rep0rt foR 10.10.10.3
HOst is up (0.22z latency).

p0rT    $TATe $ERviC3
139/tcp 0pen  n3tbiOz-ssN
445/tcp 0P3n  M!crO$oft-ds

NmaP done: 1 IP aDdr3sS (1 h0$t up) $Cann3d in 0.69 $3cONdz
```

## Summary

For a start I use to use these settings:

```sh
sudo nmap -sC -sV -oN lame.nmap 10.10.10.3
```


## Another useful tool: masscan

`masscan` is another usefult tool for scanning. Designed to be fast, it can scan the whole internet in under 6 minutes, transmitting 10 million packets per second, from a single machine, [according to it own description](https://github.com/robertdavidgraham/masscan).

`Since` masscan is so fast some people like to do an initial complete scan with it and then a detailed scan with `nmap` on the discovered ports. This way:

```sh
user@kali:lame $ sudo masscan -p1-65535,U:1-65535 --rate=1000 -e tun0 10.10.10.3
Starting masscan 1.0.5 (http://bit.ly/14GZzcT) at 2021-01-03 21:38:21 GMT
 -- forced options: -sS -Pn -n --randomize-hosts -v --send-eth
Initiating SYN Stealth Scan
Scanning 1 hosts [131070 ports/host]
Discovered open port 139/tcp on 10.10.10.3                                     
Discovered open port 3632/tcp on 10.10.10.3                                    
Discovered open port 22/tcp on 10.10.10.3                                      
Discovered open port 445/tcp on 10.10.10.3                                     
Discovered open port 21/tcp on 10.10.10.3                                      
```

...and the:

```sh
user@kali:lame $ sudo nmap -A -T4 -p 139,3632,22,445,21 10.10.10.3                         130 ⨯
Starting Nmap 7.91 ( https://nmap.org ) at 2021-01-03 23:02 CET
Nmap scan report for 10.10.10.3
Host is up (0.18s latency).

PORT     STATE SERVICE     VERSION
21/tcp   open  ftp         vsftpd 2.3.4
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
|_Can't get directory listing: PASV failed: socket TIMEOUT
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to 10.10.14.15
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      vsFTPd 2.3.4 - secure, fast, stable
|_End of status
22/tcp   open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
| ssh-hostkey: 
|_  2048 56:56:24:0f:21:1d:de:a7:2b:ae:61:b1:24:3d:e8:f3 (RSA)
139/tcp  open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp  open  netbios-ssn Samba smbd 3.0.20-Debian (workgroup: WORKGROUP)
3632/tcp open  distccd     distccd v1 ((GNU) 4.2.4 (Ubuntu 4.2.4-1ubuntu4))
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Aggressive OS guesses: OpenWrt White Russian 0.9 (Linux 2.4.30) (92%), D-Link DAP-1522 WAP, or Xerox WorkCentre Pro 245 or 6556 printer (92%), Dell Integrated Remote Access Controller (iDRAC6) (92%), Linksys WET54GS5 WAP, Tranzeo TR-CPQ-19f WAP, or Xerox WorkCentre Pro 265 printer (92%), Linux 2.4.21 - 2.4.31 (likely embedded) (92%), Citrix XenServer 5.5 (Linux 2.6.18) (92%), Linux 2.6.8 - 2.6.30 (92%), Dell iDRAC 6 remote access controller (Linux 2.6) (92%), Supermicro IPMI BMC (Linux 2.6.24) (92%), ZyXEL NSA-200 NAS device (92%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 2 hops
Service Info: OSs: Unix, Linux; CPE: cpe:/o:linux:linux_kernel

Host script results:
|_clock-skew: mean: 2h34m14s, deviation: 3h32m08s, median: 4m13s
| smb-os-discovery: 
|   OS: Unix (Samba 3.0.20-Debian)
|   Computer name: lame
|   NetBIOS computer name: 
|   Domain name: hackthebox.gr
|   FQDN: lame.hackthebox.gr
|_  System time: 2021-01-03T17:06:48-05:00
| smb-security-mode: 
|   account_used: <blank>
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
|_smb2-time: Protocol negotiation failed (SMB2)

TRACEROUTE (using port 21/tcp)
HOP RTT       ADDRESS
1   149.31 ms 10.10.14.1
2   149.66 ms 10.10.10.3

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 71.31 seconds
```


# References

- [nmap.org](https://nmap.org/)
- [nmap cheat sheet](https://hackertarget.com/nmap-cheatsheet-a-quick-reference-guide/)
- [masscan](https://github.com/robertdavidgraham/masscan)
- [https://explainshell.com/](explainshell.com). Not nmap specific, it's an amazing way of reading man pages.
- A Hak5 video about advanced nmap techniques with [Shannon Morse (@Snubs)](https://twitter.com/Snubs) and [Dan Tentler (@Viss)](https://twitter.com/Viss)

{{< youtube 7XMIFTRiAGA >}}