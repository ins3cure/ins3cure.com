---
title: "Homebrew"
date: 2020-11-26T07:32:03+01:00
url: homebrew
categories: ['blog']
tags: ['homebrew','macos']
draft: false
---
What after installing homebrew packages? Your macOS will not keep track of homebrew updates, so you must do it yourself. This little script will help.

<!--more-->

We all know that we must keep our software up to date; not only because vendors continuously release new and facy features but, more importantly, because the provide security updates -hopefully-.

If you have a Mac you are probably familiar with the software update process

![macOS updates](/img/macos-mojave-updates.jpg)

If you own a Mac, chances are that you have installed [homebrew](https://brew.sh). Homebrew is some kind of package manager that provides software that you may be missing in a Mac (and also for Linux but who cares?)

For instance, if you want a non-stripped-off, up to date python version, or an open source GPG version, you will most likely install it from homebrew.

Adding homebrew to you Mac is really easy:

{{< highlight shell >}}
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
{{< /highlight >}}

And the install the software you need:

{{< highlight shell >}}
brew install gnupg
{{< /highlight >}}

Unfortunately, the macOS software updates do not notify of homebrew updates - you have to check by your own means. The most straightforward way is using the comman line:

{{< highlight shell >}}
brew update
brew outdated
{{< /highlight >}}

{{< alert warning >}}
Note it is important to `update` first!
{{< /alert >}}

If something is outdated just update everything or a specific formula:

{{< highlight shell >}}
brew upgrade
brew upgrade <formula>
{{< /highlight >}}

## Formulae vs casks

Before continuing, let me explain the basic difference between formulae and casks:

- *Formulae* are Ruby scripts that describe how to download, compile and install software. Typìcally command line opensource software
- *Casks* is some kind of extension that allows to install GUI applications, proprietary software and binaries

### Some other homebrew terms

- *Tap*. A tap is a source of *formulae* (and maybe *casks* as well?). Default *tap* is `homebrew/core` but there are third party taps.
- *Cellar*. *Cellar* is the directory where homebrew installs stuff. After installation, homebrew creates a link in `/usr/local/bin`.

## Back to updates

Some time ago I noticed that `brew outdated` did not report some of my outdated packages. The reason was they were *casks*. So if you have installed some casks you mussst also run:

{{< highlight shell >}}
brew outdated --cask
{{< /highlight >}}

{{< alert info >}}
Note it is important to `update` first!
{{< /alert >}}


## Automating and getting notified

Since it is really important to keep our software up to date, it will be useful to set a reminder to upgrade our homebrew packages.

We can use this script:

{{< highlight shell >}}
#!/bin/zsh

# Need to add /usr/local/bin to PATH
PATH=/usr/local/bin:$PATH

BREW=brew

if ! command -v $BREW &> /dev/null
then
    echo "ERROR: $BREW not found" >&2
    exit 1
fi

$BREW update >/dev/null
OUTDATED_F=$($BREW outdated | wc -l)
OUTDATED_C=$($BREW outdated --cask | wc -l)
OUTDATED=$((OUTDATED_F+OUTDATED_C))

if [[ $OUTDATED -gt 0 ]]; then
	MSG="You have $OUTDATED outdated packeges\nPlease check your homebrew installation"
	osascript -e "display notification \"$MSG\" with title \"Homebrew\""
fi
{{< /highlight >}}

that will display a standard macOS notifications if there are packages out of date.

To run automatically once a day we can use `launchd`. So let's create a plist file:

{{< highlight xml >}}
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>Label</key>
        <string>brew.updates.notification</string>
        <key>Program</key>
        <string>/usr/local/bin/brew-check-update.sh</string>
        <key>RunAtLoad</key>
        <true/>
        <key>StartCalendarInterval</key>
        <dict>	
            <key>Hour</key>
            <integer>9</integer>
            <key>Minute</key>
            <integer>0</integer>
        </dict>	
    </dict>
</plist>
{{< /highlight >}}

And load it:

{{< highlight shell >}}
launchctl bootstrap gui/501/brew.updates.notification
{{< /highlight >}}

Now the script should run every 9:00 in the morning and warn us if we must run `brew upgrade`.

## References

- [Homebrew documentation](https://docs.brew.sh/Manpage)
- [Daemons and Services Programming Guide](https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/Introduction.html#//apple_ref/doc/uid/10000172i-SW1-SW1)