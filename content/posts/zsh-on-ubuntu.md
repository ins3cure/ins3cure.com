---
title: "zsh on Ubuntu"
date: 2021-01-28T18:30:18+01:00
url: /zsh-on-ubuntu
author: /me
categories: ['blog']
tags: ['linux', 'ubuntu', 'zsh']
draft: false
---

Setting zsh as my default shell on Ubuntu 20.04 LTS because why not

<!--more-->

Still puzzled about the Red Hat/IBM decision to f*ck up CentOS project, I migrated my ThinkPad to Ubuntu. Default shell for Ubuntu 20.04 is still bash, so I have to manually change it if I want to use zsh.

First things first:

```bash
$ sudo apt -y install zsh
```

I'm already using [oh my zsh](https://github.com/ohmyzsh/ohmyzsh) on may Mac, so why not:

```sh
$ sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
Cloning Oh My Zsh...
Cloning into '/home/luis/.oh-my-zsh'...
remote: Enumerating objects: 1194, done.
remote: Counting objects: 100% (1194/1194), done.
remote: Compressing objects: 100% (1162/1162), done.
remote: Total 1194 (delta 20), reused 1131 (delta 16), pack-reused 0
Receiving objects: 100% (1194/1194), 827.50 KiB | 2.11 MiB/s, done.
Resolving deltas: 100% (20/20), done.

Looking for an existing zsh config...
Using the Oh My Zsh template file and adding it to ~/.zshrc.

Time to change your default shell to zsh:
Do you want to change your default shell to zsh? [Y/n] y
Changing the shell...
Password: 
Shell successfully changed to '/usr/bin/zsh'.

         __                                     __
  ____  / /_     ____ ___  __  __   ____  _____/ /_
 / __ \/ __ \   / __ `__ \/ / / /  /_  / / ___/ __ \
/ /_/ / / / /  / / / / / / /_/ /    / /_(__  ) / / /
\____/_/ /_/  /_/ /_/ /_/\__, /    /___/____/_/ /_/
                        /____/                       ....is now installed!


Before you scream Oh My Zsh! please look over the ~/.zshrc file to select plugins, themes, and options.

• Follow us on Twitter: https://twitter.com/ohmyzsh
• Join our Discord server: https://discord.gg/ohmyzsh
• Get stickers, shirts, coffee mugs and other swag: https://shop.planetargon.com/collections/oh-my-zsh

➜  ~ 
``` 

Note you have to supply the sudo password to update the default shell

I only have to add:

```sh
# My customization
PROMPT='%F{2}%n@%B%F{cyan}%1~%f%b %# '
export EDITOR=/usr/bin/vi
set -o vi
```

to the `.zshrc` file.


I also installed the [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md):

```sh
% git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

Cloning into '/home/luis/.oh-my-zsh/custom/plugins/zsh-autosuggestions'...
remote: Enumerating objects: 11, done.
remote: Counting objects: 100% (11/11), done.
remote: Compressing objects: 100% (10/10), done.
remote: Total 2396 (delta 2), reused 3 (delta 1), pack-reused 2385
Receiving objects: 100% (2396/2396), 545.28 KiB | 925.00 KiB/s, done.
Resolving deltas: 100% (1533/1533), done.
```

... and modified the `.zshrc` file accordingly:

```sh
plugins=(git zsh-autosuggestions)
```

Have fun...