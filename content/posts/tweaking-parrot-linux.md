---
title: "Tweaking Parrot Linux"
date: 2020-10-02
url: tweaking-parrot-linux
categories: ['blog']
tags: ['linux', 'parrot']
summary: A few stupid tweaks to add after installing Parrot Security
---

A few stupid tweaks to add after installing Parrot Security

Add to ~/.bashrc:

```
# my bash prompt
# export PS1="\e[0;31;1m[\u\e[m@\e[0;34;1m\h\e[m \W]\$ "
# export PS1='\[\e[0;36m\]\u\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[1;32m\]\$\[\e[m\]\[\e[1;37m\] '
export PS1='\[\e[0;39m\]\u\[\e[m\] \[\e[1;34m\]\w\[\e[m\] \[\e[1;32m\]\$\[\e[m\] '
stty -echoctl
```

Get rid of neovim that confuses me and install vim instead:

```
sudo dpkg --purge neovim neovim-runtime
sudo apt install vim
```

Add to ~/.vimrc:

```
set tabstop=4
filetype on
set nu
set ruler
" set mouse=a
set list
```

Reorder windows buttons:

```
gsettings set org.mate.Marco.general button-layout "'close,minimize,maximize'"
```

Add FoxyProxy plugin to Firefox

![FoxyProxy](/img/foxyproxy.png)

