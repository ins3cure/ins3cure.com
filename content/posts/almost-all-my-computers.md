---
title: "(Almost) all my computers"
date: 2019-12-27
url: almost-all-my-computers
author: /me
categories: ['blog']
tags: ['computers']
---

A quick tour through most of the computers I remember to have owned in my life.

<!--more-->

This afternoon I came across a picture of a Toshiba T3200, which happend to be my first Intel computer. I ended up trying to remember all the computers I have owned.

After more than 30 years of computers there are some mistakes and missing pieces, including some PCs built from spare parts. This is the list:

## Sinclair ZX Spectrum

This is where it all begun... from getting to know computing basics to mastering Sinclair BASIC. How many hours spent, struggling with the dreaded R Tape loading error

I used it extensively for a long time. But it was slowly abandoned as i grew up. But it probably changed my life...

Unfortunately I lost track of this amazing little computer. I wish I had been more careful with some of these devices.

![ZX Spectrum](/img/zx_spectrum.jpg)

[https://en.wikipedia.org/wiki/ZX_Spectrum](https://en.wikipedia.org/wiki/ZX_Spectrum)

## Toshiba T3200

This machine was absolutely amazing, with its orange plasma monitor. With a 40 MB hard disk I was pretty sure it could last forever.

I can still remember countless hours of Prince of Persia games and even running Windows 3.1 on it!

I sold it to buy my next computer; how much I regret it!

![T3200](/img/Toshiba_T3200.jpg)

[Toshiba T3200 - Computer - Computing History
](https://www.computinghistory.org.uk/det/4435/Toshiba-T3200/)

## Zenith Z-Star

I don't remember a lot about this Zenith Data Systems device. I did not own this one for a long time. Unfortunately it was stolen from my own house by a "friend". However I never loved this machine, with that stupid "ball" on it.

![Zenith Z-Star](/img/Zenith-Z-star.jpg)

## IBM Aptiva

I don't remember the exact model, but probably a 2176. It run Windows 95. I spent a good amount of money to buy OS/2 for this computer. Yes, I was an OS/2 enthusiast!

![IBM Aptiva](/img/IBM_Aptiva.jpg)

## IBM ThinkPad 380

My first ThinkPad was not really owned by me; it was provided by my employer at that time (IBM) and it was an absolutely amazing piece of technology, from the insane display resolution to the perfect keyboard and, of course, the TrackPoint.

A ThinkPad fan since 20 years ago :)

![IBM ThinkPad 380](/img/IBM_ThinkPad-380.jpg)

## IBM ThinkPad iSeries 1200

This one was shipped with the infamous Windows ME. I lent it to a cousin who went abroad to study and he never gave it back.

![IBM iSeries 1200](/img/IBM_iSeries_1200.gif)

[https://www-01.ibm.com/common/ssi/rep_ca/7/897/ENUS101-187/ENUS101-187.PDF](https://www-01.ibm.com/common/ssi/rep_ca/7/897/ENUS101-187/ENUS101-187.PDF)

## IBM ThinkPad T30

Again an IBM provided machine - it replaced my old 380. PC Shop provided some Windows flavour but I installed Red Hat Enterprise Linux (yes, we could get RHEL licenses).

![IBM ThinkPad T30](/img/IBM_ThinkPad_T30.jpg)

[http://www.thinkwiki.org/wiki/Category:T30](http://www.thinkwiki.org/wiki/Category:T30)

## Acer TravelMate 243LC

Why, when did I get this computer? I don't know... But I remember one thing about this laptop; I used to travel with it during my IBM tenure. In my free time I used it to build a Linux from Scratch. I learned a lot with it.

It survived an open window under the rain and the lid is deformed because it was too close to a light bulb. Funny enough it still boots its Windows XP :)

However I don't think I would buy an Acer computer anymore.

![Acer TravelMate 240](/img/Acer_TravelMate_240.jpg)

[https://www.acer.com/ac/en/ID/content/support-product/765](https://www.acer.com/ac/en/ID/content/support-product/765)

## IBM ThinkPad R51

One of the last ThinkPads built by IBM before selling the brand to Lenovo.

Released in 2004 I probably bought it a couple of years later taking advantage of the discount for IBM employees. Shipped with Windows XP, it run Linux most of the time. I learned quite a lot with this computer, which run a personal web page and mail server 24x7 for years.

I went to a lot of upgrades until the display broke, but it still works and I keep it in a storage room just in case.

![IBM ThinkPad R51](/img/IBM_ThinkPad_R51.jpg)

[http://www.thinkwiki.org/wiki/Category:R51](http://www.thinkwiki.org/wiki/Category:R51)

## Packard Bell EasyNote BU45

When my ThinkPad R51 screen broke I needed a new computer urgently so I run to a shopping center and got this one.

Probably one of the worst computers I have ever owned... It didn't take too long until I ordered a new ThinkPad.

![Packard Bell BU45](/img/Packard-Bell_BU45.jpg)

## Lenovo IdeaPad S10e

Very cheap (less than 100€ with a special offer), a lot of coworkers bought one. It had a decent battery life and was very light and portable. Keyboard was quite decent for the price once you got used to it. And it had Linux (SuSE) preinstalled!

Unfortunately I dropped it while having lunch at the office and broke the screen. I sold it to a colleague that had a working display and wanted to restore his unit.

![Lenovo IdeaPad S10e](/img/Lenovo_IdeaPad_S10e.jpg)

## Asus EeePC

A cheap, light but durable machine. Bought it to replace my broken netbook. Upgraded with an SSD and 2 GB RAM, it is still being used by my oldest son at the university.

![Asus EeePc](/img/Asus-EeePC.png)

[https://en.wikipedia.org/wiki/Asus_Eee_PC](https://en.wikipedia.org/wiki/Asus_Eee_PC)

## Lenovo ThinkPad Edge E130

At that time I was using a company ThinkPad Edge E330 for work. In 2020 it must sound weird but it could easily run some virtual machine thanks to the VT-x virtualization technology of it Intel Core i3. So I decided to get a very similar laptop but with a smaller form factor (11" instead of 13"). I never enjoyed it too much, but it still works with upgraded disk and memory. Battery was soon dead though. Today it runs a tiny home lab.

Update: during the COVID-19 pandemic was donated to some kids that needed it for online classes.

![Lenovo ThinkPad Edge e130](/img/Lenovo_ThinkPad_Edge_e130.jpg)

## Intel NUC DN2820FYKH

A very nice barebone. As usual, I bought it cheap which turned out to be a mistake. It is a very nice machine but while the Intel Celeron processor was enough to run a multimedia center, it fell short for other uses like virtualization. A Core i5 would work as a small home lab.

It's been successfully running as a media center since I got it.

![Intel NUC](/img/Intel_NUC.jpg)

[https://ark.intel.com/content/www/us/en/ark/products/78953/intel-nuc-kit-dn2820fykh.html](https://ark.intel.com/content/www/us/en/ark/products/78953/intel-nuc-kit-dn2820fykh.html)

## MacBook Air (11-inch, Early 2014)

My first MacBook. As a long-time Apple hater, I finally decided to get a MacBook to test by myself. And I did really LOVE IT. Great hardware with a nice keyboard on an extremely light machine. And it was very easy to get used to the OS X.

It is still being used (and abused) by my oldest son for the University. A wonderful piece of technology.

![MacBook Air (11-inch, Early 2014)](/img/macbook-air-2013-2014-11.jpg)

[https://support.apple.com/kb/sp699](https://support.apple.com/kb/sp699)

## MacBook Pro 13" Retina 2015

After the successful MacBook Air, I decided to upgrade to a MacBook Pro. My choice was the 13" Retina 2015. And this is one of my all-time favourite computers.

I sold it (for a good price) to get the new MacBook Pro 2017. Not sure if it was a good move.

![MacBook Pro 13" Retina 2015](/img/MacBook_Pro_Retine_13.jpg)

## Raspberry Pi 3B

After the powerful MacBook Pro, I bought an inexpensive Raspberry Pi 3B. I used it teach some computing basics to my youngest son but it has been a multimedia station, a storage server an IDS device and a retro game emulator among other things.

About this time I also bought an Arduino Uno, another enjoyable piece of hardware but I will not add it to this list of computers.

A lot of fun with this little device...

![Raspberry Pi 3B](/img/raspio-portsplus.jpg)

## Lenovo ThinkPad X220

Got this machine second hand in a very good condition. The last ThinkPad with the perfect keyboard.

With a Core i5 processor, an SSD disk and upgraded to 8 GB RAM it is still a beast. It takes just a few seconds to change the SSD disk so my youngest kid can use either Ubuntu or Windows 10 depending on his needs.

![Lenovo ThinkPad X220](/img/Lenovo_ThinkPad_x220.jpg)

## MacBook Pro 13" 2017

Being a wonderful machine, I don't think I will ever love it as much as the previous 2015 MacBook Pro. Unfortunately, I will never get used to the new keyboard.

I have the space grey model without touchbar.

![MacBook Pro 13" 2017](/img/MacBook_Pro_13_2017.jpg)

## Lenovo ThinkPad X270

In 2018 I found a good deal for this 2017 ThinkPad and I couldn't resist. As a ThinkPad lover, I needed a modern ThinkPad.

Of course NVMe disks are great, but I miss the x220 feature to easily switch disks.

![Lenovo ThinkPad X270](/img/Lenovo_ThinkPad_x270.jpg)

## Microsoft Surface Go

Even I was surprised to get this computer. But it turned out to be a quite smart choice for short trips or events where a full-blown computer is not needed.

![Microsoft Surface Go](/img/Microsoft_Surface_Go.jpg)

## HP Elitedesk 800 G1 mini

The latest addition is a (more or less) 20x20 cm "mini PC". I bought it refurbished, just as new but very cheap. After upgrading the disk it is running a Proxmox home lab.

![HP Elitedesk 800 G1 mini](/img/HP_EliteDesk_800_G1.gif)

## MacBook Pro 13" 2020

My fourth MacBook (third Pro). Tired of the awful butterfly keyboard I sold my 2017 MacBook (again for a good price) and bought the 2020 version with the "new old" keyboard. With a 10th Gen i5 and 16 GB memory is the most powerful laptop I've ever owned.

The biggest drawback is the Touch Bar, but there was no choice so I will have to live with it 

![MacBook Pro 13" 2020](/img/MacBook_Pro_13_2020.jpg)

## Bonus track: Univac 1100

No, of course I didn't own a Univac 1100. But it is one of my first memories of a computer, when my dad took me to his workplace in a bank and let me play with those wonderful mechanical keyboards, monochrome green monitors, huge and heavy magnetic disks (with a capacity that would make us laught today), enormous and noisy printers and even punched cards.

Everything was magic, like being in a sci-fi movie. There is no doubt those days had an impact in my decision to make a living from computing.

![Univac 1100](/img/Univac_1100_2.jpg)