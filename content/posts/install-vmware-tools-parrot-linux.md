---
title: "Install VMware tools in Parrot Linux"
date: 2020-12-06T16:19:38+01:00
url: install-vmware-tools-parrot-linux
author: /me
categories: ['blog']
tags: ['linux', 'parrot', 'vmware']
draft: true
---

Parrot Linux is the currently supported Linux distro by Hackthebox. I have a Parrot virtual machine in my Mac. This is how to install the VMware tools.

<!--more-->

At the time I upgraded my Mac to Big Sur, there were some issues with Oracle VirtualBox on the new macOS version.

I did not use VMware virtualization tools in my personal computers because it was either expensive or too limited in functionality (or both) but nowadays VMware Fusion 12 is free for personal use. Of course it's worth testing.

After installing the OS...