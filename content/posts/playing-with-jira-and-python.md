---
title: "Playing With Jira and Python"
date: 2020-06-04
url: playing-with-jira-and-python
author: /me
categories: ['blog']
tags: ['code', 'jira', 'python']
summary: a piece of code to script Jira tickets
---

I like Atlassian products.

So I use Confluence, Bitbucket and Jira for my personal projects. You can ask – isn't it overkill? Well, it would really be overkill if it were not for the fact that Atlassian offers free instances for small teams of most of their products.

(Btw I run my own local instance of Confluence, it costs $10 that are donated to a charity).

Jira is a wonderful tool but sometimes I need something easier; for instance, a single main every morning with my pending tasks that I read in my mobile while I enjoy my first tea in the morning. Well, there is a library for this.

# Creating the object

First of all we need a Jira object:

    
{{< highlight python >}}
from jira import JIRA
jira = JIRA()
{{< /highlight >}}

Hmmm, not very useful. You probably want to login to a different instance living on a server other than localhost:2990. Let's try again:

{{< highlight python >}}
from jira import JIRA

options = {
	'server': 'https://' + server
}
jira = JIRA(options, basic_auth=(user, api_key))
{{< / highlight >}}

Note that you will need the api_key, not the password!

Easy! Now you can get the list of projects in your instance:

{{< highlight python >}}
projects = list_projects(jira)
for project in projects:
   print(project)
{{< /highlight >}}

And of course search for issues with the usual Jira filters:

{{< highlight python >}}
filter = 'assignee = currentuser() and status != Done order by priority desc'
issues = jira.search_issues(filter)
{{< /highlight >}}

All together:

{{< highlight python >}}
from jira import JIRA

def list_issues(jira, filter=""):
    issues = jira.search_issues(filter)
    return issues


def report_issues(issues):
    report = '''
        <p>Good morning! Your tasks for today:</p>\n
        <table width=”100%” border=”0”>\n
        <tr><td colspan=5 width="80">\n
        <tr bgcolor="#dddddd"><td>Issue ID<td>Status<td>Priority<td>Due date<td>Summary
        '''
    for issue in issues:
        issue_key = issue.key
        priority = str(issue.fields.priority)
        status = issue.fields.status.name
        summary = issue.fields.summary[:64]
        duedate = issue.fields.duedate
        if duedate is None:
            duedate = '-'
        report += "<tr><td>{}<td>{}<td>{}<td>{}<td>{}\n".format(issue_key, status, priority, duedate, summary)
    report += '</table>\n<p>Have a nice day!<p>Sincerely, your team.'
    return report


def mail_issues(text):
	[...]
    

options = {
	'server': 'https://' + server
}
jira = JIRA(options, basic_auth=(user, api_key))

filter = 'assignee = currentuser() and status != Done order by priority desc'
issues = list_issues(jira, filter)
report = report_issues(issues)
mail_issues(report)
{{< /highlight >}}

Now [read the Python Jira](https://jira.readthedocs.io/) docs and play!

