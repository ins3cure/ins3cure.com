---
title: jq cheatsheet
date: 2020-06-07
url: jq-cheatsheet
categories: ['cheatsheet']
tags: ['jq','json']
summary: or how to survive to JSON in the command line
---

JSON is everywhere.

It's very common to run `curl` requests that return a JSON object. Example:

```
curl -s http://dummy.restapiexample.com/api/v1/employees
```

But the output may look like garbage:

```
{"status":"success","data":[{"id":"1","employee_name":"Tiger Nixon","employee_salary":"320800","employee_age":"61","profile_image":""},{"id":"2","employee_name":"Garrett Winters","employee_salary":"170750","employee_age":"63","profile_image":""},{"id":"3","employee_name":"Ashton Cox","employee_salary":"86000","employee_age":"66","profile_image":""},{"id":"4","employee_name":"Cedric Kelly","employee_salary":"433060","employee_age":"22","profile_image":""},{"id":"5","employee_name":"Airi Satou","employee_salary":"162700","employee_age":"33","profile_image":""},{"id":"6","employee_name":"Brielle Williamson","employee_salary":"372000","employee_age":"61","profile_image":""},{"id":"7","employee_name":"Herrod Chandler","employee_salary":"137500","employee_age":"59","profile_image":""},[...]{"id":"24","employee_name":"Doris Wilder","employee_salary":"85600","employee_age":"23","profile_image":""}]}
```

(Warning, edited to make it shorter)

`jq` is a small tool to make JSON output readable for humans:

{{< highlight json >}}
curl -s http://dummy.restapiexample.com/api/v1/employees|jq
{
  "status": "success",
  "data": [
    {
      "id": "1",
      "employee_name": "Tiger Nixon",
      "employee_salary": "320800",
      "employee_age": "61",
      "profile_image": ""
    },
    {
      "id": "2",
      "employee_name": "Garrett Winters",
      "employee_salary": "170750",
      "employee_age": "63",
      "profile_image": ""
    },
[...]
{{< / highlight >}}

There is no doubt `jq` makes our lives easier, but I never, NEVER remember the basic syntax.

# Basic filters

## Properties

Dot filter "." is the basic filter that just colorizes and pretty-prints the JSON object:

{{< highlight json >}}
echo '{"key1": "value1", "key2": "value2"}'|jq .
{
  "key1": "value1",
  "key2": "value2"
}
{{< /highlight >}}

You can get a property:

```
$ echo '{"key1": "value1", "key2": "value2"}' | jq .key2
"value2"
```

Access nested properties:

{{< highlight json >}}
$ echo '{"key1": "value1", "key2": {"subkey1": "value21", "subkey2": "value22"}}' | jq .
{
  "key1": "value1",
  "key2": {
    "subkey1": "value21",
    "subkey2": "value22"
  }
}

$ echo '{"key1": "value1", "key2": {"subkey1": "value21", "subkey2": "value22"}}' | jq .key2.subkey2
"value22"
{{< / highlight >}}


## Arrays

"[]" removes the array and lists elements:

{{< highlight json >}}
$ echo '[{"k": "v1"}, {"k": "v2"}]' | jq .
[
  {
    "k": "v1"
  },
  {
    "k": "v2"
  }
]

$ echo '[{"k": "v1"}, {"k": "v2"}]' | jq .[]
{
  "k": "v1"
}
{
  "k": "v2"
}
{{< / highlight >}}

Use indexes:

{{< highlight json >}}
$ echo '[{"k": "v1"}, {"k": "v2"}]' | jq .[1]
{
  "k": "v2"
}

$ echo '[{"k": "v1"}, {"k": "v2"}]' | jq .[1].k
"v2"
{{< /highlight >}}

# Operators

## Pipes

You can use pipes much like shell pipes. This example is pretty much like the "nested properties" one above:

{{< highlight json >}}
$ echo '{"key1": "value1", "key2": {"subkey1": "value21", "subkey2": "value22"}}' | jq '.key2 | .subkey2'
"value22"
{{< /highlight >}}

Be careful to quote the pipes or they will be passed to the shell breaking everything.

## Commas

I'm sure it's useful :)

{{< highlight json >}}
$ echo '{"key": "value"}' | jq '. , .'
{
  "key": "value"
}
{
  "key": "value"
}
{{< /highlight >}}

# Constructors

## Arrays

Create an array

{{< highlight json >}}
$ echo '{"key": "value"}' | jq '[.]'
[
  {
    "key": "value"
  }
]
{{< /highlight >}}

Objects:

{{< highlight json >}}
$ echo '{"key": "value"}' | jq '{"foo": .}'
{
  "foo": {
    "key": "value"
  }
}
{{< /highlight >}}

# Less trivial examples

Let's use http://dummy.restapiexample.com for some more "real life" examples.

## Paging

What if the output is too long and we want to page it? Of course we can use less, but nice coloring will be lost. Not always! Do it this way:

{{< highlight json >}}
$ curl -s http://dummy.restapiexample.com/api/v1/employees|jq -C . | less -R
{{< /highlight >}}

Get employee names:

{{< highlight json >}}
$ curl -s http://dummy.restapiexample.com/api/v1/employees | jq '.data[] | .employee_name'
"Tiger Nixon"
"Garrett Winters"
"Ashton Cox"
[...]
{{< /highlight >}}

Get the structure of a document:

{{< highlight json >}}
$ curl -s http://dummy.restapiexample.com/api/v1/employees | jq -r 'path(..) | map(tostring) | join("/")'
 
status
data
data/0
data/0/id
data/0/employee_name
data/0/employee_salary
data/0/employee_age
data/0/profile_image
data/1
data/1/id
data/1/employee_name
data/1/employee_salary
data/1/employee_age
data/1/profile_image
data/2
data/2/id
[...]
{{< /highlight >}}

# There is much more...

...like functions and variables but I don't usually need them. Read tutorial and documentation:

- https://stedolan.github.io/jq/tutorial/
- https://stedolan.github.io/jq/manual/

There is a short nice talk from a jq expert. Some examples are from this talk:

{{< youtube _ZTibHotSew >}}
