---
title: "macOS Terminal and zsh"
date: 2020-11-16T18:06:47+01:00
url: terminal+zsh
categories: ['blog']
tags: ['macos', 'shell', 'zsh']
summary: Getting rid of iTerm2 while getting used to zsh
---

Since some time ago I use [iTerm2](https://www.iterm2.com) as a replacement for the default macOS Terminal application. I was not very happy because I prefer not to add too many pieces of software but I had a couple of reasons for that:

1. iTerm2 allowed to move between tabs with ⌘+number. This was something I was used to do because of Gnome Terminal
    - You can do this now with macOS default Terminal
2. Copy and paste with the mouse was easier
    - I found out you can use ⌘ + right click. And probably can configure Terminal for a different behaviour
3. iTerm2 was way more configurable
    - This is something I can live without. And I can install `tmux` from brew

Time passed and this is not necessarily true anymore. But another change came in: **zsh** as default shell instead of **bash**, again with its own caveats.

Grrrrr :rage:

One especially annoying issue was that I was not able to delete directories in a path with esc+delete, as I've been doing for years. I really had to fix that!

Let's see if these issues can be fixed.

## Profile and fonts

First things first.

- I duplicated the **Basic** profile as **Personal**
- Make it the default profile
- Changed the font to Monaco 14
- Add path to the window name
- Window size: 80x25
- Shell: when the shell exits:
    - Close the window
- Color
    - Make background black, 90% opacity, 10% blur. More or less :) 
- Default
    - Select Personal on startup

![Terminal Color](/img/TerminalColor.png)

## zsh

### Why zsh?

From macOS Catalina, **zsh** is the default shell for macOS. **bash** is deprecated and, since I spend quite some time in front of a Mac, I decided to accept bash's fate.

I hate frameworks like [zim](https://github.com/zimfw/zimfw) or [oh my zsh](https://github.com/ohmyzsh/ohmyzsh) -they just make things more complicated- so they are not the reason behind using **zsh**.

### tuning zsh

Create a `.zshrc` file:

{{< highlight shell >}}
alias ls="ls -G"
alias ll="ls -l"
PROMPT='%F{2}%n@%B%F{cyan}%1~%f%b %# '
export PATH="/usr/local/opt/python/libexec/bin:$PATH"
{{< /highlight >}}

{{< alert info >}}
The `PATH` line is there because of **python** being installed via homebrew
{{< /alert >}}


## Annoyances and -hopefully- fixes

### Can't delete part of the path with esc+delete

Fixed adding:

{{< highlight shell >}}
WORDCHARS='*?_-.[]~=&;!#$%^(){}<>'
{{< /highlight >}}

to `.zshrc`. The character "/" is removed from the default list of chars that can be part of a word. Remove any other one (i.e. ".") if required.

### Move between words in Terminal

Use ⌥ + arrows (I think this is by default)

## Autocomplete

For the autocomplete feature to work, add this to the `.zshrc`:

```shell
autoload -Uz compinit && compinit
```

{{< alert warning >}}
You may get an error if you have _insecure_ writable directories.
To check and fix this condition:
{{< /alert >}}

```shell
% autoload -Uz compinit && compinit
zsh compinit: insecure directories, run compaudit for list.
Ignore insecure directories and continue [y] or abort compinit [n]?

# Find out the offending directories:
% compaudit
There are insecure directories:
/usr/local/share/zsh/site-functions
/usr/local/share/zsh

# Fix with:
% chmod 755 /usr/local/share/zsh/site-functions /usr/local/share/zsh
```

## Conclusion

Once I upgrade to Big Sur (I usually install from scratch instead of upgrading) I may be able to get rid of iTerm2.

## References

- [Terminal User Guide](https://support.apple.com/guide/terminal/welcome/mac)
- [Moving to zsh](https://scriptingosx.com/2019/06/moving-to-zsh/)

## Update

Funny enough, two days after writing this post I read about [Kali Linux switches to zsh as default shell](https://www.kali.org/news/kali-linux-2020-4-release/). Perfect timing!

