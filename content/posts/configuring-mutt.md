---
title: "Configuring mutt"
date: 2020-06-12
url: configuring-mutt
categories: ['blog']
tags: ['linux', 'macos', 'mutt']
summary: Believe it or not, there are (smart) people using command line mail clients in 2020.
---

Believe it or not, there are (smart) people using command line mail clients in 2020. Of course I'm talking about _me_ and `mutt`.

Installation in Red Hat/CentOS is easy:

```
# yum -y install mutt
```

I use several different accounts, so I will need different configuration files. One of them, my default account, will be soft-linked to `~/.muttrc`:

```
$ ls -la .muttrc
lrwxrwxrwx. 1 me me  36 jun 11 22:32 .muttrc -> /home/me/.mutt/.muttrc-account1

$ ls -la .mutt/.muttrc-*
-rw-------. 1 me me 587 jun 12 00:13 .mutt/.muttrc-account1
-rw-------. 1 me me 584 jun 12 00:19 .mutt/.muttrc-account2
-rw-------. 1 me me 584 jun 12 00:24 .mutt/.muttrc-account3
```

So we can start with just mutt command or we can choose a configuration file: `mutt -F .mutt/.muttrc-account2`

# Basic configuration

Part of the configuration is account specific:

```
# Account specific information
set from = "test@example.com"
set realname = "My Name"

# Signature
set signature = "~/.mutt/signature"

# IMAP configuration
set imap_user = "uesrname"
set imap_pass = "password"
set folder = "imaps://imap.example.com:993"
set spoolfile = "+INBOX"

# SMTP configuration
set smtp_url = "smtps://username@smtp.example.com:465/"
set smtp_pass = "password"
set ssl_force_tls=yes
```

That's enough to make it work; however, we will improve it little by little including some files and getting rid of clear text passwords.

# Common settings

Some settings (folders, GPG, colours...) are common for all accounts, so we can just include them in the account specific configuration files. 

Something like this:

```
# Common configuration
source ~/.mutt/common.rc

# GnuPG bootstrap
source ~/.mutt/gpg.rc

# Appearance
source ~/.mutt/appearance.rc
```

# Should I write my password in the config file?

No, you shouldn't. And there is no need to do it. You can safely leave the `imap_pass` and `smtp_pass` lines out and mutt will ask for passwords when required.

You can also write them in a temporary file:

```
set imap_pass = "my_very_secret_imap_password"
set smtp_pass = "my_very_secret_smtp_password"
```

Encrypt the file:

```
gpg --recipient me@example.com --encrypt ~/.mutt/.pw-account1
```

And include it in your account configuration file:

```
source "gpg -d ~/.mutt/.pw-account1.gpg |"
```

Warning: don't forget to remove the `imap_pass` and `smtp_pass` and securely erase the file where you wrote your passwords in clear text.

# Dealing with HTML email

To be able to read HTML email we will need a command line browser. I use `w3m` that will require an additional repo to be enabled:

```
sudo subscription-manager repos --enable "codeready-builder-for-rhel-8-$(arch)-rpms"
sudo yum -y install w3m
```

Then add this to `~.mailcap`:

```
text/html; "$BROWSER" %s &; test=test -n "$DISPLAY"; needsterminal;
text/html; w3m -I %{charset} -T text/html; copiousoutput;
```

And this to your `.muttrc`:

```
# HTML
auto_view text/html
alternative_order text/plain text/enriched text/html
```

And you are good to go.

# Signing and encrypting mail

Signing and encrypting mail is almost trivial once you have properly set up your GPG keys.

I created a `gpg.rc` file from a template:

```
$ cat /usr/share/doc/mutt/gpg.rc |grep -v ^#|sed '/^$/d' > .mutt/gpg.rc
$ cat >>.muttrc <<EOF
# GnuPG bootstrap
source ~/.mutt/gpg.rc
EOF
```

And added a couple extra lines:

```
set crypt_autosign=yes
set pgp_use_gpg_agent
```

Messages will then be automatically signed. to encrypt them you just have to press "p" before sending a message with "y" and choose what you want to do:

```
PGP (e)ncrypt, (s)ign, sign (a)s, (b)oth, or (c)lear?
```

# Appearance

Add some fancy colours for good measure:

```
# set arrow_cursor
color index white default '.*'

# For new mail:
color index brightyellow black "~N"

# Header colors:
color header blue default ".*"
color header brightmagenta default "^(From)"
color header brightcyan default "^(Subject)"
color header brightwhite default "^(CC|BCC)"
```

Enjoy a proper mail client!
