---
title: "Favicon howto"
date: 2020-11-15T11:20:12+01:00
url: favicon-howto
categories: ['blog']
tags: ['web']
summary: To my surprise, favicon is not a single stupid file anymore. Sh*t.
draft: false
---

When rebuilding this page with [Hugo](https://gohugo.io) I noticed the favicon was missing. Poor me, I thought it was just one favicon.ico file.

Apparently not anymore. To summarize (from [realfavicongenerator.net](https://realfavicongenerator.net/faq)):

File | Description
--- | ---
`favicon.ico` | The _classic_ favicon _unique_ file
`favicon-16x16.png` | The classic favicon, displayed in the tabs
`favicon-32x32.png` | For Safari on Mac OS
`android-chrome-48x48.png` | For Android Chrome M39+ with 1.0 screen density
`mstile-70x70.png` | For Windows 8 / IE11
`apple-touch-icon-57x57.png` | iPhone and iPad users can turn web pages into icons on their home screen. Such link appears as a regular iOS native application. When this happens, the device looks for a specific picture. The 57x57 resolution is convenient for non-retina iPhone with iOS6 or prior.

And many, **many** more!!

Some browsers will search for the good old `/favicon.ico` file.
Some browsers will look into the classic HTML code:

{{< highlight html >}}
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
{{< /highlight >}}

Some other will try to read `/browserconfig.xml` or `site.webmanifest` or whatnot.

This is just crazy.

So I finally used [favicon.io](https://favicon.io/favicon-converter/) to create some of those, downloaded the generated files:

- android-chrome-192x192.png
- android-chrome-512x512.png
- apple-touch-icon.png
- favicon-16x16.png
- favicon-32x32.png
- favicon.ico
- site.webmanifest

and copied all those in the root folder of the site (`static` directory of hugo folder).

Does it work? To be honest, I couldn't care less. However, [favicon checker](https://realfavicongenerator.net/favicon_checker) says some of them are there. So be it.

## Update

I found a complete howto here: [Adding favicons in a multi-browser multi-platform world](https://mobiforge.com/design-development/adding-favicons-in-a-multi-browser-multi-platform-world)