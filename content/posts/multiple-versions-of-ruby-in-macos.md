---
title: "Installing multiple versions of Ruby in macOS"
date: 2019-04-16
url: installing-multiple-versions-of-ruby-in-macos
categories: ['blog']
tags: ['macos', 'ruby']
summary: how to install multiple versions of Ruby in macOS and not to make a terrible mess
---

Trying to install Jekyll on my Macbook I noticed that the version shipped with Mojave is 2.3.something. There are a couple of ways to solve this.

# homebrew

The first one is using [homebrew](https://brew.sh/). Provided that you have installed it before, it is just a command away:

```
brew install ruby
```

Don't forget to set the `PATH` environment to make sure that you use the right Ruby version:

```
export PATH=/usr/local/opt/ruby/bin:$PATH
ruby -v
ruby 2.6.4p104 (2019-08-28 revision 67798) [x86_64-darwin18]
```

# rbenv

[rbenv](https://github.com/rbenv/rbenv) is a tool commonly used to manage different versions of Ruby, which is quite useful sometimes. Of course it can be installed from homebrew as well:

```
brew install rbenv
rbenv init

# Add rbenv to you bash shell environment
echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
```

After that, you can install whatever version of Ruby you need. For example:

```
rbenv install 2.6.3
rbenv global 2.6.3
ruby -v
ruby 2.6.3p62 (2019-04-16 revision 67580)
```
