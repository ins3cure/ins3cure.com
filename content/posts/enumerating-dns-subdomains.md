---
title: "Enumerating DNS Subdomains"
date: 2021-12-11T11:26:45+01:00
url:
author: /me
categories: ['blog']
tags: ['DNS', 'enumeration']
draft: false
---

Be it for CTFs, real life™ pentests, threat intel, etc. DNS subdomain enumeration is a very important task. There are many different tools and techniques for it.

<!--more-->

## Just ask

`dig domain.com axfr`

No, it won't work :) 

## Passive

Use Google: search for `site:*.domain.com`

There are more places to search: search engines, Netcraft, DNSdumpster, VirusTotal, SSL Certificates...

Bing used to allow searches like `ip:1.1.1.1` returning domains for a scpecific IP address. Unfortunately it does not work any more :(

But there is https://api.hackertarget.com/:

```
% curl 'https://api.hackertarget.com/reverseiplookup/?q=40.113.200.201'
blogmicrosofteducacao.com.br
ccmjconsulting.com
chasenbell.com
chasinggoogle.com
customvision.ai
e.microsoft.com
gamepass.com
interngame.com
lobe.ai
microsoftsale.net
movere.io
odata.org
opensource.ms
powerplatform.co
reroute443.trafficmanager.net
surface.com
swrepository.com
xboxlive.com
zootycoon.biz
zune.ne
```

## Bruteforcing

There is a load of bruteforcing tools:

- [altdns](https://github.com/infosec-au/altdns). Subdomain discovery through alterations and permutations.
- [OWASP amass](https://github.com/OWASP/Amass). The OWASP Amass Project performs network mapping of attack surfaces and external asset discovery using open source information gathering and active reconnaissance techniques including DNS, Scraping, Certificates, APIs and web archives.
- [dnstwist](https://github.com/elceef/dnstwist). See what sort of trouble users can get in trying to type your domain name. Find lookalike domains that adversaries can use to attack you. Can detect typosquatters, phishing attacks, fraud, and brand impersonation. Useful as an additional source of targeted threat intelligence.
- [sublist3r](https://github.com/aboul3la/Sublist3r). Sublist3r is a python tool designed to enumerate subdomains of websites using OSINT. It helps penetration testers and bug hunters collect and gather subdomains for the domain they are targeting. Sublist3r enumerates subdomains using many search engines such as Google, Yahoo, Bing, Baidu and Ask. Sublist3r also enumerates subdomains using Netcraft, Virustotal, ThreatCrowd, DNSdumpster and ReverseDNS.

`amass` is really complete, there is an extensive tutorial here: https://github.com/OWASP/Amass/blob/master/doc/tutorial.md and deserves a full post (someday).

## Fuzzing

In a context where you do not have a real DNS the best option may be fuzzing. But not only that; this technique makes possible to discover non-indexed (for whatever reason) subdomains, working subdomains behind a wildcard (*.domain.com), etc.

Btw, is it different from "bruteforcing"? Nah, not really.

### ffuf

`ffuf` is a very fast multithreaded fuzzer written in go, so you can install by running `go get github.com/ffuf/ffuf` or just get it from the [github page](https://github.com/ffuf/ffuf).

Example:

`$ ffuf -w subdomains-top1million-5000.txt -H "Host: FUZZ.forge.htb" -u http://forge.htb -fc 302`

This code will run through the wordlist provided with `-w`, modify the host header and exclude results with a 302 return code. You can filter (include/exclude) by status, size, words, lines... configure the rate and number of threads, etc.

### wfuzz

Quite similar to `ffuf`, this would be the equivalent syntax:

`wfuzz -u http://forge.htb -w subdomains-top1million-5000.txt -H "Host: FUZZ.forge.htb" --hc 302`

### ffuf vs wfuzz speed

The two commands compared. `ffuf` is 4 times faster (with default configurations as seen above)!

| Program | Time |
| --------| ---- |
| ffuf | 0m7,045s |
| wfuzz | 0m28,253s |

## Resources

- [Scrutiny on the Bug Bounty](https://docs.google.com/presentation/d/1PCnjzCeklOeGMoWiE2IUzlRGOBxNp8K5hLQuvBNzrFY/edit#slide=id.g129ec7274d_1_117)
- [Subdomains Enumeration Cheat Sheet](https://pentester.land/cheatsheets/2018/11/14/subdomains-enumeration-cheatsheet.html)
- [OWASP Amass - An Extensive Tutorial](https://github.com/OWASP/Amass/blob/master/doc/tutorial.md)
- [Fuzz Faster with Ffuf](https://medium.com/quiknapp/fuzz-faster-with-ffuf-c18c031fc480)
- [Daniel Miessler SecLists](https://github.com/danielmiessler/SecLists/)