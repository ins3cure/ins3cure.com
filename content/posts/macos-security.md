---
title: "macOS security (1)"
date: 2020-11-28T08:42:59+01:00
url: macos-security
categories: ['blog']
tags: ['macos','security']
summary: A macOS security primer. Because security matters.
draft: false
---

Security matters. It is the news, we here about it every day. Even if you think you are not a target for the bad guys, **[you are](https://krebsonsecurity.com/2012/10/the-scrap-value-of-a-hacked-pc-revisited/).**

Macbooks are nice machines. I have one myself and I am quite happy with it. If you ask people why they choose a Mac you will have a variety of answers but chances are that some of them will reply: *"because it is secure"*.

## Is a Mac secure?

Security is not an binary value. There is nothing as "secure" and "not secure". It depends on multiple things - what is secure for you may not be secure for me and vice versa. There is what we call [threat model](https://en.wikipedia.org/wiki/Threat_model) and [risk management](https://en.wikipedia.org/wiki/Risk_management).

So no, there is no yes/no answer to the question **"is a Mac secure?"**. The only thing we can do is try to understand security model and features and decide whether it is secure enough for us.

Apple will of course say macOS is secure because security has been [built right in](https://www.apple.com/macos/security/). While this is a good reason (security should be built right in from the start of any project) we still have to set aside the marketing babble and the facts straight.

# Hardware

We live in an era where hardware security is at stake. The release of [Meltdown and Spectre](https://meltdownattack.com) vulnerabilities changed the whole world. After those a lot of new processor related vulnerabilities have been released. And not only Intel was affected.

Contrary to what *normally* happens with software bugs, hardware vulnerabilities are not easy to patch. Sometimes patches come at a huge cost because performance is noticeably degraded. This leads to a very interesting discussion: *do we have to patch hardware related vulnerabilities?*. The answer is *it depends*. Again you will have to analyze your threat model and decide whether performance is more important than security for your use case.

## Computer inside a computer

One of the new emerging threats (not that it has not been there for years but you know...) is the *computer inside a computer*. That is a processor that lives its own independent life in your hardware, even with an operating system of its own.

It's been there for years and it was supposed to make our lifes easier providing wonderful things like remote management capabilities. Imagine being able to power on a computer sitting 600 Km away without haveng to push a button; or unlocking a LUKS encypted disk without telling some guy you don't even know "hey, this is the password that protects all my strictly confidential data and my certificates and private keys, please type it so that the system can boot". Amazing, isn't it?

So we have RSA, BMC, iLO, iDRAC... you name it.

### Apple T2 Security Chip

In 2016 Apple release the T1 SoC (System on a Chip) to drive the SMC (System Management Controller) and the Touch Bar in certain 2016 and 2017 models. It is and ARM based processor that runs its own operating system based on watchOS (named bridgeOS). Go figure!

Starting in 2017 it was replaced by the T2, a 64 bit ARM running bridgeOS 2.0. Again it works as a [secure enclave](https://support.apple.com/guide/security/secure-enclave-overview-sec59b0b31ff/web) providing services:

- On-the-fly storage encryption (dedicated AES hardware engine to help DataProtection and FileVault)
- [Secure boot process](https://support.apple.com/kb/HT208330)
- Enhanced image signal processing (sounds scary, doesn't it?)
- Enables TouchID authentication
- T2 includes a random number generator

Cryptographic operations can be delegated to the T2 chip so integrity is guaranteed even if our kernel is compromised. The T2 SoC also allows to brick the computer if it is unfortunately lost or stolen.

So far so good, right?

### The unfixible flaw in Apple T2 chip

In October 2020 researchers released a tool to bypass T2 security. Impact is potentially huge:

- Could be used to disable System Integrity Protection
- Disable Secure Boot and allow to install some malware
- Chained with some other vulnerabilities it could potentially allow to obtain FileVault encryption keys

And last but not least: T2 can be hacked to [run Linux](https://twitter.com/qwertyoruiopz/status/1238606353645666308) and even [Doom](https://twitter.com/b3ll/status/800472338496036864)!

Fortunately the flaw is not that easy to exploit. First of all it requires physical access to the computer (the exploit runs from a different machine connected via USB). It is not even permanent. If you are a target chances are that [a different attack vector is more efficient](https://twitter.com/DanyL931/status/1313405426571112448). In short: do not run to replace your hardware if you are the kind of people that will fall into a phishing attempt.

# Encryption

We all want our data at rest encrypted, right? Windows provides [Bitlocker](https://docs.microsoft.com/en-us/windows/security/information-protection/bitlocker/bitlocker-device-encryption-overview-windows-10), Linux has [LUKS](https://gitlab.com/cryptsetup/cryptsetup/) and Apple has [Data Protection/FileVault](https://support.apple.com/guide/security/encryption-and-data-protection-overview-sece3bee0835/1/web/1).

## FileVault

FileVault encrypts data at rest using XTS-AES-128 encryption with a 256-bit key (NIST compliant).

If you have a Mac with a T2 chip it will be encrypted by default. However, enabling FileVault provides further protection by requiring your login password to decrypt your data.

There are two classic drawbacks in disk encryption:

- Only home directory is encrypted.
- If all disk (full-disk encryption) is required, user has to authenticate twice: first to unlock the encrypted volume and a second time to login. This is no longer necessary with FileVault (and of course most state-of-the-art methods from different vendors). The system starts in a pre-boot environment and asks for the login password. The encryption key is derived from the login password so no further authentication request is needed.

There is an interesting paper from French CCSD (Center for Direct Scientific Communication) providing deep [technical details about FileVault](https://hal.inria.fr/hal-01460615/document). Unfortunately maby a bit outdated (there is not even HFS+ anymore, replaced by APFS). I keep a [local copy](/pdf/HAL-FileVault.pdf) just in case.
 
In summary, data is encrypted/decrypted with a MEK (Media Encryption Key). A KEK (Key Encryption Key) is used to create a ciphertext version of MEK. The user password (along with other passwords maybe) are processed by key derivation functions that create a ciphertext version of the KEK. So if you provide a valid password you will be able to decrypt the KEK wich in turn will be used to get the MEK and then encrypt/decrypt data.

FileVault takes advantage of the T2 hardware AES facilities so encryption and decryption does not produce noticeable delay. However as far as I understand, the MEK would never leave the T2 Secure Enclave where encryption and decryption should take place.

{{< alert info >}}
There is an old whitepaper from 2012 (so probably superseded) but I could not find any recent official document in Apple's web site. It is available [here](/pdf/WP_FileVault2.pdf)
{{< /alert >}}

![The key derivation process](/img/key-derivation-process.png)

[(To be continued...)](/posts/blog/macos-security-2/)

# References

- [Hardware Security Overview](https://support.apple.com/guide/security/hardware-security-overview-secf020d1074/1/web/1)
- [Secure Enclave Overview](https://support.apple.com/guide/security/secure-enclave-overview-sec59b0b31ff/web)
- ars Technica: [T2 security chip has an unfixable flaw](https://arstechnica.com/information-technology/2020/10/apples-t2-security-chip-has-an-unfixable-flaw/)
- [Encryption and Data Protection](https://support.apple.com/guide/security/encryption-and-data-protection-overview-sece3bee0835/1/web/1)
- [Security Analysis and Decryption of Filevault 2](https://hal.inria.fr/hal-01460615/document)