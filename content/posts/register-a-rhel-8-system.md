---
title: "Registering a Rhel 8 System"
date: 2020-10-11
url: register-a-rhel-8-system
categories: ['blog']
tags: ['linux', 'rhel']
summary: Registering a RHEL 8 system to Redhat (for free!)
---

If you want to pass Red Hat Certified System Administrator EX200 certification you can prepare with a CentOS 8 system. But there is a better way.

Red Hat Enterprise Linux subscriptions are available for free under certain circumstances. All you have to do is create an account and follow instructions to download an ISO file.

![RedHat logo](/img/redhat_logo.png)

But after installing the system you'll notice you can't even update the software:

![Screenshot-2020-10-11-at-12.03.33.png](/img/reg-rhel8-1.png)

The reason is the device must be registered. If you login to your recently created profile, you'll see an active subscription but no systems attached to it:

![Screenshot-2020-10-11-at-12.07.41.png](/img/reg-rhel8-2.png)

![Screenshot-2020-10-11-at-12.09.32.png](/img/reg-rhel8-3.png)

Let's register our new device:

![Screenshot-2020-10-11-at-12.11.34.png](/img/reg-rhel8-4.png)

After a couple of minutes if we refresh our systems page we will see our device registered:

![Screenshot-2020-10-11-at-12.14.09.png](/img/reg-rhel8-5.png)

Now we can click on the system and attach a developer entitlement:

![Screenshot-2020-10-11-at-12.16.05.png](/img/reg-rhel8-6.png)

However we can't update it or install packages yet. But we are close, let's run this command:

```
subscription-manager attach --auto
```

![Screenshot-2020-10-11-at-12.19.52.png](/img/reg-rhel8-7.png)

Yeah!

![Screenshot-2020-10-11-at-12.20.55.png](/img/reg-rhel8-8.png)