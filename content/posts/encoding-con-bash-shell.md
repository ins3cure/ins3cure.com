---
title: "Encoding con bash"
date: 2016-10-04
url: encoding-con-bash-shell
author: /me
categories: ['blog']
tags: ['bash', 'encoding', 'shell']
draft: false
---

Oneliners para distintos encodings con la shell

<!--more-->

## rot13

```bash
echo "blah blah blah" | tr '[a-m][n-z][A-M][N-Z]' '[n-z][a-m][N-Z][A-M]'
oynu oynu oynu
```

Por sus propias características, aplicando el mismo algoritmo “desofusca” la cadena:

```bash
echo "oynu oynu oynu" | tr '[a-m][n-z][A-M][N-Z]' '[n-z][a-m][N-Z][A-M]'
blah blah blah
```

## Base64

```bash
echo echo "blah blah blah" | base64
YmxhaCBibGFoIGJsYWgK
```

Y al revés:

```bash
echo YmxhaCBibGFoIGJsYWgK | base64 -d
blah blah blah
``` 

## rev

(Coño, no me sabía esta O_o)

```bash
echo "hola"|rev
aloh
```

## hexdump

```bash
echo "lorem ipsum dolor sit amet" | xxd -p
6c6f72656d20697073756d20646f6c6f722073697420616d65740a
```

Y al revés...

```bash
echo 6c6f72656d20697073756d20646f6c6f722073697420616d65740a | xxd -p -r
lorem ipsum dolor sit amet
```

Pero con hexdump hay muchas más opciones (convertir a octal, a binario...):

```bash
echo "lorem ipsum dolor sit amet" | hexdump -v -e '/1 "%03i "'; echo
108 111 114 101 109 032 105 112 115 117 109 032 100 111 108 111 114 032 115 105 116 032 097 109 101 116 010

echo "lorem ipsum dolor sit amet" | hexdump -v -e '/1 "%02x "'; echo
6c 6f 72 65 6d 20 69 70 73 75 6d 20 64 6f 6c 6f 72 20 73 69 74 20 61 6d 65 74 0a

echo "lorem ipsum dolor sit amet" | hexdump -v -e '/1 "%03o "'; echo
154 157 162 145 155 040 151 160 163 165 155 040 144 157 154 157 162 040 163 151 164 040 141 155 145 164 012
```

## openssl

```bash
echo "s3cr3t" | openssl passwd -crypt -stdin -salt foobar # salt foobar
echo "s3cr3t" | openssl passwd -crypt -stdin              # random salt
echo "s3cr3t" | openssl passwd -1 -stdin -salt foobar     # shadow
echo "s3cr3t" | openssl passwd -apr1 -stdin -salt foobar  # apache
```

Y muchas más [aquí](https://gist.github.com/Janfy/940195a6fc4278112458]) y [aquí](https://www.redspin.com/it-security-blog/2009/07/string-encoding-in-the-shell/).

