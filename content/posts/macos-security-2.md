---
title: "macOS security (2)"
date: 2020-11-29
description: A macOS security primer.
url: macos-security-2
categories: ['blog']
tags: ['macos','security']
draft: false
---

In [this post](/macos-security) we started writing about macOS security. This is the continuation of that post.

<!--more-->

# Application security

macOS is design with multiple layers of security to achive a Defense in Depth paradigm.

## Gatekeeper

Gatekeeper is designe to prevent malicious software to run on a Mac. It was introduced in 2012 and its functionality has change over the years. We'll focus on macOS Catalina here.

Gatekeeper does various checks at different moments. First of all, it chechs for malicious content first time a querantined program is going to be launched by LaunchServices.

But first we need to know a bit about those new concepts.

Quarantine is a technology that adds some metadata to files that have arrived from external sources like browser downloads, AirDrop, etc. Those new files are *quarantined* and, under certain circumstances, a prompt will be presented on first launch. But be careful: the application is responsible for adding metadata to the file and not all applications do that.

Listing file metadata from the command line:

{{< highlight shell >}}
% mdls Firefox\ 83.0.dmg 
[...]
kMDItemDateAdded                       = 2020-11-29 20:48:04 +0000
kMDItemDateAdded_Ranking               = 2020-11-29 00:00:00 +0000
kMDItemDisplayName                     = "Firefox 83.0.dmg"
kMDItemDocumentIdentifier              = 0
kMDItemDownloadedDate                  = (
    "2020-11-29 20:48:04 +0000"
)
kMDItemFSContentChangeDate             = 2020-11-29 20:48:04 +0000
kMDItemFSCreationDate                  = 2020-11-29 20:47:54 +0000
[...]
kMDItemKind                            = "Disk Image"
kMDItemLogicalSize                     = 76533364
kMDItemPhysicalSize                    = 84746240
kMDItemWhereFroms                      = (
    "https://download-installer.cdn.mozilla.net/pub/firefox/releases/83.0/mac/en-US/Firefox%2083.0.dmg",
    "https://www.mozilla.org/en-US/firefox/download/thanks/"
)
{{< /highlight >}}

*LaunchServices* is a framework fon finding and launching applications:

- Open an app in finder or the dock
- Use of NSWorkspace API (generally)
- Apps opened via document handlers or URLs

Not all applications are launched by LaunchServices. Some exceptions are:

- Using NSTask to start a process
- Using exec / posix_spawn
- Loading a library via NSBuldle / dlopen

Before macOS Catalina (Mojave) these are the checks performed upon first use when software is querantined:

|Activity|LaunchServices
|---|---|
Malicious scan content|No known malicious content
Signature check|No tampering
Local policy check|Must be signed with Developer ID certificate
First launch prompt|User must approve

Mopjave 10.4.5 did some changes in the policies, but Catalina went a step further and software launched without using LaunchServices is addressed by Gatekeeper as well:

|Activity|LaunchServices|No LaunchServices
|---|---|---|
Malicious scan content|No known malicious content|No known malicious content
Signature check|No tampering|No tampering
Local policy check|New software requires notarization| New software requires notarization
First launch prompt|User must approve|User must approve software in bundles

To clarify, this is Apple's description of the notarization process:

> Notarization gives users more confidence that the Developer ID-signed software you distribute has been checked by Apple for malicious components. Notarization is not App Review. The Apple notary service is an automated system that scans your software for malicious content, checks for code-signing issues, and returns the results to you quickly. If there are no issues, the notary service generates a ticket for you to staple to your software; the notary service also publishes that ticket online where Gatekeeper can find it.

And two final thoughts:

- In macOS Catalina Gatekeeper will do a malicious content scan no matter if software is quarantined, giving the user the chance to remove the software
- There is always a way for you to run a specific piece of software you want to run regardless of what Gatekeeper thinks about it.

### spctl: SecAssessment system policy security

The `spctl` tool manages the security assessment policy subsystem. Example:

{{< highlight shell >}}
% spctl -a -vvv /Applications/pCloud\ Drive.app
/Applications/pCloud Drive.app: accepted
source=Notarized Developer ID
origin=Developer ID Application: PCLOUD LTD (KSTWHH4JHP)
{{< /highlight >}}

This command will enable the user to run any downloaded software regardless of where it comes from:

```
spctl --master-disable
```

After running that command, the Security & Privacy settings will show an option that is hidden since some versions ago to allow apps downloaded from anywhere to run on your system.

Of course this setting can be reverted with:

```
spctl --master-enable
```

![allow from anywhere](/img/spctl.jpg)

To chck the status:

```
% /usr/sbin/spctl --status 
assessments enabled
```

Not being enabled would pose a security risk.

## Yet more malware related tools

### XProtect

XProtect aims to protect you from known malware. Launched in 2009 it is the first attempt of an antimalware tool after a long time of stubborn denial.

Apple definition is quite optimistic (state-of-the-art may be a little exaggerated in my humble opinion)

> macOS includes built-in state-of-the-art antivirus technology called XProtect for the signature-based detection of malware, the use of which supports best-practice protection from viruses and malware. The system uses YARA signatures, which Apple updates regularly.

XProtect maintains a blacklist and a yara rules file in `/System/Library/CoreServices/XProtect.bundle/Contents/Resources/`. The list is short and not really updated very often. Software is checked on first launch or if has changed and, if detected as malware, the user will be notified and given the option to remove the malicious software.

### MRT (Malware Removal Tool)

In Apple's own words:

> The Malware Removal Tool (MRT) is an engine in macOS that remediates infections based on updates automatically delivered from Apple.

I short Apple fights malware with a combination of these components:

- Gatekeeper
- XProtect
- MRT

Does not sound really impressive though.

## Data protection

It may happen that malware escapes from all three aformentioned tools. But not everything is lost yet! That's where _Defense in Depth_ comes in.

What is App Sandbox. In Apple's words:

> App sandbox restricts access to system resources and user data in macOS apps to contain damage if an app becomes compromised.

Enabling macOS App Sandbox capability is mandatory to distribute macOS apps through the App Store.

User data protection means that users have to approve apps tryng to access their data. This means: 

- A user can choose what a program can access
- The user will be (frequently) prompted for access

Privacy protection in macOS Catalina focuses on various items:

- Require user consent to use hardware that can record users (camera, mic, screen recording, keystrokes)
- Require user consent to access sensitive files or folders such as photos, mails or messages
- Controls automation or how apps share data with each other

### Hardware and recording capabilities

In general, first time some specific function calls are made, a user consent window is displayed. For example, [CGWindowListCreateImage()](https://developer.apple.com/documentation/coregraphics/1454852-cgwindowlistcreateimage) which could potentially get sensitive information from another window.

Apps can freely record the contents of their own windows, the menu bar and the desktop background image.

This is an example of an app requesting permission to access hardware:

![zoom wants to use the camera](/img/zoom-camera-access.png)

### Privacy sensitive files

Up to macOS Catalina, some locations required (under certain circumstances) user consent before being accessed by some apps because they can potentially store pricavy sensitive files:

- Contacts
- Calendars
- Reminders
- Photos

macOS Catalina protects access to some more locations:

- ~/Desktop
- ~/Documents
- ~/Downloads
- iCloud Drive
- Third-party cloud storage
- Removable volumes
- Network volumes

![terminal requesting access](/img/terminal-privacy.png)

In order to avoid and excessive number of prompts for consent, macOS Catalina uses a complex mechanism explained under the _user intent_ idea. _User intent_ is inferred when a user starts actions like double click in Finder, selects files in Open or Save panel or uses drag and drop. In these cases the user prompt is not needed.

This is a simplified _user consent_ vs _user intent_ comparison:

|User consent|User intent|
|---|---|
Rective|Proactive
Prompt interrupts workflow|Inferred transparently from workflow
Applies to whole class of data|Applies to just the selected files

There are some reports of macOS Cataline user intents being buggy but I have not experienced it myself.

### Private data managed by the system

Some private data is managed by the system. These are the protected categories (starting in Mojave):

- Mail
- Messages
- Safari browsing history
- HTTP cookies
- Call history
- iTunes backups
- Time machine backups
- Trash (from Catalina)

Some applications like disk management or backup software may require access to all disk regardless of the type of data it stores. There are APIs ([isReadableFile()](https://developer.apple.com/documentation/foundation/filemanager/1418292-isreadablefile), [isWritableFile()](https://developer.apple.com/documentation/foundation/filemanager/1416680-iswritablefile)...) that can check whether the app is authorized without triggering user consent. Depending on the result the app may decide to skip the inaccessible file or request approval.

![Security & Privacy](/img/privacy-full-disk-access.png)

### Automation

Some applications may want to perform tasks as:

- simulate key press and/or release or mouse clicks (example: a password manager automatically typing passwords)
- control another application (example: an app that wants to control Keynote, Outlook tryng to access Skype)

Again, these activities may trigger a user consent dialog

### The TCC framework

Behind curtains there is the TCC (Transparency Consent & Control) framework in charge. It proivides:

- The TCC database:
    - /Library/Application\ Support/com.apple.TCC/TCC.db
    - ~/Library/Application\ Support/com.apple.TCC/TCC.db
- The TCC framework
    - /System/Library/PrivateFrameworks/TCC.framework/Versions/A/Resources/tccd
- tccutil binary
    - `/usr/bin/tccutil`
- Logging
    - subsystem == "com.apple.TCC"
    - Example:
    `log stream --debug --predicate 'subsystem == "com.apple.TCC"'`
- Profile type
    - com.apple.TCC.configuration-profile-policy

# References

- [Notarizing macOS Software](https://developer.apple.com/documentation/xcode/notarizing_macos_software_before_distribution)
- [Protecting against malware](https://support.apple.com/guide/security/protecting-against-malware-sec469d47bd8/web)
- [App Sandbox](https://developer.apple.com/documentation/security/app_sandbox)
- [Advances in macOS Security](https://developer.apple.com/videos/play/wwdc2019/701/)
- [Jamf Nation User Conference 2018](https://www.youtube.com/watch?v=vOcJ2R6zPHs)
