---
title: "Configuring RHEL 8 for YubiKey"
date: 2022-01-15T02:04:14+01:00
url: /rhel-8-yubikey
author: /me
categories: ['blog']
tags: ['2FA', 'rhel', 'yubikey']
draft: false
---

An alternative to Google Authenticator.

<!--more-->

According to [Wikipedia](https://en.wikipedia.org/wiki/YubiKey), _The YubiKey is a hardware authentication device manufactured by Yubico to protect access to computers, networks, and online services that supports one-time passwords (OTP), public-key cryptography, and authentication, and the Universal 2nd Factor (U2F) and FIDO2 protocols developed by the FIDO Alliance._

To make it work we will need to install some packages from the [EPEL repository](https://docs.fedoraproject.org/en-US/epel/#_el8):

{{< highlight shell >}}
yum -y install pam_yubico ykclient ykpers
{{< /highlight >}}

(Note: man pages are not nearly as complete as the documentation in the [yubico developers page](https://developers.yubico.com/yubikey-personalization/Manuals/ykpersonalize.1.html) Take a look at it if you need more information).

Insert your YubiKey and run:

{{< highlight shell >}}
ykpersonalize -2 -ochal-resp -ochal-hmac -ohmac-lt64 -oserial-api-visible
{{< /highlight >}}

Optionally add `-ochal-btn-trig` and the device will require a button touch; this is hardly a security improvement if you leave your YubiKey plugged in. I guess this is solved with the new Bio Series YubiKeys that will recognize your fingerprint but they are way too expensive.

You should get a similar output to this:

{{< highlight shell >}}
[user@host ~]$ ykpersonalize -2 -ochal-resp -ochal-hmac -ohmac-lt64 -oserial-api-visible
Firmware version 3.4.3 Touch level 1551 Program sequence 3

Configuration data to be written to key configuration 2:

fixed: m:
uid: n/a
key: h:b4...blablahbla...d0
acc_code: h:000000000000
OATH IMF: h:0
ticket_flags: CHAL_RESP
config_flags: CHAL_HMAC|HMAC_LT64
extended_flags: SERIAL_API_VISIBLE

Commit? (y/n) [n]: y
[user@host ~]$
{{< /highlight >}}

The Yubikey has to be associated to a user. So, logged in as the desired user, run:

{{< highlight shell >}}
[user@host ~]$ ykpamcfg -2
Stored initial challenge and expected response in '/home/user/.yubico/challenge-<serial>'.
{{< /highlight >}}

At this point you may have to touch the YubiKey button depending on your configuration.

We are almost done!

## Testing

In order to test minimizing the risk of being locked out, make sure you can run `sudo`. In my case I have a file `/etc/sudoers.d/user` containing `user ALL=(ALL) ALL`.

Modify `/etc/pam.d/sudo` and add this line before `auth       include      system-auth`:

{{< highlight shell >}}
auth       required   pam_yubico.so mode=challenge-response
{{< /highlight >}}

In my case this is the resulting file:

{{< highlight shell >}}
#%PAM-1.0
auth       required   pam_yubico.so mode=challenge-response
auth       include      system-auth
account    include      system-auth
password   include      system-auth
session    include      system-auth
{{< /highlight >}}

Disconnect the YubiKey and try to run this command:

{{< highlight shell >}}
[user@host ~]$ sudo echo test
[sudo] password for user: 
Sorry, try again.
[sudo] password for user: 
sudo: 1 incorrect password attempt
{{< /highlight >}}

The `sudo` command failed, as expected. Now try again after connecting the YubiKey:

{{< highlight shell >}}
[user@host ~]$ sudo echo test
[sudo] password for user: 
test
{{< /highlight >}}

It works! Now the serious stuff. Want to require the 2FA to log in? Update `/etc/pam.d/gdm-password` by adding `auth       required   pam_yubico.so mode=challenge-response` before `auth substack password-auth`. This should be the result on a standard RHEL 8 system:

{{< highlight shell >}}
auth     [success=done ignore=ignore default=bad] pam_selinux_permit.so
auth        required      pam_yubico.so mode=challenge-response
auth        substack      password-auth
auth        optional      pam_gnome_keyring.so
auth        include       postlogin
[...]
{{< /highlight >}}

Save the file. Lock the screen. Make sure your YubiKey is connected. Fingers crossed. Try to unlock.

If you are still reading this, it has worked. And SELinux is still in enforcing mode! :joy: 

## Moving the challenge file

In some cases you may want to move the challenge file to a path only readable and writable by root. You can do it this way:

{{< highlight shell >}}
sudo mkdir /var/yubico
sudo chown root.root /var/yubico
sudo chmod 700 /var/yubico
ykpamcfg -2 -v
...
Stored initial challenge and expected response in '$HOME/.yubico/challenge-123456'.
sudo mv ~/.yubico/challenge-123456 /var/yubico/<username>-123456
sudo chown root.root /var/yubico/<username>-123456
sudo chmod 600 /var/yubico/<username>-123456
{{< /highlight >}}

Where `<username>` is the name of the user that has to be authenticated with the YubiKey. Then you have to add the right path as an option to the PAM module in the PAM file configuration. In our exmaple, the line in `/etc/pam.d/gdp-password` will read:

{{< highlight shell >}}
auth   required        pam_yubico.so mode=challenge-response chalresp_path=/var/yubico
{{< /highlight >}}

# 2FA for ssh

There are several methods:

- OTP via PAM module (`pam_yubico`). Requires a validation server.
- U2F via `pam-u2f`.
- With OpenSSH 8.2 or higher, FIDO/U2F is natively supported. See [OpenSSH 8.2 release notes](https://www.openssh.com/txt/release-8.2)

maybe addressed in another post.

# Conclusion

Advantages:

- Easy to configure
- Usually easier than typing a 6-digit code
- More convenient than Google Authenticator for frequent use
- Works offline (or not, but you can choose)
- No SELinux hassle! It is well supported and you have your booleans if you ever need them:

{{< highlight shell >}}
user@host$ getsebool -a | grep -i yubikey
authlogin_yubikey --> off
{{< /highlight >}}

Disadvantages:

- Expensive
- Even more expensive if you need a backup (recommended)
- Forget your YubiKey connected and you are screwed
- Lose your YubiKey and you are (even more) screwed

# References

- [Red Hat Linux Family Login Guide - Challenge Response](https://support.yubico.com/hc/en-us/articles/360016648999-Red-Hat-Linux-Family-Login-Guide-Challenge-Response)
- [Local Authentication Using Challenge Response](https://developers.yubico.com/yubico-pam/Authentication_Using_Challenge-Response.html)
- [Using YubiKeys with Fedora](https://fedoraproject.org/wiki/Using_Yubikeys_with_Fedora)