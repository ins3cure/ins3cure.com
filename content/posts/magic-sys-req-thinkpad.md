---
title: "Magic Sys Req Thinkpad"
date: 2014-07-28
url: magic-sys-req-thinkpad
author: /me
categories: ['blog']
tags: ['thinkpad']
draft: false
---

Cómo ejecutar Magic SysRq en un Lenovo ThinkPad que no tiene la tecla SysRq

<!--more-->

No sé si alguna vez habréis necesitado utilizar las “Magic SysRq” en un Thinkpad. Hasta que no me hizo falta reiniciar de forma más o menos “segura” un Thinpad Edge E330 que se había colgado en una actualización a medio hacer, no me di cuenta de que la tecla SysRq no existe :D 

Afortunadamente encontré la entrada de un blog que daba las instrucciones precisas. Lo pego tal cual:

- Pulsar y mantener pulsada la tecla <kbd>Ctrl</kbd>
- Pulsar y mantener pulsada la tecla <kbd>Alt</kbd>
- Pulsar y mantener pulsada la tecla <kbd>Fn</kbd>
- Pulsar y mantener pulsada la tecla <kbd>PrtSc</kbd>
- Soltar <kbd>Fn</kbd>
- Mientras <kbd>Ctrl</kbd>, <kbd>Alt</kbd>, y <kbd>PrtSc</kbd> siguen pulsados, pulsar y soltar <kbd>R</kbd>
- Todavía con </bd>Ctrl</kbd>, <kbd>Alt</kbd>, and <kbd>PrtSc</kbd> pulsadas, pulsar y soltar las otras teclas de REISUB en orden dejando un par de segundos entre una y otra: <kbd>E</kbd> <kbd>I</kbd> <kbd>S</kbd> <kbd>U</kbd> <kbd>B</kbd>.
- El sistema debería reiniciarse.

Por cierto, el nemotécnico en español es [REInicia SUBnormal](http://es.wikipedia.org/wiki/REInicia_SUBnormal)

