---
title: "nginx: get an 'A+'!"
date: 2020-05-31
url: nginx-get-an-a
categories: ['blog']
tags: ['nginx', 'security']
summary: Make your nginx proxy get an A+ in Qualys SSL test
---

After deploying the blog and performing some usual test I stumbled upon a terrible score in Qualys SSL:

![Qualys B](/img/qualysB.png)

No, that's not acceptable!

Problems are:

- Some weak cipher suites are present
- Forward secrecy is not available
- Some other minor issues (no HSTS with a minimum timeout) prevent us from reaching the A+.

Fortunately it is not difficult to fix.

# Protocol support

We only support TLS 1.2. We are fine here. However, let's add new shiny TLSv1.3:

```
ssl_protocols TLSv1.3 TLSv1.2;
```

# Cipher suite

If we want an immaculate Qualys result, we can remove all TLS_RSA ciphers. Please not this will leave out some really old browsers, Personally I don't give a crap, but YMMV:

```
ssl_prefer_server_ciphers on;
ssl_ciphers EECDH+ECDSA+AESGCM:EECDH+aRSA+AESGCM:EECDH+ECDSA+SHA512:EECDH+ECDSA+SHA384:EECDH+ECDSA+SHA256:ECDH+AESGCM:ECDH+AES256:DH+AESGCM:DH+AES256:RSA+AESGCM:!aNULL:!eNULL:!LOW:!RC4:!3DES:!MD5:!EXP:!PSK:!SRP:!DSS;
```

# Key exchange

For a strong key exchange, we will create the new Diffie-Hellmann paramenters:

```
openssl dhparam -out /etc/nginx/dhparam.pem 2048
```

and add it to nginx configuration:

```
ssl_dhparam /etc/nginx/dhparam.pem;
```

(Note I'm using 2048 and not 4096 to match the certificate RSA key size)

# HSTS

Adding HSTS with a timeout longer than 6 months will deserve an A+:

```
add_header Strict-Transport-Security 'max-age=31536000; includeSubDomains; preload' always;
```

Et voilá!

![Qualys A](/img/qualysA.png)
