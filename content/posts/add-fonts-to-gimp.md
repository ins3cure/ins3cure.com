---
title: "Adding fonts to Gimp"
date: 2020-05-30
url: adding-fonts-to-gimp
author: /me
categories: ['blog']
tags: ['gimp']
draft: false
---

This article describes how to add fonts to Gimp.

<!--more-->

Download the fonts from your favourite site (e.g. https://www.dafont.com)

Make sure `~/.fonts` directory exists. Move your new fonts to this directory so that they are available for GIMP and any other program using fontconfig. Unzip if necessary.

Refresh the GIMP fonts so you don't have to restart Gimp:

![Add fonts to GIMP](/img/gimp_add_fonts.png)
