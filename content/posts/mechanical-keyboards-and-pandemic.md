---
title: "Mechanical keyboards and pandemic"
date: 2021-01-16T07:27:37+01:00
lastmod: 2021-12-29
url: mechanical-keyboards-and-pandemic
author: /me
categories: ['blog','hardware']
tags: ['hardware']
draft: false
---

COVID-19 pandemic has changed our lives. Death toll is over two million today. Two million deaths. Some reports say jobs lost are hundreds of millions. Hundreds of millions!! I feel lucky to keep my job and to be able to work from home, in a safe environment.

But of course it comes at a cost. Your employer may not provide all the stuff you need to make your work setup as comfortable as you like, so you may need new monitor (or monitors), a larger table, a new chair, wemcams and mics... And of course a keyboard.

<!--more-->

I am an avid laptop user since a lot of years ago (check [this post](/almost-all-my-computers) to find out). Chances are that part of my free time I'm also stuck to a computer, so a good keyboard is a must for me. That's why I prefer using:

* ThinkPad. Probably the best laptop keyboards ever. At list until the X220 model. From then on they are just ok.
* Apple MacBook. Keyboards used to be good enough for me. Except during the butterfly keyboard era. That was crap. Fortunately I sold my 2017 MacBook Pro to get a new 2020 one, where keyboard is _decent_ again.

During pandemic times I've been working from home, and I think I have worked more than ever. For work I used an old MacBook but in 2020 it was _upgraded_ to a Dell Latitude 7xxx series. A nice machine, but I don't like the keyboard at all. And after some weeks (not sure, maybe a couple of months) I started to develop some wrist pain.

I read that a mechanical keyboard might help so I borrowed one from my kids. And yeah, even if they were cheap gaming TKL keyboards with fancy RGB lights, they were nice. I can't say wrist pain was really caused by the Dell keyboard but the fact is it disappeared!

So I started looking for a keyboard. After some research this what I was looking for:

* A mechanical keyboard, of course
* ISO Esp distribution
* TKL (tenkeyless) or smaller
* RGB is optional, but backlight is a must
* Mac/Windows/Linux compatible
* If USB, USB-C is preferred
* Bluetooth is nice to have. But it must support multiple connections
* Cherry MX Brown switches
* Being able to get fancy keycaps is also a nice to have feature
* Not too expensive, of course

In the process I learned that it was nearly impossible to get a keyboard with all those features. ISO distribution in particular was a limiting factor. So I came up with two candidates:

* [Ducky One 2 Mini RGB](https://www.duckychannel.com.tw/en/Ducky-One2-Mini-RGB)
* [Drevo Calibur V2 Pro](https://www.drevo.net/product/keyboard/calibur-v2-pro)

I bought both with the idea of returning one but I will probably keep both of them.

## Ducky One 2 Mini RGB

This is a 60% keyboard. This means that you don't get functions keys, delete key and dedicated arrow keys. There is a version (name SF that stands for 65%) that has some extra keys including arrows. But it was not available and I decided to give the Mini a try.

![Ducky One 2 Mini RGB](/img/ducky_one_2_mini_rgb.png)

To be honest it was a great surprise. The keyboard is nice and the form factor is really cool. But the feeling with the keys was totally awesome! I had been reading about the different switches and I decided to go blindly for the Cherry MX brown. And I was right! This is the keyboard I want to be typing on for hours.

But it has drawbacks, it is not perfect at all of course.

It has no bluetooth. This is something I can live without though.

Lack of delete and functions keys is something I don't really care. Arrows may be a little more difficult but I'm confident I can learn to use the <kbd>Fn</kbd> plus whatever. Some dip switches allow to move the <kbd>Fn</kbd> key to the most convenient place, so I guess I can get along with it.

Using it with the Mac is not that easy though. In fact, it just does not work out of the box. You must upgrade the firmware first. And even then not everything works as expected. At this moment I have not been able to make function keys work. And for the life of me I couldn't type a <kbd>\\</kbd> (backslash).

## Drevo Calibur V2 Pro

The second keyboard I tested is the Drevo Calibur V2 Pro. There is a similar model called TE insted of Pro, but I think the difference is just TE has no bluetooth. But not sure.

It has some clear advantages over the Ducky:

* It has bluetooth. Pairing is a breeze and it supports up to five devices. Awesome.
* Mac support is way better. Function keys work and I can even type <kbd>\\</kbd> (by pressing <kbd>Fn</kbd>+<kbd>AltGr</kbd>+<kbd>Esc</kbd>, for the record). I has even a small "option" and "command" legend undel the WN-ALT keys :joy: 
* It has a weird form factor (see picture); larger than a 60% but smaller than a TKL. It has dedicated arrows and a bunch of other keys like <kbd>Home</kbd>, <kbd>End</kbd>, <kbd>PgUp</kbd>, <kbd>PgDn</kbd>, etc. You will probably not miss anything.

But also disadvantages:

* Does not look as cool as the Ducky
* Outemu brown switches are not nearly as pleasant as the Cherry MX ones.
* The right columns are a bit uncomfortable for me. Even more than trying to use <kbd>Fn</kbd>+something for arrows! It is difficult for me to find the large <kbd>Enter</kbd> or <kbd>Backspace</kbd> keys by touch.
* Keycaps are also worse than the ones in the ducky. Not the quality itself (both are low end ABS plastic), but the shape. They are slightly more sharp and I feel I make way more typing errors with this keyboard.


![Drevo Calibur V2 Pro](/img/drevo_calibur_v2_pro.jpg)

## Conclusions

Both keyboards are good, no one is perfect. To be fair I have just tested them for a few hours so some drawbacks may disappear.

If I had to choose one I would have a hard time to decide. Apparently Drevo has more advantages over the Ducky, but the typing experience is way better in the latter, at least in the beginning. So I will keep both.

ISO layout seems to be a very limiting factor when looking for new keycaps. And I badly need something like this:

![YMDK Carbon](/img/keycaps.jpg)

## Update: more keyboards

In the ongoing search for the perfect keyboard I came across a new one. It is not a mechanical keyboard but it is the best I have found so far:

![Logitech MX Keys Mini](/img/logitech-mx-mini.jpg)

It is the [Logitech MX Keys Mini](https://www.logitech.com/en-gb/products/keyboards/mx-keys-mini.html). It is a slim, light, small form-factor (but with arrow keys). Long battery life, charges via USB-C and has an interesting *smart illumination* with proximity sensors that control the backlight. It connects via bluetooth with up to three devices and works seamlessly with Windows, Linux and macOS. It's black :smile: 

And most importantly, typing experience is really great for me.