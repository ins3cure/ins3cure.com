---
title: Markdown syntax guide
date: 2019-09-25
url: markdown
author: /me
categories: ['blog']
tags: ['hugo', 'markdown']
draft: false
---
This article offers a sample of basic Markdown syntax that can be used in Hugo content files, also it shows whether basic HTML elements are decorated with CSS in my personalized Hugo theme based on <a href="https://github.com/mitrichius/hugo-theme-anubis">Anubis</a> and <a href="https://github.com/panr/hugo-theme-hello-friend/">hello-friend</a>.

<!--more-->

## Headings

The following HTML `<h1>` to `<h6>` elements represent six levels of section headings.
`<h1>` is the highest section level while `<h6>` is the lowest.

# This is a H1 header

## This is a H2 header

### This is a H3 header

#### This is a H4 header

##### This is a H5 header

###### This is a H6 header


## Paragraph

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tristique sit amet ex at rutrum. Praesent tempor, ligula quis dictum posuere, enim enim porttitor augue, quis placerat diam nibh non urna. Duis commodo, turpis nec ultrices sagittis, nulla neque tempus magna, vel fermentum nisl mi nec massa.

Itatur? Quiatae cullecum rem ent aut odis in re eossequodi nonsequ idebis ne sapicia is sinveli squiatum, core et que aut hariosam ex eat.


## Images

### Markdown

```md
![Hugo test](/img/logo.png)

![Hugo test](/img/logo.png "This is the screenshot")
```

### Output

![Hugo IRIS Theme](/img/logo.png)

![Hugo IRIS Theme](/img/logo.png "This is the screenshot")



## Blockquotes

The blockquote element represents content that is quoted from another source, optionally with a citation which must be within a `footer` or `cite` element, and optionally with in-line changes such as annotations and abbreviations.

<br>

### Blockquote without attribution

> Tiam, ad mint andaepu dandae nostion secatur sequo quae.
> Note that you can use *Markdown syntax* within a blockquote.

### Blockquote with attribution

> Don't communicate by sharing memory, share memory by communicating.
> — <cite>Rob Pike[^1]</cite>

[^1]: The above quote is excerpted from Rob Pike's [talk](https://www.youtube.com/watch?v=PAAkCSZUG1c) during Gopherfest, November 18, 2015.



## Tables

Tables aren't part of the core Markdown spec, but Hugo supports supports them out-of-the-box.

| Name | Age |
|---|---|
| Bob | 27 |
| Alice | 23 |

### Inline Markdown within tables

| Inline | Markdown | In | Table |
|---|---|---|---|
| *italics*  | **bold** | ~~strikethrough~~ | `code` |



## Code Blocks

### kbd, pre, code

Good for keystrokes:

<kbd>⌥</kbd>
<kbd>⌘</kbd>
<kbd></kbd>
<kbd>⇧</kbd>
<kbd>⇪</kbd>
<kbd>⇥</kbd>
<kbd>←</kbd>
<kbd>↩</kbd>
<kbd>⏏</kbd>
<kbd>◀</kbd>
<kbd>▶</kbd>
<kbd>▲</kbd>
<kbd>▼</kbd>
<kbd>Esc:wq!</kbd>

### pre

Keeps format "as is":

<pre>
package main

import "fmt"

func main() {
  fmt.Println("hello world")
}
</pre>

### code

Does not honor line breaks!

<code>
package main

import "fmt"

func main() {
  fmt.Println("hello world")
}
</code>

### Code block with backticks

Both ways look the same to me:

Markdown: <code>```go</code>

```go
package main

import "fmt"

func main() {
  fmt.Println("hello world")
}
```

Markdown: <code>```html</code>

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Example HTML5 Document</title>
  </head>
  <body>
    <p>Test</p>
  </body>
</html>
```

### Code block with Hugo's internal highlight shortcode

Markdown: <code>{{</* highlight go */>}}</code>

{{< highlight go>}}
package main

import "fmt"

func main() {
  fmt.Println("hello world")
}
{{< /highlight >}}

Markdown: <code>{{</* highlight html */>}}</code>

{{< highlight html >}}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Example HTML5 Document</title>
  </head>
  <body>
    <p>Test</p>
  </body>
</html>
{{< /highlight >}}


## Hints and alerts

{{< hint info >}}
```
{{</* hint info */>}}
text
{{</* /hint */>}}
```
{{< /hint >}}

{{< hint warning >}}
```
{{</* hint warning */>}}
text
{{</* /hint */>}}
```
{{< /hint >}}

{{< hint danger >}}
```
{{</* hint danger */>}}
text
{{</* /hint */>}}
```
{{< /hint >}}

<pre>
{{</* alert danger|warning|info|success */>}}
I'm a danger alert
{{</* /alert */>}}
</pre>

{{< alert danger >}}
I'm a danger alert
{{< /alert >}}

{{< alert warning >}}
I'm a warning alert
{{< /alert >}}

{{< alert info >}}
I'm an info alert
{{< /alert >}}


{{< alert success >}}
I'm a success alert
{{< /alert >}}

## List Types

### Ordered List

1. First item
2. Second item
3. Third item

### Unordered List

* List item
* Another item
* And another item

### Nested list

* Item
  1. First Sub-item
  2. Second Sub-item



## Other Elements — abbr, sub, sup, kbd, mark

<abbr title="Graphics Interchange Format">GIF</abbr> is a bitmap image format.

H<sub>2</sub>O

X<sup>n</sup> + Y<sup>n</sup>: Z<sup>n</sup>

Press <kbd><kbd>CTRL</kbd>+<kbd>ALT</kbd>+<kbd>Delete</kbd></kbd> to end the session.

Most <mark>salamanders</mark> are nocturnal, and hunt for insects, worms, and other small creatures.