---
title: "Resize macOS encrypted disk"
date: 2017-07-07
tags: ['macos']
url: resize-macos-encrypted-disk
summary: How to resize an encrypted macOS volume
---

To increase size of volume Encrypted in 250 MB:

`hdiutil resize -size 250m /Users/me/Encrypted/Encrypted.dmg`