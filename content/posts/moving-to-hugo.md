---
title: "Moving to Hugo"
date: 2020-11-14
url: moving-to-hugo
categories: ['blog']
tags: ['hugo', 'web']
summary: Go, move the page to hugo!
---

![Hugo](/img/hugo_logo.png)

Yes, I'm moving again to a different blogging platform.

For some time I was using [Jekyll](https://jekyllrb.com) but it had some drawbacks of me. Mainly, I don't like Ruby. Well, I don't understand Ruby and, honestly, I don't feel like digging deeper into it.

So I wanted to get rid of ruby and find a platform that could run in a non-very-powerful cloud server (meaning "no database, forget wordpress") but easy to work with. [Ghost](https://ghost.org) did the trick for some time.

But after some version change I had some problems updating posts — something I often do. I had no time or motivation to troublesshoot and fix it, so the _blog_ remained kind of abandoned for some time.

Keeping track of changes via git was also a concern. This was not trivial for Ghost. So I thought about moving back to Jekyll. But looking at the available options I found [Hugo](https://gohugo.io).

Finding time in 2020 is not that complicated. Due to COVID-19 we have curfews and lockouts every now and then so there is no other option than staying at home during the weekend. Migrating was quite easy (not a lot of content). And this is the result.

## Installing hugo

I installed Hugo in my MacBook. I'm using [homebrew](https://brew.sh) so it is trivial:

```
% brew install hugo
```

## Creating the blog

Creating a site is again trivial. Just go to your favourite project directory and run:

```
% hugo new site quickstart
```

## Choosing a theme

There is a huge amount of [themes for Hugo](https://themes.gohugo.io). I wanted something really simple so I chose [Geekblog theme](https://themes.gohugo.io/hugo-geekblog/).

Just follow the instrucions in its homepage — basically download and unzip the current release. Funny enough, using get is not recommended (it is possible but makes things a bit more difficult and I do not want that).

I just had to make a few minor changes to adapt it to my taste.

Btw, there is a companion theme named [Geekdoc](https://themes.gohugo.io/hugo-geekdoc/) that looks perfect for migrating my internal documentation, currently in a local Atlassian Confluence instance.

## Configuration file

The configuration file (hugo provides a reasonable default) may be a [toml](https://en.wikipedia.org/wiki/TOML) or [yaml](https://en.wikipedia.org/wiki/YAML) file.

## Creating a post

Creating new posts is as easy as:

```
% hugo new posts/my-new-post.md
```

This command will create a new file based on the template `archetypes/default.md` that looks like this for me:

{{< highlight yaml >}}
---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
url: 
tags: ['']
summary: 
---
{{< /highlight >}}


## Markdown

Of course posts are written with markdown syntax. Learn more here:

- [Markdown cheatsheet, Adam Pritchard](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [The Markdown Guide, Matt Cone](https://www.markdownguide.org/)

## Shortcodes

Shortcodes are simple snippets inside your content files calling built-in or custom templates.

### Example

This code:

```
{{</* highlight html */>}}
<section id="main">
  <div>
   <h1 id="title">{{ .Title }}</h1>
    {{ range .Pages }}
        {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
{{</* /highlight */>}}
```

will render:

{{< highlight html >}}
<section id="main">
  <div>
   <h1 id="title">{{ .Title }}</h1>
    {{ range .Pages }}
        {{ .Render "summary"}}
    {{ end }}
  </div>
</section>
{{< /highlight >}}

So the key is:

```
{{</* highlight html */>}}

{{</* /highlight */>}}
```

There are shortcodes for:

- gist
- instagram
- twitter
- vimeo
- youtube

...and many more.

I like these ones:

### Info/warning/danger boxes

I love these ones:

{{< alert info >}}
```
{{</* alert info */>}}
text
{{</* /alert */>}}
```
{{< /alert >}}

{{< alert warning >}}
```
{{</* alert warning */>}}
text
{{</* /alert */>}}
```
{{< /alert >}}

{{< alert danger >}}
```
{{</* alert danger */>}}
text
{{</* /alert */>}}
```
{{< /alert >}}

## Emojis

Activete emojis adding this to the configuration file:

```
enableEmoji = true
```

Then you will be able to use:

| code | emoji |
| --- | --- |
\:joy\: | :joy:
\:thumbsup\: | :thumbsup:
\:facepalm\: | :facepalm:
\:scream\: | :scream: 
\:metal\: | :metal: 

See a [complete reference here](https://www.webfx.com/tools/emoji-cheat-sheet/)