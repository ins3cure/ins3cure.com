---
title: Adding proper security headers to NGINX
date: 2020-10-12
url: adding-security-headers-to-nginx
author: /me
categories: ['blog']
tags: ['security', 'web']
draft: false
---
Add the right security headers to nginx because we all like to get an "A" in securityheaders.com

<!--more-->

After auditing quite a few sites, I tested my own site for security headers. And guess what happened?

![security report "D"](/img/security-report-summary-d.png)

Wow, this had to be fixed :D

I just added:

```
# Set security headers
add_header X-Frame-Options SAMEORIGIN;
add_header X-Content-Type-Options nosniff;
add_header X-XSS-Protection "1; mode=block";
add_header Content-Security-Policy "default-src 'self'";
add_header Referrer-Policy "no-referrer";

# Set HSTS to 365 days
add_header Strict-Transport-Security 'max-age=31536000; includeSubDomains; preload' always;
```

![security report "A"](/img/security-report-summary-a.png)

I will add the new [Permissions-Policy](https://www.w3.org/TR/permissions-policy-1/) later, once I read about -and hopefully understand- it.

