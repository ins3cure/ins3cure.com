---
title: "Caffeinate"
date: 2020-12-04
url: caffeinate
categories: ['blog']
tags: ['macos']
summary: Prevent macOS from sleeping while doing stuff
draft: false
---

This week I've been trying to watch an online conference (with my left eye) while working (with the right one).

I was using an old MacBook Air with the new macOS Big Sur to watch the conference. As usual, it is configured to lock the screen after a short time of inactivity.

Some programs inhibit this behaviour while active, but it was not the case (is was using Zoom -ouch!- via browser -of course no Zoom app-). So the laptop kept locking unless I was quick enough to press a key.

I do not want to disable my configuration so I had to look for an alternative. And it exists in the OS by default: `caffeinate`.

According to the man page:

```
caffeinate creates assertions to alter system sleep behavior.  If no
assertion flags are specified, caffeinate creates an assertion to prevent 
idle sleep.  If a utility is specified, caffeinate creates the assertions 
on the utility's behalf, and those assertions will persist for the duration 
of the utility's execution. Otherwise, caffeinate creates the assertions
directly, and those assertions will persist until caffeinate exits.
```

There are a few options depending on the assertions you want to create:

- <kbd>-d</kbd> prevents display from sleeping
- <kbd>-i</kbd> prevents system from idle sleeping
- <kbd>-m</kbd> prevents the disk from idle sleeping
- Etc. (see man page)

So to summarize, this is how I solved my annoying problem:

{{< highlight shell >}}
% caffeinate -dt 43200
{{< /highlight >}}

Enough for 12 hours of conferences :grin: 