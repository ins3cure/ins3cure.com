---
title: "Getting started with Vagrant"
date: 2020-08-14
url: getting-started-with-vagrant
categories: ['blog']
tags: ['vagrant']
draft: false
---
Getting started with Vagrant. Very first steps to install and run a vagrant image in a Mac

<!--more-->

![Vagrant logo](/img/vagrant-logo.png)

## macOS install

Install Vagrant from the package in the [official download page](https://www.vagrantup.com/downloads). There is a homebrew package but [Hashicorp discourages installing from package managers](https://www.vagrantup.com/docs/installation).

Once installed it is added to the `PATH` variable:

{{< highlight shell >}}
% vagrant version
Installed Version: 2.2.9
Latest Version: 2.2.9

You're running an up-to-date version of Vagrant!
{{< /highlight >}}

## Documentation

- https://www.vagrantup.com/docs/index

## Getting started

Vagrant can work with several providers, but VirtualBox is builtin. So make sure VirtualBox is installed as well.

### Up and running

This works out of the box:

{{< highlight shell >}}
$ vagrant init hashicorp/bionic64
$ vagrant up
{{< /highlight >}}

VM can be terminated with:

{{< highlight shell >}}
$ vagrant destroy
{{< /highlight >}}

### Project setup

First step is creating a Vagrantfile. This is:

1. the root directory of the project
2. describes the machines and resources you need

There is a command to setup the root directory of a project:

{{< highlight shell >}}
$ mkdir vagrant_getting_started
$ cd vagrant_getting_started
$ vagrant init hashicorp/bionic64
{{< /highlight >}}

You can also run vagrant init in an existing directory to initialize it.

### Installing a box

You can add boxes with `vagrant box add`.

{{< highlight shell >}}
$ vagrant box add hashicorp/bionic64
{{< /highlight >}}

This will download the box named "hashicorp/bionic64" from [HashiCorp's Vagrant Cloud box catalog](https://vagrantcloud.com/boxes/search), a place where you can find and host boxes.

### Using a box

Once you add a box, you have to configure it. So open the Vagrantfile and:

```
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/bionic64"
end
```

You can specify a specific version:

```
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/bionic64"
  config.vm.box_version = "1.1.0"
end
```

or an URL

```
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/bionic64"
  config.vm.box_url = "https://vagrantcloud.com/hashicorp/bionic64"
end
```

### Up and ssh

You can start the environment and ssh to it:

{{< highlight shell >}}
$ vagrant up
$ vagrant ssh
{{< /highlight >}}

`vagrant destroy` will stop and remove the box from Virtual box, but will not delete the files.

The latter is done by `vagrant box remove`.

### Synced folders

Vagrant shares the project directory with the `/vagrant` directory in the guest VM. So, after running:

{{< highlight shell >}}
$ vagrant up
$ vagrant ssh
{{< /highlight >}}

you are inside the VM.Then you can see your project directory in `/vagrant`:

{{< highlight shell >}}
vagrant@vagrant:~$ pwd
/home/vagrant
vagrant@vagrant:~$ cd /vagrant/
vagrant@vagrant:/vagrant$ ls
Vagrantfile
vagrant@vagrant:/vagrant$ ls -la
total 8
drwxr-xr-x  1 vagrant vagrant  128 Jun 13 16:06 .
drwxr-xr-x 24 root    root    4096 Jun 13 16:44 ..
drwxr-xr-x  1 vagrant vagrant  128 Jun 13 16:06 .vagrant
-rw-r--r--  1 vagrant vagrant 3024 Jun 13 16:05 Vagrantfile
{{< /highlight >}}

### Provisioning

There is an automatic provisioning feature builtin. For instance, you can create a script that will run after vagrant up to install software:

{{< highlight shell >}}
#!/usr/bin/env bash
 
apt-get update
apt-get install -y apache2
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi
{{< /highlight >}}

Then configure the Vagrantfile:

```
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/bionic64"
  config.vm.provision :shell, path: "bootstrap.sh"
end
```

And create an index.html file to verify that it works:

{{< highlight html >}}
<!DOCTYPE html>
<html>
  <body>
    <h1>Getting started with Vagrant!</h1>
  </body>
</html>
{{< /highlight >}}

Everything will run when you run `vagrant up`.

If the environment is already running you can use `vagrant reload --provision`.

### Networking

Port forwarding:

```
Vagrant.configure("2") do |config|
  config.vm.box = "hashicorp/bionic64"
  config.vm.provision :shell, path: "bootstrap.sh"
  config.vm.network :forwarded_port, guest: 80, host: 4567
end
```

This will result in http://127.0.0.1:4567 forwarded to the web server in the guest VM.

See official documentation about [networking](https://www.vagrantup.com/docs/networking/).

### Teardown

You can suspend, halt, or destroy the guest machine:

- `vagrant suspend`
- `vagrant halt`
- `vagrant destroy`

Finally you can delete from disk:

- `vagrant box remove NAME`

### Rebuild

`vagrant up` will recreate the environment.

### Providers

You can use any other provider without changing the Vagrantfile:

{{< highlight shell >}}
$ vagrant up --provider=vmware_fusion
{{< /highlight >}}

## References

- [Vagrant Crash Course](https://www.youtube.com/watch?v=vBreXjkizgo) in Youtube:

{{< youtube vBreXjkizgo >}}
