---
title: "Hackthebox: Breach challenge"
date: 2020-12-07T11:40:29+01:00
url: htb/breach
author: /me
categories: ['writeup']
tags: ['challenge', 'osint', 'hacking']
draft: true
---

After a long time I get back to HTB with this very easy OSINT challenge

<!--more-->

So this is the challenge:

{{< alert info >}}
You managed to pull some interesting files off one of Super Secure Startup's anonymous FTP servers. Via some OSINT work(a torrent or online Password breach site) you have also procured a recent data breach dump. Can you unlock the file and retrieve the key?
{{< /alert >}}

After unzipping the file we find:

```shell
$ tree
.
├── ftp-files
│   ├── key.docx
│   └── web-developer-needed.docx
├── hash.txt
├── office2hashcat.py
├── public-data-breach.txt

1 directory, 3 files
```

The file `public-data-breach.txt` contains typical information from a leak:

```shell
$ head public-data-breach.txt 
id,first_name,last_name,email,gender,ip_address,password
1,Tannie,Suckling,tsuckling0@indiegogo.com,Male,132.16.254.188,"bzonlyone"
2,Agna,Delhay,adelhay1@instagram.com,Female,157.252.209.139,"dipset07"
3,Kari,Daber,kdaber2@salon.com,Female,138.180.122.147,"nghi2308"
4,Harp,Sidle,hsidle3@yandex.ru,Male,14.115.255.103,"bec087131"
```

And two docx files, one of them is encrypted. First idea is to _brute force_ the docx with a dictionary created from the leak. So:

Create the dictionary:

```shell
awk -F, '{print $NF}' public-data-breach.txt | sed 's/"/d/g' > dict.txt
```

Run:

```shell
hashcat -a 0 -m 9600 --status -o found.txt hash.txt dict.txt
```

And of course it fails, mainly because we have not done any OSINT and this is an OSINT challenge :smile: 

So let's take a closer look at the clear text document:

> Please send your CV to hr@ supersecurestartup.com for details.

Ok, so there is a [@supersecurestartup.com](https://twitter.com/SuperSecStartup) Twitter account. You can have a look.

In the meantime, we try to find leaked accounts from the super secure company:

```shell
$ grep supersec public-data-breach.txt 
267,Johanna,Boyce,j.boyce@supersecurestartup.com,Female,225.10.71.76,"t434839865"
5502,Ishaaq,Boone,i.boone@supersecurestartup.com,Female,9.69.124.206,"shibby0"
9673,Lidia,Kaur,l.kaur@supersecurestartup.com,Female,81.107.254.205,"dama-051288."
13686,Kalvin,Tyler,k.tyler@supersecurestartup.com,Male,119.245.151.100,"mybebosyt"
17620,Bianka,Phelps,b.phelps@supersecurestartup.com,Female,126.204.123.232,"Love!July2018"
19955,Pedro,Smith,p.smith@supersecurestartup.com,Male,62.130.245.163,"shloffle"
```

Ok, one of the tweets is replied by someone who claims to be a super secure HR employee:

![Tweet](/img/htb-breach-1.png)

And happens to be in the leak!

```sh
17620,Bianka,Phelps,b.phelps@supersecurestartup.com,Female,126.204.123.232,"Love!July2018"
```

Of course the password does not work at the first attempt. I was tempted to brute force it, but let's try to be a bit smarter:

```shell
$ ls -l ftp-files/key.docx 
-rw-r--r-- 1 501 dialout 18432 Mar 26  2019 ftp-files/key.docx
```

The file is from March 2019 so let's try `Love!March2019`. Yeah...!!

We get some _encoded_ stuff:

> Encrypted SSH Key for root user:
> SFRCe1A0c...XzBmX0luZjBybWF0aTBufQ==

Once the base64 is _decoded_ we get the flag.

{{< alert danger >}}
Note: this post must be hidden until the challenge is retired
{{< /alert >}}