---
title: "Fail2ban in Centos 8"
date: 2020-05-31
url: fail2ban-in-centos-8
categories: ['blog']
tags: ['linux', 'security']
---
Fail2Ban is an intrusion prevention software written in Python that protects computer servers from brute-force attacks

<!--more->

Even if ssh password authentication is disabled, I find terribly annoying having the logs filled up with IP addresses trying to bruteforce ssh logins.

That's why I still deploy `fail2ban`.

You can easily install fail2ban on CentOS 8 provided that you have added EPEL repository:

```
yum -y install fail2ban
systemctl enable --now fail2ban
```

sshd jail is not created by default. To solve it:

```
cat >/etc/fail2ban/jail.d/10-sshd.conf <<EOF
[Default]
bantime = 900
findtime = 600
maxretry = 3
# ignoreip = 127.0.0.1/8 ::1 103.1.2.3
banaction = iptables-multiport

[sshd]
enabled = true
EOF

systemctl reload fail2ban
```

This means after 3 (maxretry) failed attempts in 10 (findtime) minutes, an IP will be banned for 900 (bantime) seconds.

You can list banned IPs:

```
# fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed:	16
|  |- Total failed:	132
|  `- Journal matches:	_SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned:	5
   |- Total banned:	11
   `- Banned IP list:	222.186.175.150 222.186.31.166 222.186.175.163 222.186.30.57 222.186.175.154
```

...and the firewall rich rules:

```
# firewall-cmd --list-rich-rules
rule family="ipv4" source address="222.186.31.166" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
rule family="ipv4" source address="222.186.175.163" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
rule family="ipv4" source address="222.186.30.57" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
rule family="ipv4" source address="222.186.175.154" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
rule family="ipv4" source address="112.85.42.194" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
```