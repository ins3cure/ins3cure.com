---
title: "Adding syntax hightlighting to Ghost blog posts"
date: 2020-06-11
url: adding-syntax-hightlighting-to-ghost-blog-posts
author: /me
categories: ['blog']
tags: ['ghost', 'web']
draft: false
---

How to add colour to your code in ghost blog posts

<!--more-->

![code picture](/img/code-program-null.jpg)

Next time you want to add colour to your code in ghost blog posts do this:

1. Go to https://cdnjs.com/libraries/prism.
2. Go to your Ghost settings -> Code Injection

![Ghost code injection](/img/ghost_code_injection.png)

3. Not sure if it is really relevant but copy this from the prism page into the headers:

{{< highlight html >}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.16.0/prism.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.16.0/plugins/line-numbers/prism-line-numbers.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.16.0/themes/prism-okaidia.css" />
{{< /highlight >}}

4. ...and all languages you need into the footer:

{{< highlight html >}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.20.0/components/prism-python.min.js" integrity="sha256-z1w1HLignub5kA0f24TCzv765MT4114LEgW/oIYuLag=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.20.0/components/prism-bash.min.js" integrity="sha256-aGuBnrEzXZQ2RDCdqqyCzGXLeJzXRU0kJx8KoUTFPYs=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.20.0/components/prism-css.min.js" integrity="sha256-4ut/7XCv/zjtcNF9GlQL+Jpy3AuQlLt2ExXNtIjZ8Po=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.20.0/components/prism-javascript.min.js" integrity="sha256-g87nhVlyWjKmsk1hSfvqjr22ILWlzX/0LtHFXkLB6IU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.20.0/components/prism-markdown.min.js" integrity="sha256-LVbOCHxYsNr9pF5KGdB2NphtO2FKM92SGFxRkWc9Ovg=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.20.0/components/prism-go.min.js" integrity="sha256-Og32rFEm2ICsLGNCdEf+nUE/QDhI4nxQ2SLHXD4Yv90=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.20.0/components/prism-powershell.min.js" integrity="sha256-WqpEa6OsJule/vRjgts5OUXn1decJ74Kxq4WdMCfTTc=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.20.0/components/prism-shell-session.min.js" integrity="sha256-AA80TzgLh7tiMCgr55/1mQEG07qTSZYBE/yp8FF4hLQ=" crossorigin="anonymous"></script>
{{< /highlight >}}

