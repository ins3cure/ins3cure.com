---
title: "Pandoc on RHEL-8"
date: 2022-01-16T09:04:44+01:00
url: /pandoc-on-rhel-8
author: /me
categories: ['blog']
tags: ['pandoc']
draft: false
---

Pandoc is a Haskell library for converting from one markup format to another, and a command-line tool that uses this library. It can convert from/to multiple formats including, but not limited to, various flavors of Markdown, HTML, LaTeX and Word docx and can produce PDF output.

<!--more-->

Unfortunately `pandoc` is not available in Red Hat Enterprise Linux repos.

# How to install

`pandoc` provides for different operatig systems including Linux. The Linux tarball is a statically linked binary and it has certain limitations (it can't use LUA filters).

Installation is not rocket science. Go to https://github.com/jgm/pandoc/releases/latest, download the latest tarball (currently pandoc-2.17.0.1-linux-amd64.tar.gz) and run:

{{< highlight shell >}}
sudo tar xvzf pandoc-2.17.0.1-linux-amd64.tar.gz --strip-components 1 -C /usr/local/
{{< /highlight >}}

You want to install TeX to be able to generate PDF output:

{{< highlight shell >}}
sudo yum -y install texlive
{{< /highlight >}}

The TeX Live install is huge:

{{< highlight shell >}}
[...]
Transaction Summary
=======================================================================================
Install  227 Packages

Total download size: 356 M
Installed size: 607 M
Is this ok [y/N]: y
Downloading Packages:
[...]
{{< /highlight >}}

# Hello World!

Of course it works, producting nice PDFs:

{{< highlight shell >}}
pandoc hello.md -f markdown -t pdf -o hello.pdf
{{< /highlight >}}

# Eisvogel template

But bu may want to use fancy templates as Eisvogel, aimed at technical documents:

Go to https://github.com/Wandmalfarbe/pandoc-latex-template/releases/latest and download the latest version of the template (currently Eisvogel-2.0.0.tar.gz). Extract the needed file (there are lots of extra files that are not required) and move it to the local templates directory (usually ~/.pandoc/templates).

{{< highlight shell >}}
mkdir -p ~/.pandoc/templates
tar xf ~/Downloads/Eisvogel-2.0.0.tar.gz eisvogel.latex
mv eisvogel.latex ~/.pandoc/templates/
{{< /highlight >}}

Ok, now we should be able to create a very nice pdf document based on that template:

{{< highlight shell >}}
$ pandoc pandoc-on-rhel-8.md -f markdown -s -t pdf -o pandoc-on-rhel-8.pdf --template eisvogel
Error producing PDF.
! LaTeX Error: File `footnotebackref.sty' not found.

Type X to quit or <RETURN> to proceed,
or enter new name. (Default extension: sty)

Enter file name: 
! Emergency stop.
<read *> 
         
l.99 \setlength
{{< /highlight >}}

Meeeec. It seems some component is missing. Maybe we can install them from the repos:

{{< highlight shell >}}
$ yum search footnotebackref.sty
No matches found.

$ yum provides footnotebackref.sty
Error: No Matches found
{{< /highlight >}}

Meeec. 

Hmmm, maybe we did not install enough texlive packages. There are a few more:

{{< highlight shell >}}
$ sudo yum install texlive*
[sudo] password for user:
[...]
Transaction Summary
==========================================================================================
Install  124 Packages

Total download size: 128 M
Installed size: 197 M
Is this ok [y/N]: 
[...]
{{< /highlight >}}

Meeec. Same error. Before continuing, I tried to edit the template and removing the missing package. It only led to another missing package. I commented it out. Removed it and... you guessed it, another missing dependency. And a lot more. No, that approach did not work.

The I decided to try to use the LaTeX Live Manager:

{{< highlight shell >}}
$ tlmgr install footnotebackref
*** WARNING ***: Performing this action will likely destroy the RHEL TeXLive install on your system.
*** WARNING ***: This is almost NEVER what you want to do.
*** WARNING ***: Try using dnf install/update instead.
*** WARNING ***: If performing this action is really what you want to do, pass the "ignore-warning" option.
*** WARNING ***: But please do not file any bugs with the OS Vendor.
{{< /highlight >}}

Meeec. Sounds scary! And I don't want to destroy my laptop! But... who cares? I will try to install all missing dependencies:

{{< highlight shell >}}
$ tlmgr install adjustbox babel-german background bidi collectbox csquotes everypage filehook footmisc footnotebackref framed fvextra letltxmacro ly1 mdframed mweights needspace pagecolor sourcecodepro sourcesanspro titling ucharcat ulem unicode-math upquote xecjk xurl zref --ignore-warning
You don't have permission to change the installation in any way,
specifically, the directory /usr/share/texlive/tlpkg/ is not writable.
Please run this program as administrator, or contact your local admin.
tlmgr: An error has occurred. See above messages. Exiting.
{{< /highlight >}}

Meeec. No permissions? Who do you think you are?

{{< highlight shell >}}
$ sudo tlmgr install adjustbox babel-german background bidi collectbox csquotes everypage filehook footmisc footnotebackref framed fvextra letltxmacro ly1 mdframed mweights needspace pagecolor sourcecodepro sourcesanspro titling ucharcat ulem unicode-math upquote xecjk xurl zref --ignore-warning
[sudo] password for user: 
tlmgr: Remote repository is newer than local (2017 < 2018)
Cross release updates are only supported with
  update-tlmgr-latest(.sh/.exe) --update
Please see https://tug.org/texlive/upgrade.html for details.
{{< /highlight >}}

The [link above](https://tug.org/texlive/upgrade.html) leads to some instructions that do not match the Red Hat installation.

Well, this is open source, we can still build everything from source.

# Docker image to the rescue!

Ok, I give up. But there is still a chance. Pandoc offers not only several binaries for different operating systems, but also docker images, including some with LaTeX for PDF output. So go install `podman` (rootless/daemonless _redhat-ish_ tool to run standalone (non-orchestrated) containers):

{{< highlight shell >}}
$ sudo yum install podman buildah skopeo
{{< /highlight >}}

And test it:

{{< highlight shell >}}
podman run --rm --volume "`pwd`:/data" \
  --user `id -u`:`id -g` \
  pandoc/latex README.md \
  -o README.pdf
{{< /highlight >}}

Cool, it works. But we still don't have the nice Eisvogel template. But chances are that some invested some time in adding it to the pandoc _official_ dockerfile. Turns out yes, it happened: [Pandoc LaTeX with Eisvogel](https://hub.docker.com/r/rstropek/pandoc-latex).

Just add the `mydoc.yaml` metadata and run the example command:

{{< highlight shell >}}
podman run --rm \
    -v `pwd`:/data \
    -w /data \
    rstropek/pandoc-latex \
    -f markdown \
    --template https://raw.githubusercontent.com/Wandmalfarbe/pandoc-latex-template/v1.4.0/eisvogel.tex \
    -t latex \
    -o mydoc.pdf \
    --metadata-file=mydoc.yaml \
    mydoc.md
{{< /highlight >}}

It works! It needs a bit outdated template (the newest version is 2.0.0 and does not work) but that's a start. I may update the post if I succeed updating the Dockerfile so that it works with the most recent template.