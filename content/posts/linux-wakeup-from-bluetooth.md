---
title: "Linux wakeup from Bluetooth devices"
date: 2021-01-25T15:54:40+01:00
url: linux-wakeup-from-bluetooth
author: /me
categories: ['blog']
tags: ['bluetooth', 'linux', 'ubuntu']
draft: false
---

How to wake up from suspend an Ubuntu laptop with a Bluetooth device

<!--more-->

For different reasons, I use to have multiple laptops in my desktop, all of them connected to a single monitor. So most of the times I am using only one while the remaining laptops stay suspended.

With the Macbook there is no issue: just press a key on the bluetooth keyboard or click on the Magic Mouse and the device wakes up flawlessly. But it is different with my Ubuntu ThinkPad. Laptops are usually closed on some kind of vertical stand so I have to pick it up, open the lid to wake it up and put it back on the stand carefully not to get entangled with the cables. Really inconvenient.

It took only a couple of searches to find out that it is possible to wake from suspend in Linux with Bluetooth devices but it is usually disabled by default. According to [kernel documentation](/sys/bus/usb/devices/*/power/wakeup):

> power/wakeup

> This file is empty if the device does not support remote wakeup. Otherwise the file contains either the word enabled or the word disabled, and you can write those words to the file. The setting determines whether or not remote wakeup will be enabled when the device is next suspended. (If the setting is changed while the device is suspended, the change won’t take effect until the following suspend.)

{{< highlight shell >}}
$ grep . /sys/bus/usb/devices/*/power/wakeup
/sys/bus/usb/devices/1-5/power/wakeup:disabled
/sys/bus/usb/devices/1-7/power/wakeup:disabled
/sys/bus/usb/devices/1-9/power/wakeup:disabled
/sys/bus/usb/devices/usb1/power/wakeup:disabled
/sys/bus/usb/devices/usb2/power/wakeup:disabled
{{< /highlight >}}

It just has to be enabled to work:

{{< highlight shell >}}
echo enabled |sudo tee /sys/bus/usb/devices/*/power/wakeup
{{< /highlight >}}

But we need to find a way to make it permanent (and yeah, that code is ugly and I don't feel comfortable with that wildcard!)

(to be continued...)