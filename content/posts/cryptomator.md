---
title: "Cryptomator"
date: 2020-12-12T17:05:02+01:00
url: cryptomator
author: /me
categories: ['blog']
tags: ['cloud', 'encryption', 'security']
draft: false
---

The cloud is a convenient place to store your files. But convenience is not always the best for your privacy. Cryptomator comes to the rescue - it was designed to solve privacy issues when saving your files to cloud storage.

<!--more-->

If your using Google One, One Drive, Dropbox... yes, they are reading your files. _[At least them](https://en.wikipedia.org/wiki/PRISM_(surveillance_program))_. There are services more privacy oriented like pCloud o Tresorit but they may be more expensive and lack some of the features that big players offer.

![cryptomator logo](/img/cryptomator_logo.png)

If you are concerned about your privacy, you can use [Cryptomator](https://cryptomator.org/). Cryptomator was designed to solve privacy issues when saving files to cloud storages. Being an open source project, there is a complete [security documentation](https://docs.cryptomator.org/en/latest/security/security/architecture/).

Some key points:

- Cryptomator provides a virtual drive (called _vault_) where you can add, edit and remove files as with anhy other drive
- Files are transparently encrypted and decrypted using state-of-the-art AES-256 algorithm.
- Multiplatform: works in Windows, macOS, Linux, Android and iOS.
- Unlike other open source projects, [cryptomator has been independently audited](https://cryptomator.org/faq/security/audits/)

But there also drawbacks. Not _everything_ is _always_ encrypted. Cryptomator has not been designed for that, it is especially oriented to the cloud. In particular, [according to the documentation](https://docs.cryptomator.org/en/latest/security/security-target/), Cryptomator does not encrypt:

- access, modification, and creation timestamp of files and folders,
- number of files and folders in a vault and in the folders, and
- size of the stored files.

{{< alert danger >}}
To summarize: Cryptomator is a tool especially designed to protect your files at rest in the cloud.
{{< /alert >}}

## Usage demo

{{< youtube T76ozVJs9l4 >}}