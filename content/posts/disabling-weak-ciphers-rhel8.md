---
title: "Disabling weak ciphers in SSH (RHEL8)"
date: 2020-11-23T20:13:32+01:00
url: disabling-weak-ciphers-rhel8
categories: ['blog']
tags: ['ssh', 'rhel', 'security']
summary: Discovering the new system-wide crypto policy in RHEL8
draft: false
---

Imagine a Nessus (or any other vulnerability scan tool) reports something like this:

> **SSH Server CBC Mode Ciphers Enabled**
>
> **Description**
>
>The SSH server is configured to support Cipher Block Chaining (CBC) >encryption. This may allow an attacker to recover the plaintext message >from the ciphertext. 
>
>Note that this plugin only checks for the options of the SSH server and >does not check for vulnerable software versions.
>
>**Solution**
>
>Contact the vendor or consult product documentation to disable CBC mode >cipher encryption, and enable CTR or GCM cipher mode encryption.

A quick check shows that weak CBC mode ciphers are enabled:

{{< highlight shell >}}
ssh -vv user@remote.server.com 2>&1 | grep -i cbc
debug2: ciphers ctos: aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@openssh.com,aes128-ctr,aes128-cbc
debug2: ciphers stoc: aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cbc,aes128-gcm@openssh.com,aes128-ctr,aes128-cbc
{{< /highlight >}}

This was traditionally fixed editing `/etc/ssh/sshd_config` and removing unwanted algorithms. What is in there?

{{< highlight shell >}}
# grep -i Ciphers /etc/ssh/sshd_config
# Ciphers and keying
# Ciphers, MACs, KexAlgoritms and GSSAPIKexAlgorithsm will not have any
{{< /highlight >}}

Oops, nothing! In that case, the default configuration is being used. Checking `man 5 ssh_config` we should get the defaults but in RHEL 8 we find this instead:

> The default is handled system-wide by crypto-policies(7).  To see the defaults and how to modify this default, see manual page update-crypto-policies(8).

This is apparently new in RHEL 8. We can get the available ciphers:

{{< highlight shell >}}
[user@server ~]# ssh -Q cipher
3des-cbc
aes128-cbc
aes192-cbc
aes256-cbc
rijndael-cbc@lysator.liu.se
aes128-ctr
aes192-ctr
aes256-ctr
aes128-gcm@openssh.com
aes256-gcm@openssh.com
chacha20-poly1305@openssh.com
{{< /highlight >}}

And the policy:

{{< highlight shell >}}
[user@server ~]# update-crypto-policies --show
DEFAULT
{{< /highlight >}}

And the crypto policy status for SSH:

{{< highlight shell >}}
[user@server ~]# cat /etc/sysconfig/sshd
# Configuration file for the sshd service.

# The server keys are automatically generated if they are missing.
# To change the automatic creation, adjust sshd.service options for
# example using  systemctl enable sshd-keygen@dsa.service  to allow creation
# of DSA key or  systemctl mask sshd-keygen@rsa.service  to disable RSA key
# creation.

# Do not change this option unless you have hardware random
# generator and you REALLY know what you are doing

SSH_USE_STRONG_RNG=0
# SSH_USE_STRONG_RNG=1

# System-wide crypto policy:
# To opt-out, uncomment the following line
# CRYPTO_POLICY=
{{< /highlight >}}

The commented line `# CRYPTO_POLICY=` indicates that crypto policy is in use by ssh.

According to official documentation, crypto policy affects:

- TLS
- IPSec
- SSH
- DNSSec
- Kerberos

And there are four defined policies (description in the official documentation):

- DEFAULT: secure settings for current threat models
- LEGACY: ensures maximum compatibility but is less secure
- FUTURE: conservative security level that is believed to withstand any near-term future attacks
- FIPS: conforms with the FIPS 140-2 requirements

Apparently we have two choices:

1. The RHEL8 way: update crypto policy via  `update-crypto-policies` command
2. The traditional way: opt out from crypto policy and configure `sshd_config` as usual

### The RHEL8 way

Ok, let's see if the RHEL8 way helps. We'll run the command...

{{< highlight shell >}}
[user@server ~]# update-crypto-policies --set FUTURE
Setting system policy to FUTURE
Note: System-wide crypto policies are applied on application start-up.
It is recommended to restart the system for the change of policies
to fully take place.
{{< /highlight >}}

...and reboot (oops) the system. After a few seconds the system is up and running again (WARNING, your mileage may vary).

Unfortunately it did not help:

{{< highlight shell >}}
# grep -i Ciphers /etc/ssh/sshd_config
# Ciphers and keying
# Ciphers, MACs, KexAlgoritms and GSSAPIKexAlgorithsm will not have any
{{< /highlight >}}

Despite policy was really changed

{{< highlight shell >}}
[user@server ~]# update-crypto-policies --show
FUTURE
{{< /highlight >}}

it did not affect the valid _weak_ algorithms.

### The traditional way

We could try with FIPS but I prefer to try the traditional way. So we uncomment the line `# CRYPTO_POLICY=` in `/etc/sysconfig/sshd`, add this line to the configuration file:

{{< highlight shell >}}
[user@server ~]# systemctl restart sshd
{{< /highlight >}}

And restart ssh service::

```
Ciphers aes128-ctr,aes192-ctr,aes256-ctr,aes128-gcm@openssh.com,aes256-gcm@openssh.com,chacha20-poly1305@openssh.com
```

Rescan the server and the vulnerability will be -hopefully- gone.

## Important update

{{< alert danger >}}
Setting crypto policy to FUTURE breaks nginx SSL!!
{{< /alert >}}

Apparently because of a weak let's encrypt certificate:

```
2020/11/23 20:07:04 [emerg] 10552#0: SSL_CTX_use_certificate("/etc/letsencrypt/.../fullchain.pem") failed (SSL: error:140AB18F:SSL routines:SSL_CTX_use_certificate:ee key too small)
```

## To do

1. Would FIPS mode have disabled AES-CBC mode? I don't know but I will try to setup a system to test it.
2. AInvestigate why the Let's Encrypt certificate does not work in the FUTURE :grin:  

## References

- [Using system-wide cryptographic policies](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening)
- [System-wide crypto policies in RHEL 8](https://access.redhat.com/articles/3666211)