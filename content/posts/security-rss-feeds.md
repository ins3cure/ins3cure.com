---
title: "Security RSS feeds"
date: 2021-01-28T18:56:49+01:00
url: security-rss-feeds
author: /me
categories: ['blog']
tags: ['security']
draft: false
---

Some security-related RSS feeds

<!--more-->

## announcements

* Citrix Knowledge Center : Security Bulletins
https://support.citrix.com/feed/products/all/securitybulletins.rss
* Juniper Networks - Channel: Security Advisories Category: Security Advisories
https://kb.juniper.net/InfoCenter/index?page=rss&channel=SECURITY_ADVISORIES&cat=SIRT_1&detail=content
* Ubuntu security notices
https://usn.ubuntu.com/rss.xml
* updates
https://bodhi.fedoraproject.org/rss/updates/?type=security
* Microsoft Security Content: Comprehensive Edition
http://technet.microsoft.com/en-us/security/rss/comprehensive
* LinuxSecurity.com - Security Advisories
http://www.linuxsecurity.com/static-content/linuxsecurity_advisories.rss
* Apple - Support - Most Recent  - macOS
http://rss.support.apple.com/macos
* Debian Security
https://www.debian.org/security/dsa
* VMware Security & Compliance Blog
http://blogs.vmware.com/security/rss.xml
* Cisco Security Advisory
https://tools.cisco.com/security/center/psirtrss20/CiscoSecurityAdvisory.xml
* CERTSI - Vulnerabilities RSS
https://www.certsi.es/feed/vulnerabilities
* US-CERT Current Activity
http://www.us-cert.gov/ncas/current-activity.xml
* National Vulnerability Database
https://nvd.nist.gov/feeds/xml/cve/misc/nvd-rss.xml

## blogs

* The Register - Security
https://www.theregister.co.uk/security/headlines.atom
* Security Research & Defense
https://blogs.technet.microsoft.com/srd/feed/
* Red Hat Security Blog Blog Posts
https://access.redhat.com/blogs/766093/feed
* Project Zero
https://googleprojectzero.blogspot.com/feeds/posts/default
* MSRC
http://blogs.technet.com/b/msrc/rss.aspx
* Security Intelligence
http://feeds.feedburner.com/SecurityIntelligence
* SecurityFocus News
http://www.securityfocus.com/rss/news.xml
* The Hacker News
http://thehackernews.com/feeds/posts/default
* Google Online Security Blog
http://googleonlinesecurity.blogspot.com/atom.xml
* Qualys Blog
https://community.qualys.com/blogs/feeds/blogs
* Dark Reading:
http://www.darkreading.com/rss/all.xml
* SecurityFocus Vulnerabilities
http://www.securityfocus.com/rss/vulnerabilities.xml
* Krebs on Security
http://krebsonsecurity.com/feed
* EmergingThreats's Main web
http://doc.emergingthreats.net/bin/view/Main/WebRss
* Darknet
http://feeds.feedburner.com/darknethackers
* Schneier on Security
http://www.schneier.com/blog/index.rdf
* Naked Security - Sophos
http://feeds.feedburner.com/NakedSecurity

## CMS

* Security – WordPress News
https://wordpress.org/news/category/security/feed/
* Security Announcements
https://developer.joomla.org/security-centre.feed?type=rss
* Security advisories
https://www.drupal.org/security/rss.xml

## Spanish CCN-CCERT

* Últimas Vulnerabilidades
https://www.ccn-cert.cni.es/component/obrss/rss-ultimas-vulnerabilidades.feed
* Noticias
https://www.ccn-cert.cni.es/component/obrss/rss-noticias.feed
* CERTSI - Security Advisories RSS
https://www.incibe-cert.es/feed/avisos-seguridad/all
