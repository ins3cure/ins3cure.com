---
title: Privacy policy
url: privacy
author: /me
draft: false
---

We collect information about your IP address, browser and the date you visited us in technical logs that are kept for 30 days.

Logs look like this:

```
<ip_address> - - [01/Jan/2020:00:00:00 +0000] "GET / HTTP/1.1" 200 17445 "-" "Mozilla/5.0  (User-agent)" "-"
```

That's all we know about you. We do not share or shell this information.