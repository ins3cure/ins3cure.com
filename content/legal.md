---
title: Legal notice
url: legal
author: /me
draft: false
---

The information in this weblog is provided “AS IS” with no warranties, and confers no rights.

This weblog does not represent the thoughts, intentions, plans or strategies of my employer, whoever it is at this moment. It is solely my opinion.

## Privacy and cookies

Please read our [privacy](/privacy) and [cookie](/cookies) policies if you are really interested.